/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module accdb is aliced;

import std.datetime;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.strex;
import iv.tox;
import iv.txtser;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;


// ////////////////////////////////////////////////////////////////////////// //
public alias TextDigest = ubyte[20]; // ripemd160

public TextDigest textDigest (const(void)[] buf) nothrow @trusted @nogc {
  import std.digest.ripemd;
  return ripemd160Of(buf);
}


public SysTime systimeNow () nothrow @trusted {
  try {
    SysTime st = Clock.currTime;
    return SysTime.fromUnixTime(st.toUnixTime); // trunc to second precision
  } catch (Exception e) {}
  assert(0, "time error!");
}


// ////////////////////////////////////////////////////////////////////////// //
enum TriOption { Default = -1, No = 0, Yes = 1 }

enum ContactStatus { Offline, Online, Away, Busy, Connecting }

alias PubKey = ubyte[TOX_PUBLIC_KEY_SIZE];
alias ToxAddr = ubyte[TOX_ADDRESS_SIZE];


// ////////////////////////////////////////////////////////////////////////// //
struct CommonOptions {
  TriOption showOffline = TriOption.Default; // show this contact even if it is offline
  TriOption showPopup = TriOption.Default; // show popups for messages from this contact
  TriOption blinkActivity = TriOption.Default; // blink tray icon if we have some activity for this contact
  TriOption skipUnread = TriOption.Default; // skip this contacts in `next_unread` command
  TriOption ftranAllowed = TriOption.Default; // file transfers allowed for this contact
  int resendRotDays = -1; // how many days message should be in "resend queue" if contact is offline (-1: use default value)
  int hmcOnOpen = -1; // how many history messages we should show when we opening a chat with a contact (-1: use default value)
  //@SRZNonDefaultOnly TriOption confAutoJoin; // automatically join the conference when we're going online
}


struct ProtoOptions {
  bool ipv6;
  bool udp;
  bool localDiscovery;
  bool holePunching;
  ushort startPort;
  ushort endPort;
  ushort tcpPort;
  ubyte proxyType;
  ushort proxyPort;
  string proxyAddr;
}


struct AccountConfig {
  string nick; // my nick
  string statusmsg; // my status message
  bool showOffline; // show offline persons?
  bool showPopup; // show popups for messages?
  bool blinkActivity; // blink tray icon if we have some activity (unread msg, transfer request, etc.)?
  bool skipUnread; // skip contacts in `next_unread` command?
  bool hideEmptyGroups; // hide empty groups? (can be overriden by `hideNoVisible` group option)
  bool ftranAllowed; // file transfers allowed for this group (-1: use default value)
  int resendRotDays; // how many days message should be in "resend queue" if contact is offline
  int hmcOnOpen; // how many history messages we should show when we opening a chat with a contact
}


struct GroupOptions {
  uint gid; // group id; there is always group with gid 0, it is "common" default group
  string name; // group name
  string note; // group notes/description
  bool opened; // is this group opened?
  TriOption showOffline = TriOption.Default; // show offline persons in this group
  TriOption showPopup = TriOption.Default; // show popups for messages from this group
  TriOption blinkActivity = TriOption.Default; // blink tray icon if we have some activity (unread msg, transfer request, etc.) for this group
  TriOption skipUnread = TriOption.Default; // skip contacts from this group in `next_unread` command
  TriOption hideIfNoVisibleMembers = TriOption.Default; // hide this group if there are no visible items in it
  TriOption ftranAllowed; // file transfers allowed for this group
  int resendRotDays = -1; // how many days message should be in "resend queue" if contact is offline (-1: use default value)
  int hmcOnOpen = -1; // how many history messages we should show when we opening a chat with a contact (-1: use default value)
}


struct ContactInfo {
  enum Kind {
    // normal contact
    Friend,
    // pending authorization acceptance
    //   statusmsg: request text
    //   lastonlinetime: request time
    PengingAuthAccept,
    // auth requested, awaiting acceptance
    //   statusmsg: request text
    //   lastonlinetime: request time
    // this will change to `Friend` when contact gets online (it means that auth is accepted)
    PengingAuthRequest,
    // this contact was deleted and put in "total ignore" mode
    // i.e. any activity from this contact (especially auth requests) will be silently dropped on the floor
    KillFuckDie,
  }
  uint gid; // group id (see groups.rc)
  string nick; // empty: unauthorized
  @SRZNonDefaultOnly string visnick; // empty: use `nick`
  @SRZNonDefaultOnly string statusmsg; // request message for pending auth request
  @SRZNonDefaultOnly uint lastonlinetime; // local unixtime; changed when contact status changed between offline and online (any kind of online)
  Kind kind = Kind.Friend;
  string note; // contact notes/description
  PubKey pubkey; // used as unique contact id, same as directory name
  uint nospam; // used only in `toxCoreLoadDataFile()`
  @SRZNonDefaultOnly ToxAddr fraddr; // as address includes nospam, save it too
  CommonOptions opts;
}


// ////////////////////////////////////////////////////////////////////////// //
private void syncFile(bool fullsync=false) (Imp!"core.stdc.stdio".FILE* fl) nothrow @nogc {
  if (fl !is null) {
    import core.stdc.stdio : fileno;
    int fd = fileno(fl);
    if (fd >= 0) {
      import core.sys.posix.unistd : fsync, fdatasync;
      static if (fullsync) fsync(fd); else fdatasync(fd);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void mkdirRec (const(char)[] path) nothrow {
  import core.stdc.stdlib : malloc, free;
  import core.sys.posix.sys.stat : mkdir;

  if (path.length == 0) return;

  char* tpath = cast(char*)malloc(path.length+1024);
  if (tpath is null) assert(0, "out of memory");
  scope(exit) free(tpath);
  uint tpos = 0;

  if (path[0] == '/') tpath[tpos++] = '/';

  while (path.length > 0) {
    if (path[0] == '/') { path = path[1..$]; continue; }
    while (path.length && path[0] != '/') {
      tpath[tpos++] = path[0];
      path = path[1..$];
    }
    tpath[tpos] = 0;
    mkdir(tpath, 0o700);
    tpath[tpos++] = '/';
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// write to temporary file, then atomically replace
public bool serialize(T) (in auto ref T v, string fname) nothrow {
  import core.stdc.stdlib : realloc, free;
  import core.stdc.stdio : rename;
  import core.sys.posix.fcntl : open, O_WRONLY, O_CREAT, O_TRUNC;
  import core.sys.posix.unistd : close, write, fdatasync, unlink;
  import std.internal.cstring;

  static struct OutRng {
    int fd;
    ubyte[256] buf;
    uint bufused;
    bool waserror;

    this (int afd) nothrow @trusted @nogc { fd = afd; }
    ~this () { flush(); }
    @disable this (this);
    void opAssign() (in auto ref OutRng a) { static assert(0, "no assigns!"); }
    void flush () nothrow @trusted @nogc {
      if (waserror) { bufused = 0; return; }
      uint bufofs = 0;
      while (bufofs < bufused) {
        auto wr = write(fd, buf.ptr+bufofs, bufused-bufofs);
        if (wr == 0) { waserror = true; bufused = 0; return; }
        if (wr < 0) {
          import core.stdc.errno;
          if (errno != EINTR) { waserror = true; bufused = 0; return; }
          continue;
        }
        bufofs += cast(uint)wr;
      }
      bufused = 0;
    }
    void put (char ch) nothrow @trusted @nogc {
      if (waserror) return;
      assert(bufused < buf.sizeof);
      buf.ptr[bufused++] = ch;
      if (bufused == buf.sizeof) flush();
    }
  }

  if (fname.length == 0 || fname.length > 65536) return false;

  static char* tmpfname = null;
  uint tmpfnsize = 0;
  if (tmpfnsize < fname.length+8) {
    tmpfnsize = cast(uint)fname.length+8;
    tmpfname = cast(char*)realloc(tmpfname, tmpfnsize);
    if (tmpfname is null) assert(0, "out of memory");
  }
  tmpfname[0..fname.length] = fname[];
  tmpfname[fname.length..fname.length+5] = ".$$$\x00";
  int fo = open(tmpfname, O_WRONLY|O_CREAT|O_TRUNC, 0o600);
  if (fo == -1) {
    conwriteln("failed to create file: '", fname, ".$$$'");
    return false;
  }

  bool waserror = false;
  try {
    auto or = OutRng(fo);
    v.txtser(or, skipstname:true);
    or.flush();
    waserror = or.waserror;
  } catch (Exception e) {
    waserror = true;
  }

  if (!waserror) fdatasync(fo);
  close(fo);
  if (waserror) {
    unlink(tmpfname);
    return false;
  }

  if (rename(tmpfname, fname.tempCString) != 0) {
    unlink(tmpfname);
    return false;
  }

  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
/*
  log format (it looks like text, but it isn't text):
  T[YYYY/MM/DD HH:NN:SS]: text-in-utf8
  date in local time.
  text: chars in range of [0..31, 92, 127] are \xHH-encoded
  T is message type:
    !: server/app notification
    <: outgoing
    >: incoming
  if char before text is '!' instead of space, this is "/me" message
*/
static struct LogFile {
public:
  // this struct must not outlive LogFile
  static struct Msg {
    enum Kind { Notification, Incoming, Outgoing }
    Kind kind;
    bool isMe; // "/me" message?
    SysTime time;
    char[] rawText; // undecoded

    // get decoded text
    string text () const {
      import iv.utfutil : utf8Encode;
      char[] text;
      text.reserve(rawText.length);
      // decode text
      foreach (dchar dc; byDChar) {
        char[4] buf = void;
        auto len = utf8Encode(buf[], dc);
        text ~= buf[0..len];
      }
      return cast(string)text; // it is safe to cast here
    }

    // decode raw text to dchars; returns forward range
    auto byDChar () const nothrow @safe @nogc {
      static struct Range {
      nothrow @trusted @nogc:
        const(char)[] raw; // anchored
        usize pos;
        pure @property bool empty () const => (pos >= raw.length);
        @property dchar front () const {
          if (pos < raw.length) {
            char ch = raw.ptr[pos];
            if (ch <= 127) {
              if (ch != '\\') return ch;
              if (raw.length-pos < 2) return ch;
              ch = raw.ptr[pos+1];
              if (ch != 'x' && ch != 'X') {
                switch (ch) {
                  case 'e': return '\e';
                  case 'n': return '\n';
                  case 'r': return '\r';
                  case 't': return '\t';
                  default:
                }
                return ch;
              }
              if (raw.length-pos < 4) return '?';
              int n0 = raw.ptr[pos+2].digitInBase(16);
              if (n0 < 0) return '?';
              int n1 = raw.ptr[pos+3].digitInBase(16);
              if (n1 < 0) return '?';
              return cast(char)(n0*16+n1);
            }
            Utf8DecoderFast dc;
            uint cpos = cast(uint)pos;
            while (!dc.decode(cast(ubyte)raw.ptr[cpos++])) {
              if (cpos >= raw.length) break; // no more chars
            }
            return (dc.complete ? dc.codepoint : dc.replacement);
          } else {
            return Utf8DecoderFast.replacement;
          }
        }
        void popFront () {
          if (pos < raw.length) {
            char ch = raw.ptr[pos];
            if (ch <= 127) {
              ++pos;
              if (ch != '\\') return;
              if (raw.length-pos < 1) { pos = raw.length; return; }
              ch = raw.ptr[pos++];
              if (ch != 'x' && ch != 'X') return;
              if (raw.length-pos < 2) { pos = raw.length; return; }
              pos += 2;
              return;
            }
            Utf8DecoderFast dc;
            while (!dc.decode(cast(ubyte)raw.ptr[pos++])) {
              if (pos >= raw.length) break; // no more chars
            }
          }
        }
        Range save () const => Range(raw, pos);
      }
      return Range(rawText, 0);
    }
  }

private:
  char[] data;

public:
  Msg[] messages;

public:
  @disable this (this); // no copies
  void opAssign() (in auto ref LogFile lf) { static assert(0, "no copies!"); }

  this (const(char)[] fname) nothrow => load(fname);

  // no `Msg` should outlive this call!
  void clear () nothrow {
    delete messages;
    delete data;
  }

  void load (const(char)[] fname) nothrow {
    import std.internal.cstring;

    if (messages.length) {
      messages.length = 0;
      messages.assumeSafeAppend;
    }
    if (data.length) {
      data.length = 0;
      data.assumeSafeAppend;
    }

    if (fname.length == 0) return;

    try {
      auto fi = VFile(fname);
      if (fi.size > int.max/8) assert(0, "log file too big");
      data.length = cast(int)fi.size;
      fi.rawReadExact(data);
    } catch (Exception e) {}

    parseData();
  }

  // `data` should be set
  private void parseData () nothrow {
    import core.stdc.string : memchr;

    assert(messages.length == 0); // sorry
    if (data.length == 0) return;

    // count '\n'
    int nlcount = 0;
    {
      auto dp = data.ptr;
      while (dp < data.ptr+data.length) {
        auto nx = cast(char*)memchr(dp, '\n', data.length-(dp-data.ptr));
        if (nx is null) break;
        ++nlcount;
        dp = nx+1;
      }
    }
    messages.reserve(nlcount+1);

    static int toInt (const(char)[] s) nothrow @trusted @nogc {
      int res = 0;
      while (s.length && s.ptr[0].isdigit) {
        res = res*10+s.ptr[0]-'0';
        s = s[1..$];
      }
      return res;
    }

    // parse messages
    auto dp = data.ptr, nx = data.ptr+data.length;
    for (; dp < data.ptr+data.length; dp = nx+1) {
      nx = cast(char*)memchr(dp, '\n', data.length-(dp-data.ptr));
      if (nx is null) nx = data.ptr+data.length;
      // 25 is the minimal message length
      if (nx-dp < 25) continue;
      if (dp[1] != '[' || dp[6] != '/' || dp[9] != '/' || dp[12] != ' ' || dp[15] != ':' || dp[18] != ':' || dp[21] != ']' || dp[22] != ':') continue;
      foreach (immutable cidx, immutable char ch; dp[0..21]) {
        if (cidx < 2 || cidx == 6 || cidx == 9 || cidx == 12 || cidx == 15 || cidx == 18) continue;
        if (!ch.isdigit) continue;
      }
      Msg msg;
      // message kind
      switch (dp[0]) {
        case '!': msg.kind = msg.Kind.Notification; break;
        case '<': msg.kind = msg.Kind.Outgoing; break;
        case '>': msg.kind = msg.Kind.Incoming; break;
        default: continue;
      }
      // "/me"?
      switch (dp[23]) {
        case ' ': msg.isMe = false; break;
        case '!': msg.isMe = true; break;
        default: continue;
      }
      int year = toInt(dp[2..6]);
      if (year < 2018 || year > 2036) continue; //FIXME in 2036! ;-)
      int month = toInt(dp[7..9]);
      if (month < 1 || month > 12) continue;
      int day = toInt(dp[10..12]);
      if (day < 1 || day > 31) continue;
      int hour = toInt(dp[13..15]);
      if (hour < 0 || hour > 23) continue;
      int minute = toInt(dp[16..18]);
      if (minute < 0 || minute > 59) continue;
      int second = toInt(dp[19..21]);
      if (second < 0 || second > 59) continue;
      bool timeok = false;
      try {
        msg.time = SysTime(DateTime(year, month, day, hour, minute, second));
        timeok = true;
      } catch (Exception e) {}
      if (!timeok) continue;
      msg.rawText = dp[24..(nx-dp)];
      messages ~= msg;
    }
  }

public:
  // append line to log file; text is in utf8
  // WARNING! does no validity checks!
  // WARNING! not thread-safe!
  static bool appendLine (const(char)[] fname, LogFile.Msg.Kind kind, const(char)[] text, bool isMe, SysTime time) nothrow {
    import core.sys.posix.fcntl : open, O_WRONLY, O_CREAT, O_APPEND;
    import core.sys.posix.unistd : close, write, fdatasync;
    import std.internal.cstring;

    if (fname.length == 0) return false;

    int fo = open(fname.tempCString, O_WRONLY|O_APPEND|O_CREAT, 0o600);
    if (fo == -1) {
      import core.stdc.errno;
      auto err = errno;
      conwriteln("failed to create file: '", fname, "' (", err, ")");
      return false;
    }
    scope(exit) { fdatasync(fo); close(fo); }

    bool xwrite (const(char)[] s...) nothrow @nogc {
      while (s.length > 0) {
        auto wr = write(fo, s.ptr, s.length);
        if (wr == 0) return false; // out of disk space (and the log is probably corrupted at this point)
        if (wr < 0) {
          import core.stdc.errno;
          if (errno != EINTR) return false;
          continue;
        }
        s = s[wr..$];
      }
      return true;
    }

    bool wrnum (int n, int wdt) nothrow @nogc {
      import core.stdc.stdio : snprintf;
      if (n < 0) return false; // alas
      char[128] buf = void;
      auto len = (wdt > 0 ? snprintf(buf.ptr, buf.length, "%0*d", wdt, n) : snprintf(buf.ptr, buf.length, "%d", n));
      if (len < 1) return false;
      return xwrite(buf[0..len]);
    }

    final switch (kind) {
      case LogFile.Msg.Kind.Notification: if (!xwrite("!")) return false; break;
      case LogFile.Msg.Kind.Incoming: if (!xwrite(">")) return false; break;
      case LogFile.Msg.Kind.Outgoing: if (!xwrite("<")) return false; break;
    }

    // write date
    auto dt = cast(DateTime)time;
    if (!xwrite("[")) return false;
    if (!wrnum(dt.year, 4)) return false;
    if (!xwrite("/")) return false;
    if (!wrnum(cast(int)dt.month, 2)) return false;
    if (!xwrite("/")) return false;
    if (!wrnum(dt.day, 2)) return false;
    if (!xwrite(" ")) return false;
    if (!wrnum(dt.hour, 2)) return false;
    if (!xwrite(":")) return false;
    if (!wrnum(dt.minute, 2)) return false;
    if (!xwrite(":")) return false;
    if (!wrnum(dt.second, 2)) return false;
    if (!xwrite("]:")) return false;
    if (!xwrite(isMe ? "!" : " ")) return false;

    // write encoded string
    while (text.length) {
      usize end = 0;
      while (end < text.length) {
        char ch = text.ptr[end];
        if (ch < ' ' || ch == 92 || ch == 127) break;
        ++end;
      }
      if (end > 0) {
        if (!xwrite(text[0..end])) return false;
        text = text[end..$];
      } else {
        switch (text.ptr[0]) {
          case '\e': if (!xwrite(`\e`)) return false; break;
          case '\n': if (!xwrite(`\n`)) return false; break;
          case '\r': if (!xwrite(`\r`)) return false; break;
          case '\t': if (!xwrite(`\t`)) return false; break;
          default:
            import core.stdc.stdio : snprintf;
            char[16] buf = void;
            auto len = snprintf(buf.ptr, buf.length, "\\x%02x", cast(uint)text.ptr[0]);
            if (!xwrite(buf[0..len])) return false;
            break;
        }
        text = text[1..$];
      }
    }

    return xwrite("\n");
  }

  // append line to log file; text is in utf8
  // WARNING! does no validity checks!
  static bool appendLine (const(char)[] fname, LogFile.Msg.Kind kind, const(char)[] text, bool isMe=false) nothrow {
    try {
      return appendLine(fname, kind, text, isMe, systimeNow);
    } catch (Exception e) {}
    return false;
  }
}
