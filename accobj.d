/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module accobj is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.strex;
import iv.lockfile;
import iv.tox;
import iv.txtser;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

import accdb;
import toxproto;
import popups;

import tkmain;
import tklog;
import tkminiedit;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool optShowOffline = false;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string accBaseDir = ".";
__gshared Contact activeContact;


private bool decodeHexStringInto (ubyte[] dest, const(char)[] str) {
  dest[] = 0;
  int dpos = 0;
  foreach (immutable char ch; str) {
    if (ch <= ' ' || ch == '_' || ch == '.' || ch == ':') continue;
    int dig = -1;
         if (ch >= '0' && ch <= '9') dig = ch-'0';
    else if (ch >= 'A' && ch <= 'F') dig = ch-'A'+10;
    else if (ch >= 'a' && ch <= 'f') dig = ch-'a'+10;
    else return false;
    if (dpos >= dest.length*2) return false;
    if (dpos%2 == 0) dest[dpos/2] = cast(ubyte)(dig<<4); else dest[dpos/2] |= cast(ubyte)dig;
    ++dpos;
  }
  return (dpos == dest.length*2);
}


public PubKey decodePubKeyStr (const(char)[] str) {
  PubKey res;
  if (!decodeHexStringInto(res[], str)) res[] = toxCoreEmptyKey[];
  return res;
}


public ToxAddr decodeAddrStr (const(char)[] str) {
  ToxAddr res = 0;
  if (!decodeHexStringInto(res[], str)) res[] = toxCoreEmptyAddr[];
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
final class Group {
private:
  this (Account aOwner) nothrow { acc = aOwner; }

private:
  bool mDirty; // true: write contact's config
  string diskFileName;

public:
  Account acc;
  GroupOptions info;

private:
  bool getTriOpt(string fld, string fld2=null) () const nothrow @trusted @nogc {
    enum lo = "info."~fld;
    if (mixin(lo) != TriOption.Default) return (mixin(lo) == TriOption.Yes);
    static if (fld2.length) return mixin("acc.info."~fld2); else return mixin("acc.info."~fld);
  }

  int getIntOpt(string fld) () const nothrow @trusted @nogc {
    enum lo = "info."~fld;
    if (mixin(lo) >= 0) return mixin(lo);
    return mixin("acc.info."~fld);
  }

public:
  void markDirty () pure nothrow @safe @nogc => mDirty = true;

  void save () {
    import std.path : dirName;
    // save this contact
    assert(acc !is null);
    // create disk name
    if (diskFileName.length == 0) {
      diskFileName = acc.basePath~"/contacts/groups.rc";
    }
    mkdirRec(diskFileName.dirName);
    if (serialize(info, diskFileName)) mDirty = false;
  }

  @property bool visible () const nothrow @trusted @nogc {
    if (!hideIfNoVisibleMembers) return true; // always visible
    // check if we have any visible members
    foreach (const(Contact) c; acc.contacts.byValue) {
      if (c.gid != info.gid) continue;
      if (c.visibleNoGroupCheck) return true;
    }
    return false; // nobody's here
  }

  @property nothrow @safe {
    uint gid () const pure @nogc => info.gid;

    bool opened () const @nogc => info.opened;
    void opened (bool v) @nogc { pragma(inline, true); if (info.opened != v) { info.opened = v; markDirty(); } }

    string name () const @nogc => info.name;
    void name (string v) @nogc { pragma(inline, true); if (v.length == 0) v = "<unnamed>"; if (info.name != v) { info.name = v; markDirty(); } }

    string note () const @nogc => info.note;
    void note (string v) @nogc { pragma(inline, true); if (info.note != v) { info.note = v; markDirty(); } }

    @nogc {
      bool showOffline () const => getTriOpt!"showOffline";
      bool showPopup () const => getTriOpt!"showPopup";
      bool blinkActivity () const => getTriOpt!"blinkActivity";
      bool skipUnread () const => getTriOpt!"skipUnread";
      bool hideIfNoVisibleMembers () const => getTriOpt!("hideIfNoVisibleMembers", "hideEmptyGroups");
      bool ftranAllowed () const => getTriOpt!"ftranAllowed";
      int resendRotDays () const => getIntOpt!"resendRotDays";
      int hmcOnOpen () const => getIntOpt!"hmcOnOpen";
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Contact {
public:
  // `Connecting` for non-account means "awaiting authorization"

  static struct XMsg {
    bool isMe; // "/me" message?
    SysTime time;
    string text;
    long msgid; // ==0: unknown yet
    int resendCount = 0;
    MonoTime resendAllowTime = MonoTime.zero;

    string textOrig () const pure nothrow @trusted @nogc {
      int pos = 0;
      while (pos < text.length && text.ptr[pos] != ']') ++pos;
      pos += 2;
      return (pos < text.length ? text[pos..$] : null);
    }

    bool isOurMark (const(void)[] atext, SysTime atime) nothrow @trusted {
      pragma(inline, true);
      return (atime == time && textDigest(textOrig) == textDigest(atext));
    }

    bool isOurMark (long aid, const(void)[] atext, SysTime atime) nothrow @trusted {
      pragma(inline, true);
      return (aid > 0 ? (msgid == aid) : (atime == time && textDigest(textOrig) == textDigest(atext)));
    }

    bool isOurMark (long aid, in ref TextDigest adigest, SysTime atime) nothrow @trusted {
      pragma(inline, true);
      return (aid > 0 ? (msgid == aid) : (atime == time && textDigest(textOrig) == adigest));
    }
  }

private:
  MonoTime nextSendTime; // for resend queue

private:
  this (Account aOwner) nothrow { acc = aOwner; edit = new MiniEdit(); }

  void removeData () nothrow {
    if (diskFileName.length == 0) return;
    try {
      import std.file : rmdirRecurse, rename;
      import std.path : dirName;
      auto dn = diskFileName.dirName;
      if (dn.length == 0 || dn == "/") return; // just in case
      conwriteln("removing dir <", dn, ">");
      rmdirRecurse(dn);
      //rename(dn, "_"~dn);
      diskFileName = null;
      mDirty = false;
    } catch (Exception e) {}
  }

private:
  bool mDirty; // true: write contact's config

public:
  Account acc;
  string diskFileName;
  ContactInfo info;
  ContactStatus status = ContactStatus.Offline; // not saved, so if it safe to change it
  MiniEdit edit;
  XMsg[] resendQueue;
  int unreadCount;

public:
  void markDirty () pure nothrow @safe @nogc => mDirty = true;

  void loadUnreadCount () nothrow {
    assert(diskFileName.length);
    assert(acc !is null);
    try {
      import std.path : dirName;
      auto fi = VFile(diskFileName.dirName~"/logs/unread.dat");
      unreadCount = fi.readNum!int;
    } catch (Exception e) {
      unreadCount = 0;
    }
  }

  void saveUnreadCount () nothrow {
    assert(diskFileName.length);
    assert(acc !is null);
    try {
      import std.path : dirName;
      auto fo = VFile(diskFileName.dirName~"/logs/unread.dat", "w");
      fo.writeNum(unreadCount);
    } catch (Exception e) {
    }
  }

  void loadResendQueue () {
    import std.path : dirName;
    string fname = diskFileName.dirName~"/logs/resend.log";
    LogFile lf;
    lf.load(fname);
    foreach (const ref lmsg; lf.messages) {
      XMsg xmsg;
      xmsg.isMe = lmsg.isMe;
      xmsg.time = lmsg.time;
      xmsg.text = lmsg.text;
      xmsg.msgid = 0;
      resendQueue ~= xmsg;
    }
    nextSendTime = MonoTime.currTime;
  }

  void saveResendQueue () {
    import std.file : remove;
    import std.path : dirName;
    assert(diskFileName.length);
    assert(acc !is null);
    mkdirRec(diskFileName.dirName~"/logs");
    string fname = diskFileName.dirName~"/logs/resend.log";
    try { remove(fname); } catch (Exception e) {}
    if (resendQueue.length) {
      foreach (const ref msg; resendQueue) {
        LogFile.appendLine(fname, LogFile.Msg.Kind.Outgoing, msg.text, msg.isMe, msg.time);
      }
    }
  }

  void save () {
    import std.path : dirName;
    // save this contact
    assert(acc !is null);
    // create disk name
    if (diskFileName.length == 0) {
      diskFileName = acc.basePath~"/contacts/"~tox_hex(info.pubkey[])~"/config.rc";
      acc.contacts[info.pubkey] = this;
    }
    mkdirRec(diskFileName.dirName);
    mkdirRec(diskFileName.dirName~"/avatars");
    mkdirRec(diskFileName.dirName~"/files");
    mkdirRec(diskFileName.dirName~"/fileparts");
    mkdirRec(diskFileName.dirName~"/logs");
    saveUnreadCount();
    if (serialize(info, diskFileName)) mDirty = false;
  }

  // save if dirty
  void update () {
    if (mDirty) save();
  }

public:
  @property bool visibleNoGroupCheck () const nothrow @trusted @nogc => (!kfd && (optShowOffline || showOffline || acceptPending || requestPending || status != ContactStatus.Offline || unreadCount > 0));

  @property bool visible () const nothrow @trusted @nogc {
    if (kfd) return false;
    if (acceptPending || requestPending) return true;
    if (unreadCount > 0) return true;
    if (!showOffline && !optShowOffline && status == ContactStatus.Offline) return false;
    auto grp = acc.groupById(gid);
    return (grp.visible && grp.opened);
  }

  @property nothrow @safe {
    bool online () const pure @nogc => (status != ContactStatus.Offline && status != ContactStatus.Connecting);

    ContactInfo.Kind kind () const @nogc => info.kind;
    void kind (ContactInfo.Kind v) @nogc { pragma(inline, true); if (info.kind != v) { info.kind = v; markDirty(); } }

    uint gid () const @nogc => info.gid;
    void gid (uint v) @nogc { pragma(inline, true); if (info.gid != v) { info.gid = v; markDirty(); } }

    string nick () const @nogc => info.nick;
    void nick (string v) @nogc { pragma(inline, true); if (info.nick != v) { info.nick = v; markDirty(); } }

    string visnick () const @nogc => info.visnick;
    void visnick (string v) @nogc { pragma(inline, true); if (info.visnick != v) { info.visnick = v; markDirty(); } }

    string displayNick () const @nogc => (info.visnick.length ? info.visnick : (info.nick.length ? info.nick : "<unknown>"));

    string statusmsg () const @nogc => info.statusmsg;
    void statusmsg (string v) @nogc { pragma(inline, true); if (info.statusmsg != v) { info.statusmsg = v; markDirty(); } }

    bool kfd () const @nogc => (info.kind == ContactInfo.Kind.KillFuckDie);
    bool friend () const @nogc => (info.kind == ContactInfo.Kind.Friend);
    bool acceptPending () const @nogc => (info.kind == ContactInfo.Kind.PengingAuthAccept);
    bool requestPending () const @nogc => (info.kind == ContactInfo.Kind.PengingAuthRequest);

    void setLastOnlineNow () {
      try {
        auto ut = systimeNow.toUnixTime();
        if (info.lastonlinetime != cast(uint)ut) { info.lastonlinetime = cast(uint)ut; markDirty(); }
      } catch (Exception e) {}
    }

    string note () const @nogc => info.note;
    void note (string v) @nogc { pragma(inline, true); if (info.note != v) { info.note = v; markDirty(); } }
  }

  private bool getTriOpt(string fld) () const nothrow @trusted @nogc {
    enum lo = "info.opts."~fld;
    if (mixin(lo) != TriOption.Default) return (mixin(lo) == TriOption.Yes);
    auto grp = acc.groupById(info.gid);
    enum go = "grp.info."~fld;
    if (mixin(go) != TriOption.Default) return (mixin(go) == TriOption.Yes);
    return mixin("acc.info."~fld);
  }

  private void setTriOpt(string fld) (TriOption val) {
    enum lo = "info.opts."~fld;
    if (mixin(lo) != val) {
      mixin("info.opts."~fld~" = val;");
      markDirty();
      update();
    }
  }

  private int getIntOpt(string fld) () const nothrow @trusted @nogc {
    enum lo = "info.opts."~fld;
    if (mixin(lo) >= 0) return mixin(lo);
    auto grp = acc.groupById(info.gid);
    enum go = "grp.info."~fld;
    if (mixin(go) >= 0) return mixin(go);
    return mixin("acc.info."~fld);
  }

  @property nothrow @safe @nogc {
    bool showOffline () const => !kfd && getTriOpt!"showOffline";
    bool showPopup () const => !kfd && getTriOpt!"showPopup";
    bool blinkActivity () const => !kfd && getTriOpt!"blinkActivity";
    bool skipUnread () const => kfd || getTriOpt!"skipUnread";
    bool ftranAllowed () const => !kfd && getTriOpt!"ftranAllowed";
    int resendRotDays () const => getIntOpt!"resendRotDays";
    int hmcOnOpen () const => getIntOpt!"hmcOnOpen";
  }

  @property void showOffline (TriOption v) {
    setTriOpt!"showOffline"(v);
  }

  void loadLogInto (ref LogFile lf) {
    import std.file : exists;
    import std.path : dirName;
    string lname = diskFileName.dirName~"/logs/hugelog.log";
    if (lname.exists) lf.load(lname); else lf.clear();
  }

  void appendToLog (LogFile.Msg.Kind kind, const(char)[] text, bool isMe, SysTime time) {
    import std.path : dirName;
    string lname = diskFileName.dirName~"/logs/hugelog.log";
    LogFile.appendLine(lname, kind, text, isMe, time);
  }

  void ackReceived (long msgid) {
    if (msgid <= 0) return; // wtf?!
    bool changed = false;
    usize idx = 0;
    while (idx < resendQueue.length) {
      if (resendQueue[idx].msgid == msgid) {
        foreach (immutable c; idx+1..resendQueue.length) resendQueue[c-1] = resendQueue[c];
        resendQueue[$-1] = XMsg.init;
        resendQueue.length -= 1;
        resendQueue.assumeSafeAppend;
        changed = true;
      } else {
        ++idx;
      }
    }
    if (changed) {
      saveResendQueue();
      // resend next message
      if (resendQueue.length) processResendQueue(forced:true);
    }
  }

  // <0: not found
  long findInResendQueue (const(char)[] text, SysTime time) {
    foreach (ref XMsg msg; resendQueue) {
      if (msg.time == time && msg.textOrig == text) {
        //conwriteln("<", text, "> found in resend queue; id=", msg.msgid, "; rt=<", msg.text, ">");
        return msg.msgid;
      } /*else if (msg.textOrig == text) {
        conwriteln("<", text, "> (", time, ":", msg.time, ") IS NOT: id=", msg.msgid, "; rt=<", msg.text, ">");
      }*/
    }
    return -1;
  }

  bool removeFromResendQueue (long msgid, in ref TextDigest digest, SysTime time) {
    bool doSave = false;
    usize pos = 0;
    while (pos < resendQueue.length) {
      if (resendQueue[pos].isOurMark(msgid, digest, time)) {
        foreach (immutable c; pos+1..resendQueue.length) resendQueue[c-1] = resendQueue[c];
        resendQueue[$-1] = XMsg.init;
        resendQueue.length -= 1;
        resendQueue.assumeSafeAppend;
        doSave = true;
      } else {
        ++pos;
      }
    }
    if (doSave) saveResendQueue();
    return doSave;
  }

  // called when toxcore goes offline
  void resetQueueIds () {
    foreach (ref XMsg msg; resendQueue) msg.msgid = 0;
    if (activeContact is this) logResetAckMessageIds();
    nextSendTime = MonoTime.currTime+10.seconds;
  }

  void processResendQueue (bool forced=false) {
    if (status == ContactStatus.Offline || status == ContactStatus.Connecting) return;
    if (forced) conwriteln("force resending for ", info.nick);
    auto ctt = MonoTime.currTime;
    if (!forced) {
      if (nextSendTime > ctt) return; // oops
    }
    // resend auth request?
    if (acceptPending) {
      if (isValidAddr(info.fraddr)) {
        string msg = info.statusmsg;
        if (msg.length == 0) msg = "I brought you a tasty fish!";
        if (!toxCoreSendFriendRequest(acc.toxpk, info.fraddr, msg)) { conwriteln("address: '", tox_hex(info.fraddr), "'; error sending friend request"); }
      }
    } else {
      bool doSave = false;
      if (forced) {
        foreach (ref XMsg msg; resendQueue) msg.resendAllowTime = ctt;
      }
      foreach (ref XMsg msg; resendQueue) {
        if (forced || msg.resendAllowTime <= ctt) {
          if (forced) conwriteln("  ...resending for ", info.nick);
          long msgid = toxCoreSendMessage(acc.toxpk, info.pubkey, msg.text, msg.isMe);
          if (msgid < 0) break;
          if (activeContact is this) logFixAckMessageId(msg.msgid, msgid, msg.textOrig, msg.time);
          msg.msgid = msgid;
          msg.resendAllowTime = ctt+(forced ? 2.seconds : 10.seconds);
          if (msg.resendCount++ != 0) doSave = true;
          // do not resend more than one message at a time, so receiver will get them in order.
          // ack processor will call us again in forced more if there's more messages to resend.
          // TODO: introduce a flag to avoid resending until connection lost, or got an ack.
          break;
        }
      }
      if (doSave) saveResendQueue();
    }
    //nextSendTime = MonoTime.currTime+(forced ? 5.seconds : 30.seconds);
    nextSendTime = MonoTime.currTime+30.seconds; // this should be enough to get an ack (i hope)
  }

  void send (const(char)[] text) {
    /*
    static bool isWordBoundary (char ch) {
      if (ch <= ' ') return true;
      if (ch >= 127) return false;
      if (ch >= 'A' && ch <= 'Z') return false;
      if (ch >= 'a' && ch <= 'z') return false;
      if (ch >= '0' && ch <= '9') return false;
      return true;
    }
    */

    void sendOne (const(char)[] text, bool action) {
      if (text.length == 0) return; // just in case

      SysTime now = systimeNow;
      long msgid = toxCoreSendMessage(acc.toxpk, info.pubkey, text, action);
      if (msgid < 0) { conwriteln("ERROR sending message to '", info.nick, "'"); return; }

      // add this to resend queue
      XMsg xmsg;
      xmsg.isMe = action;
      xmsg.time = now;
      xmsg.msgid = msgid; // 0: we are offline
      xmsg.resendCount = 0;

      auto ctt = MonoTime.currTime;
      if (nextSendTime <= ctt) nextSendTime = ctt+30.seconds;
      xmsg.resendAllowTime = ctt+60.seconds;

      {
        import std.datetime;
        import std.format : format;
        auto dt = cast(DateTime)now;
        xmsg.text = "[%04u/%02u/%02u %02u:%02u:%02u] %s".format(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, text);
      }

      resendQueue ~= xmsg;

      if (activeContact is this) addTextToLog(acc, this, LogFile.Msg.Kind.Outgoing, action, text, now, msgid);
      appendToLog(LogFile.Msg.Kind.Outgoing, text, action, now);
    }

    bool action = text.startsWith("/me ");
    if (action) text = text[4..$].xstripleft;

    while (text.length) {
      auto ep = text.indexOf('\n');
      if (ep < 0) ep = text.length; else ++ep; // include '\n'
      // remove line if it contains only spaces
      bool hasNonSpace = false;
      foreach (immutable char ch; text[0..ep]) if (ch > ' ') { hasNonSpace = true; break; }
      if (hasNonSpace) break;
      text = text[ep..$];
    }
    while (text.length && text[$-1] <= ' ') text = text[0..$-1];
    if (text.length == 0) return; // nothing to do

    // now split it
    //TODO: split at word boundaries
    enum ReservedSpace = 32+3+3; // 32 is for date/time stamp
    const int maxmlen = tox_max_message_length()-ReservedSpace;
    assert(maxmlen > 0);

    // remove leading spaces if we have to split the text
    while (text.length > maxmlen && text[0] <= ' ') text = text[1..$];

    if (text.length <= maxmlen) {
      sendOne(text, action);
    } else {
      // k8: toxcore developers are idiots, so we have to do dynalloc here
      auto tmpbuf = new char[](maxmlen+ReservedSpace+64);
      scope(exit) delete tmpbuf;

      bool first = true;
      assert(text[0] > ' ');
      while (text.length) {
        // skip trailing spaces
        if (!first && text[0] <= ' ') { text = text[1..$]; continue; }
        // find word boundary
        int epos = maxmlen;
        if (epos < text.length) {
          // go back, remember last word start
          while (epos > 0 && text[epos-1] > ' ') --epos;
          if (epos == 0) {
            // find utf start
            epos = maxmlen;
            while (epos > 0) {
              --epos;
              if (text[epos] < 128 || (text[epos]&0xc0) == 0xc0) break;
            }
            if (epos == 0) epos = maxmlen; // meh
          } else {
            // remove trailing spaces
            while (epos > 0 && text[epos-1] <= ' ') --epos;
            // we removed trailing spaces, so there should be at least one non-space char
            assert(epos > 0);
          }
        } else {
          epos = cast(int)text.length;
        }
        assert(epos > 0);
        if (first && epos >= text.length) {
          sendOne(text[0..epos], action);
        } else {
          int ofs = 0;
          if (!first) { tmpbuf[0..3] = "..."; ofs = 3; }
          tmpbuf[ofs..ofs+epos] = text[0..epos];
          if (epos < text.length) {
            tmpbuf[ofs+epos..ofs+epos+3] = "...";
            sendOne(tmpbuf[0..ofs+epos+3], action);
          } else {
            sendOne(tmpbuf[0..ofs+epos], action);
          }
        }
        first = false;
        text = text[epos..$];
      }
    }

    //saveResendQueue();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Account {
public:
  void saveGroups () nothrow {
    import std.algorithm : sort;
    GroupOptions[] glist;
    scope(exit) delete glist;
    foreach (Group g; groups) glist ~= g.info;
    glist.sort!((in ref a, in ref b) => a.gid < b.gid);
    glist.serialize(basePath~"contacts/groups.rc");
  }

  void saveUpdatedGroups () {
    bool needToUpdate = false;
    foreach (Group g; groups) if (g.mDirty) { needToUpdate = true; g.mDirty = false; }
    if (needToUpdate) saveGroups();
  }

private:
  ContactStatus mStatus = ContactStatus.Offline;

public:
  PubKey toxpk = toxCoreEmptyKey;
  string toxDataDiskName;
  string basePath; // with trailing "/"
  ProtoOptions protoOpts;
  AccountConfig info;
  Group[] groups;
  Contact[PubKey] contacts;

public:
  bool mIAmConnecting = false;
  bool mIAmOnline = false;
  bool forceOnline = true; // set to `false` to stop autoreconnecting

public:
  @property bool isConnecting () const nothrow @safe @nogc => mIAmConnecting;

  @property bool isOnline () const nothrow @safe @nogc {
    if (!toxpk.isValidKey) return false;
    if (mIAmConnecting) return false;
    if (!mIAmOnline) return false;
    return true;
  }

  @property ContactStatus status () const nothrow @safe @nogc {
    if (!toxpk.isValidKey) return ContactStatus.Offline;
    if (mIAmConnecting) return ContactStatus.Connecting;
    if (!mIAmOnline) return ContactStatus.Offline;
    return mStatus;
  }

  @property void status (ContactStatus v) {
    if (!toxpk.isValidKey) return;
    conwriteln("changing status to ", v, " (old: ", mStatus, ")");
    if (v == ContactStatus.Connecting) v = ContactStatus.Online;
    forceOnline = (v != ContactStatus.Offline);
    if (mStatus == ContactStatus.Offline) {
      mIAmConnecting = (v != ContactStatus.Offline);
      mIAmOnline = false; // just in case
    }
    toxCoreSetStatus(toxpk, v);
    mStatus = v;
    // if not reconnecting, make all contacts offline
    if (v == ContactStatus.Offline && !mIAmConnecting) {
      foreach (Contact ct; this) ct.status = ContactStatus.Offline;
    }
    glconPostScreenRepaint();
    fixTrayIcon();
  }

  void processResendQueue () {
    if (!isOnline) return;
    foreach (Contact ct; contacts.byValue) { ct.processResendQueue(); ct.update(); }
    saveUpdatedGroups();
  }

  void saveResendQueue () {
    foreach (Contact ct; contacts.byValue) { ct.saveResendQueue(); ct.update(); }
    saveUpdatedGroups();
  }

  string getAddress () {
    if (!toxpk.isValidKey) return null;
    return tox_hex(toxCoreGetSelfAddress(toxpk));
  }

private:
  void toxCreate () {
    toxpk = toxCoreOpenAccount(toxDataDiskName);
    if (!toxpk.isValidKey) {
      conwriteln("creating new Tox account...");
      string nick = info.nick;
      if (nick.length > 0) {
        //FIXME: utf
        if (nick.length > tox_max_name_length()) nick = nick[0..tox_max_name_length()];
      } else {
        nick = "anonymous";
      }
      toxpk = toxCoreCreateAccount(toxDataDiskName, nick);
      if (!toxpk.isValidKey) throw new Exception("cannot create Tox account");
    }
    conwriteln("my address: [", tox_hex(toxCoreGetSelfAddress(toxpk)), "]");
    toxLoadKnownContacts();
  }

  // load contacts from ToxCore data and add 'em to contact database
  void toxLoadKnownContacts () {
    if (!toxpk.isValidKey) return;
    version(none) {
      toxCoreForEachFriend(toxpk, delegate (in ref PubKey self, in ref PubKey frpub, scope const(char)[] anick) {
        string nick = anick.buildNormalizedString!true;
        auto c = (frpub in contacts ? contacts[frpub] : null);
        if (c is null) {
          conwriteln("NEW friend with pk [", tox_hex(frpub), "]; name is: ", nick);
          c = new Contact(this);
          c.info.gid = 0;
          c.info.nick = nick;
          c.info.pubkey[] = frpub[];
          c.info.opts.showOffline = TriOption.Default;
          auto ls = toxCoreLastSeen(self, frpub);
          if (ls != SysTime.min) {
            c.info.lastonlinetime = cast(uint)ls.toUnixTime();
          }
          if (c.info.lastonlinetime == 0 && nick.length == 0) {
            c.info.kind = ContactInfo.Kind.PengingAuthRequest;
          }
          contacts[c.info.pubkey] = c;
          c.save();
          //HACK!
          if (clist !is null) clist.buildAccount(this);
        } else {
          bool needSave = false;
          auto ls = toxCoreLastSeen(self, frpub);
          if (ls != SysTime.min) {
            auto lsu = cast(uint)ls.toUnixTime();
            if (c.info.lastonlinetime != lsu) { c.info.lastonlinetime = lsu; needSave = true; }
          }
          if (c.info.nick != nick) {
            conwriteln("OLD friend with pk [", tox_hex(frpub), "]; new name is: ", nick);
            if (c.info.nick.length == 0 && nick.length != 0) {
              needSave = true;
              c.info.nick = nick;
            }
          } else {
            conwriteln("OLD friend with pk [", tox_hex(frpub), "]; old name is: ", nick);
          }
          if (needSave) c.save();
        }
        return false; // don't stop
      });
    } else {
      try {
        auto data = toxCoreLoadDataFile(VFile(toxDataDiskName));
        foreach (const ref ci; data.friends) {
          auto c = (ci.pubkey in contacts ? contacts[ci.pubkey] : null);
          if (c is null) {
            conwriteln("NEW friend with pk [", tox_hex(ci.pubkey), "]; name is: ", ci.nick);
            c = new Contact(this);
            c.info.gid = 0;
            c.info.nick = ci.nick;
            c.info.statusmsg = ci.statusmsg;
            c.info.lastonlinetime = ci.lastonlinetime;
            c.info.kind = ci.kind;
            c.info.pubkey[] = ci.pubkey[];
            c.info.opts.showOffline = TriOption.Default;
            contacts[c.info.pubkey] = c;
            c.save();
            //HACK!
            if (clist !is null) clist.buildAccount(this);
          } else {
            bool needSave = false;
            version(none) {
              if (c.info.nick != ci.nick) {
                conwriteln("OLD friend with pk [", tox_hex(ci.pubkey), "]; new name is: ", ci.nick);
              } else {
                conwriteln("OLD friend with pk [", tox_hex(ci.pubkey), "]; old name is: ", ci.nick);
              }
            }
            if (c.info.kind != ci.kind) { c.info.kind = ci.kind; needSave = true; }
            if (c.info.lastonlinetime != ci.lastonlinetime) { c.info.lastonlinetime = ci.lastonlinetime; needSave = true; }
            if (c.info.nick.length == 0 && ci.nick.length != 0) { c.info.nick = ci.nick; needSave = true; }
            if (needSave) c.save();
          }
        }
      } catch (Exception e) {}
    }
  }

public:
  bool sendFriendRequest (in ref ToxAddr fraddr, const(char)[] msg) {
    if (!toxpk.isValidKey) return false;
    if (!isValidAddr(fraddr)) return false;
    if (msg.length == 0) return false;
    if (msg.length > tox_max_friend_request_length()) return false;
    if (fraddr[0..PubKey.length] == toxpk[]) return false; // there is no reason to friend myself
    PubKey frpub = fraddr[0..PubKey.length];
    auto c = (frpub in contacts ? contacts[frpub] : null);
    if (c !is null) { c.statusmsg = msg.idup; c.save(); }
    if (!toxCoreSendFriendRequest(toxpk, fraddr, msg)) return false;
    if (c is null) {
      c = new Contact(this);
      c.info.gid = 0;
      c.info.nick = null; // unknown yet
      c.info.pubkey[] = frpub[];
      c.info.opts.showOffline = TriOption.Default;
      c.info.kind = ContactInfo.Kind.PengingAuthRequest;
      c.info.fraddr[] = fraddr[]; // save address for resending (and just in case)
      contacts[c.info.pubkey] = c;
      c.save();
      //HACK!
      if (clist !is null) clist.buildAccount(this);
    }
    return true;
  }

private:
  // connection established
  void toxConnectionDropped () {
    alias timp = this;
    conprintfln("TOX[%s] CONNECTION DROPPED", timp.srvalias);
    mIAmConnecting = false;
    mIAmOnline = false;
    auto oldst = mStatus;
    toxCoreSetStatus(toxpk, ContactStatus.Offline); // this kills toxcore instance
    foreach (Contact ct; contacts.byValue) {
      ct.status = ContactStatus.Offline;
      if (ct.kfd) toxCoreRemoveFriend(toxpk, ct.info.pubkey);
      // this is not quite correct: we need to distinguish when user goes offline, and when toxcore temporarily goes offline
      // but we're killing toxcore instance on disconnect, so it works
      ct.resetQueueIds();
    }
    if (forceOnline) {
      mStatus = ContactStatus.Offline;
      status = oldst;
    }
    glconPostScreenRepaint();
  }

  // connection established
  void toxConnectionEstablished () {
    alias timp = this;
    conprintfln("TOX[%s] CONNECTION ESTABLISHED", timp.srvalias);
    mIAmConnecting = false;
    mIAmOnline = true;
    if (info.statusmsg.length == 0) info.statusmsg = "Come taste the gasoline! [BioAcid]";
    toxCoreSetStatusMessage(toxpk, info.statusmsg);
    //toxLoadKnownContacts();
    foreach (Contact ct; contacts.byValue) ct.resetQueueIds();
    glconPostScreenRepaint();
  }

  void toxFriendOffline (in ref PubKey fpk) {
    if (auto ct = fpk in contacts) {
      auto ls = toxCoreLastSeen(toxpk, fpk);
      if (ls != SysTime.min) {
        auto lsu = cast(uint)ls.toUnixTime();
        if (ct.info.lastonlinetime != lsu) { ct.info.lastonlinetime = lsu; ct.markDirty(); glconPostScreenRepaint(); }
      }
      if (ct.status != ContactStatus.Offline) {
        conwriteln("friend <", ct.info.nick, "> gone offline");
        ct.status = ContactStatus.Offline;
        glconPostScreenRepaint();
      }
    }
  }

  void toxSelfStatus (ContactStatus cst) {
    if (mStatus != cst) {
      mStatus = cst;
      glconPostScreenRepaint();
    }
  }

  void toxFriendStatus (in ref PubKey fpk, ContactStatus cst) {
    if (auto ct = fpk in contacts) {
      if (ct.kfd) return;
      if (ct.status != cst) {
        conwriteln("status for friend <", ct.info.nick, "> changed to ", cst);
        auto ls = toxCoreLastSeen(toxpk, fpk);
        if (ls != SysTime.min) {
          auto lsu = cast(uint)ls.toUnixTime();
          if (ct.info.lastonlinetime != lsu) { ct.info.lastonlinetime = lsu; ct.markDirty(); }
        }
        ct.status = cst;
        // if it is online, and it is not a friend, turn it into a friend
        if (ct.online && !ct.friend) ct.kind = ContactInfo.Kind.Friend;
        if (ct.online) ct.processResendQueue(true); // why not?
        glconPostScreenRepaint();
      }
    }
  }

  void toxFriendStatusMessage (in ref PubKey fpk, string msg) {
    if (auto ct = fpk in contacts) {
      msg = msg.xstrip();
      if (ct.info.statusmsg != msg) {
        conwriteln("status message for friend <", ct.info.nick, "> changed to <", msg, ">");
        ct.info.statusmsg = msg;
        ct.save();
        if (ct.showPopup) showPopup(PopupWindow.Kind.Status, ct.nick, (msg.length ? msg : "<nothing>"));
        glconPostScreenRepaint();
      }
    }
  }

  void toxFriendNickChanged (in ref PubKey fpk, string nick) {
    if (auto ct = fpk in contacts) {
      nick = nick.xstrip();
      if (nick.length && ct.info.nick != nick) {
        auto onick = ct.info.nick;
        ct.info.nick = nick;
        ct.save();
        if (ct.showPopup) showPopup(PopupWindow.Kind.Status, ct.nick, "changed nick to '"~nick~"' from '"~onick~"'");
        glconPostScreenRepaint();
      }
    }
  }

  void toxFriendReqest (in ref PubKey fpk, const(char)[] msg) {
    alias timp = this;
    conprintfln("TOX[%s] FRIEND REQUEST FROM [%s]: <%s>", timp.srvalias, tox_hex(fpk[]), msg);
    if (auto ct = fpk in contacts) {
      if (ct.kfd) return;
      // if not waiting for acceptance, force-friend it
      if (!ct.acceptPending) {
        conwriteln("force-friend <", ct.info.nick, ">");
        toxCoreAddFriend(toxpk, fpk);
      } else {
        ct.kind = ContactInfo.Kind.PengingAuthAccept;
        ct.info.statusmsg = msg.idup;
      }
      ct.setLastOnlineNow();
      ct.save();
      showPopup(PopupWindow.Kind.Info, "Friend Accepted", (msg.length ? msg : "<nothing>"));
    } else {
      // new friend request
      conwriteln("AUTH REQUEST from pk [", tox_hex(fpk), "]");
      auto c = new Contact(this);
      c.info.gid = 0;
      c.info.nick = null;
      c.info.pubkey[] = fpk[];
      c.info.opts.showOffline = TriOption.Default;
      c.info.statusmsg = msg.idup;
      c.kind = ContactInfo.Kind.PengingAuthAccept;
      contacts[c.info.pubkey] = c;
      c.setLastOnlineNow();
      c.save();
      showPopup(PopupWindow.Kind.Info, "Friend Request", (msg.length ? msg : "<nothing>"));
      //HACK!
      if (clist !is null) clist.buildAccount(this);
    }
    glconPostScreenRepaint();
  }

  void toxFriendMessage (in ref PubKey fpk, bool action, string msg, SysTime time) {
    if (auto ct = fpk in contacts) {
      bool doPopup = ct.showPopup;
      LogFile.Msg.Kind kind = LogFile.Msg.Kind.Incoming;
      ct.appendToLog(kind, msg, action, time);
      if (*ct is activeContact) {
        // if inactive or invisible, add divider line and increase unread count
        if (!mainWindowVisible || !mainWindowActive) {
          if (ct.unreadCount == 0) addDividerLine();
          ct.unreadCount += 1;
          ct.saveUnreadCount();
        } else {
          doPopup = false;
        }
        addTextToLog(this, *ct, kind, action, msg, time);
      } else {
        ct.unreadCount += 1;
        ct.saveUnreadCount();
      }
      if (doPopup) showPopup(PopupWindow.Kind.Incoming, ct.nick, msg);
    }
  }

  // ack for sent message
  void toxMessageAck (in ref PubKey fpk, long msgid) {
    alias timp = this;
    conprintfln("TOX[%s] ACK MESSAGE FROM [%s]: %u", timp.srvalias, tox_hex(fpk[]), msgid);
    if (auto ct = fpk in contacts) {
      if (*ct is activeContact) ackLogMessage(msgid);
      ct.ackReceived(msgid);
    }
  }

private:
  @property string srvalias () const pure nothrow @safe @nogc => info.nick;

  LockFile lockf;

  // false: get lost
  bool tryLockIt () {
    if (toxDataDiskName.length == 0) return false;
    lockf = LockFile(toxDataDiskName~".lock");
    if (!lockf.tryLock) {
      lockf.close();
      return false;
    }
    return true;
  }

  void createWithBaseDir (string aBaseDir) {
    import std.algorithm : sort;
    import std.file : DirEntry, SpanMode, dirEntries;
    import std.path : baseName;

    basePath = normalizeBaseDir(aBaseDir);
    toxDataDiskName = basePath~"toxdata.tox";

    if (!tryLockIt) throw new Exception("cannot activate already active account");

    protoOpts.txtunser(VFile(basePath~"proto.rc"));
    info.txtunser(VFile(basePath~"config.rc"));

    // load groups
    GroupOptions[] glist;
    glist.txtunser(VFile(basePath~"contacts/groups.rc"));
    bool hasDefaultGroup = false;
    bool hasMoronsGroup = false;
    foreach (ref GroupOptions gi; glist[]) {
      auto g = new Group(this);
      g.info = gi;
      bool found = false;
      foreach (ref gg; groups[]) if (gg.gid == g.gid) { delete gg; gg = g; found = true; }
      if (!found) groups ~= g;
      if (g.gid == 0) hasDefaultGroup = true;
      if (g.gid == g.gid.max-2) hasMoronsGroup = true;
    }

    // create default group if necessary
    if (!hasDefaultGroup) {
      GroupOptions gi;
      gi.gid = 0;
      gi.name = "default";
      gi.note = "default group for new contacts";
      gi.opened = true;
      auto g = new Group(this);
      g.info = gi;
      groups ~= g;
    }

    // create morons group if necessary
    if (!hasMoronsGroup) {
      GroupOptions gi;
      gi.gid = gi.gid.max-2;
      gi.name = "<morons>";
      gi.note = "group for completely ignored dumbfucks";
      gi.opened = false;
      gi.showOffline = TriOption.No;
      gi.showPopup = TriOption.No;
      gi.blinkActivity = TriOption.No;
      gi.skipUnread = TriOption.Yes;
      gi.hideIfNoVisibleMembers = TriOption.Yes;
      gi.ftranAllowed = TriOption.No;
      gi.resendRotDays = 0;
      gi.hmcOnOpen = 0;
      auto g = new Group(this);
      g.info = gi;
      groups ~= g;
      //saveGroups();
    }

    groups.sort!((in ref a, in ref b) => a.gid < b.gid);

    if (!hasDefaultGroup || !hasMoronsGroup) saveGroups();

    static bool isValidCDName (const(char)[] str) {
      if (str.length != 64) return false;
      foreach (immutable char ch; str) {
        if (ch >= '0' && ch <= '9') continue;
        if (ch >= 'A' && ch <= 'F') continue;
        if (ch >= 'a' && ch <= 'f') continue;
        return false;
      }
      return true;
    }

    // load contacts
    foreach (DirEntry de; dirEntries(basePath~"contacts", SpanMode.shallow)) {
      if (de.name.baseName == "." || de.name.baseName == ".." || !isValidCDName(de.name.baseName)) continue;
      try {
        import std.file : exists;
        if (!de.isDir) continue;
        string cfgfn = de.name~"/config.rc";
        if (!cfgfn.exists) continue;
        ContactInfo ci;
        ci.txtunser(VFile(cfgfn));
        auto c = new Contact(this);
        c.diskFileName = cfgfn;
        c.info = ci;
        contacts[c.info.pubkey] = c;
        c.loadResendQueue();
        c.loadUnreadCount();
        // fix contact group
        if (groupById!false(c.gid) is null) {
          c.info.gid = 0; // move to default group
          c.save();
        }
        //conwriteln("loaded contact [", c.info.nick, "] from '", c.diskFileName, "'");
      } catch (Exception e) {
        conwriteln("ERROR loading contact from '", de.name, "/config.rc'");
      }
    }

    toxCreate();
    assert(toxpk.isValidKey, "something is VERY wrong here");
    conwriteln("created ToxCore for [", tox_hex(toxpk[]), "]");
  }

public:
  this (string aBaseDir) { createWithBaseDir(aBaseDir); }

  ~this () {
    if (toxpk.isValidKey) {
      toxCoreCloseAccount(toxpk);
      toxpk[] = toxCoreEmptyKey[];
    }
  }

  // save account info (but not contacts)
  void save () {
    serialize(info, basePath~"config.rc");
  }

  // will not write contact to disk
  Contact createEmptyContact () {
    auto c = new Contact(this);
    c.info.gid = 0;
    c.info.nick = "test contact";
    c.info.pubkey[] = 0;
    return c;
  }

  // returns `null` if there is no such group, and `dofail` is `true`
  inout(Group) groupById(bool dofail=true) (uint agid) inout nothrow @nogc {
    foreach (const Group g; groups) if (g.gid == agid) return cast(inout)g;
    static if (dofail) assert(0, "group not found"); else return null;
  }

  int opApply () (scope int delegate (ref Contact ct) dg) {
    foreach (Contact ct; contacts.byValue) if (auto res = dg(ct)) return res;
    return 0;
  }

  // returns gid, or uint.max on error
  uint findGroupByName (const(char)[] name) nothrow {
    if (name.length == 0) return uint.max;
    foreach (const Group g; groups) if (g.name == name) return g.gid;
    return uint.max;
  }

  // returns gid, or uint.max on error
  uint createGroup(T:const(char)[]) (T name) nothrow {
    if (name.length == 0) return uint.max;
    // groups are sorted by gid, yeah
    uint newgid = 0;
    foreach (const Group g; groups) {
      if (g.name == name) return g.gid;
      if (newgid == g.gid) newgid = g.gid+1;
    }
    if (newgid == uint.max) return uint.max;
    // create new group
    GroupOptions gi;
    gi.gid = newgid;
    static if (is(T == string)) gi.name = name; else gi.name = name.idup;
    gi.opened = true;
    auto g = new Group(this);
    g.info = gi;
    groups ~= g;
    import std.algorithm : sort;
    groups.sort!((in ref a, in ref b) => a.gid < b.gid);
    saveGroups();
    return gi.gid;
  }

  // returns `false` on any error
  bool moveContactToGroup (Contact ct, uint gid) nothrow {
    if (ct is null || gid == uint.max) return false;
    // check if this is our contact
    bool found = false;
    foreach (Contact cc; contacts.byValue) if (cc is ct) { found = true; break; }
    if (!found) return false;
    // find group
    Group grp = null;
    foreach (Group g; groups) if (g.gid == gid) { grp = g; break; }
    if (grp is null) return false;
    // move it
    if (ct.info.gid != gid) {
      ct.info.gid = gid;
      ct.markDirty();
    }
    return true;
  }

  // returns `false` on any error
  bool removeContact (Contact ct) {
    if (ct is null || !isOnline) return false;
    // check if this is our contact
    bool found = false;
    foreach (Contact cc; contacts.byValue) if (cc is ct) { found = true; break; }
    if (!found) return false;
    toxCoreRemoveFriend(toxpk, ct.info.pubkey);
    ct.removeData();
    contacts.remove(ct.info.pubkey);
    //HACK!
    if (clist !is null) {
      if (clist.isActiveContact(ct.info.pubkey)) clist.resetActiveItem();
      clist.buildAccount(this);
    }
    /*
    ct.kind = ContactInfo.Kind.KillFuckDie;
    ct.markDirty();
    */
    return true;
  }

private:
  static string normalizeBaseDir (string aBaseDir) {
    import std.path : absolutePath, expandTilde;
    if (aBaseDir.length == 0 || aBaseDir == "." || aBaseDir[$-1] == '/') throw new Exception("invalid base dir");
    if (aBaseDir.indexOf('/') < 0) aBaseDir = "~/.bioacid/"~aBaseDir;
    aBaseDir = aBaseDir.expandTilde.absolutePath;
    if (aBaseDir[$-1] != '/') aBaseDir ~= '/';
    return aBaseDir;
  }

public:
  static Account CreateNew (string aBaseDir, string aAccName) {
    aBaseDir = normalizeBaseDir(aBaseDir);
    auto lockf = LockFile(aBaseDir~"toxdata.tox.lock");
    if (!lockf.tryLock) {
      lockf.close();
      throw new Exception("cannot create locked account");
    }
    mkdirRec(aBaseDir~"contacts");
    // write protocol options
    {
      ProtoOptions popt;
      serialize(popt, aBaseDir~"proto.rc");
    }
    // account options
    {
      AccountConfig acc;
      acc.nick = aAccName;
      acc.showPopup = true;
      acc.blinkActivity = true;
      acc.hideEmptyGroups = false;
      acc.ftranAllowed = true;
      acc.resendRotDays = 4;
      acc.hmcOnOpen = 10;
      serialize(acc, aBaseDir~"config.rc");
    }
    // create default group
    {
      GroupOptions[1] grp;
      grp[0].gid = 0;
      grp[0].name = "default";
      grp[0].opened = true;
      //grp[0].hideIfNoVisible = TriOption.Yes;
      serialize(grp[], aBaseDir~"contacts/groups.rc");
    }
    // now load it
    return new Account(aBaseDir);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void setupToxEventListener (SimpleWindow sdmain) {
  assert(sdmain !is null);

  sdmain.addEventListener((ToxEventBase evt) {
    auto acc = clist.accountByPK(evt.self);

    if (acc is null) return;

    bool fixTray = false;
    scope(exit) { if (fixTray) fixTrayIcon(); glconPostScreenRepaint(); }

    // connection?
    if (auto e = cast(ToxEventConnection)evt) {
      if (e.who[] == acc.toxpk[]) {
        if (e.connected) acc.toxConnectionEstablished(); else acc.toxConnectionDropped();
        fixTray = true;
      } else {
        if (!e.connected) acc.toxFriendOffline(e.who);
      }
      return;
    }
    // status?
    if (auto e = cast(ToxEventStatus)evt) {
      if (e.who[] == acc.toxpk[]) {
        acc.toxSelfStatus(e.status);
        fixTray = true;
      } else {
        acc.toxFriendStatus(e.who, e.status);
      }
      return;
    }
    // status message?
    if (auto e = cast(ToxEventStatusMsg)evt) {
      if (e.who[] != acc.toxpk[]) acc.toxFriendStatusMessage(e.who, e.message);
      return;
    }
    // new nick?
    if (auto e = cast(ToxEventNick)evt) {
      if (e.who[] != acc.toxpk[]) acc.toxFriendNickChanged(e.who, e.nick);
      return;
    }
    // incoming text message?
    if (auto e = cast(ToxEventMessage)evt) {
      if (e.who[] != acc.toxpk[]) {
        acc.toxFriendMessage(e.who, e.action, e.message, e.time);
        fixTray = true;
      }
      return;
    }
    // ack outgoing text message?
    if (auto e = cast(ToxEventMessageAck)evt) {
      if (e.who[] != acc.toxpk[]) acc.toxMessageAck(e.who, e.msgid);
      return;
    }
    // friend request?
    if (auto e = cast(ToxEventFriendReq)evt) {
      if (e.who[] != acc.toxpk[]) acc.toxFriendReqest(e.who, e.message);
      return;
    }
    //
    //glconProcessEventMessage();
  });
}
