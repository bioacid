/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module bioacid is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.gxx;
import iv.meta;
import iv.nanovega;
import iv.nanovega.blendish;
import iv.nanovega.textlayouter;
import iv.strex;
import iv.tox;
import iv.sdpyutil;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

version(sfnt_test) import iv.nanovega.simplefont;

import accdb;
import accobj;
import fonts;
import popups;
import icondata;
import notifyicon;
import toxproto;

import tkmain;
import tklog;
import tkclist;
import tkminiedit;


// ////////////////////////////////////////////////////////////////////////// //
//__gshared string accountNameToLoad = "_fakeacc";
__gshared string accountNameToLoad = "";
__gshared string globalHotkey = "M-H-F";
__gshared bool optAccountAllowCreate = false;
__gshared mouseStartMarking = false;


//==========================================================================
//
//  quoteText
//
//==========================================================================
string quoteText (const(char)[] text, int wrapwidth=73, const(char)[] pfx=">") {
  text = text.xstripright;
  if (text.length == 0) return null;
  string res;

  string currline;
  string currword;
  int wordwidth = 0;
  int currwidth = 0;
  bool wasnewline = true;

  Utf8DecoderFast dc;

  void flushWord (bool force) {
    if (currwidth && currwidth+wordwidth+1 > wrapwidth) {
      if (res.length) res ~= "\n";
      res ~= pfx;
      res ~= currline;
      currline = null;
      currwidth = 0;
    }
    currline ~= " ";
    currline ~= currword;
    currwidth += wordwidth+1;
    if (force) {
      if (res.length) res ~= "\n";
      res ~= pfx;
      res ~= currline;
      currline = null;
      currwidth = 0;
    }
    currword = null;
    wordwidth = 0;
  }

  void addChar (char ch) {
    if (!dc.decodeSafe(cast(ubyte)ch)) return;
    if (dc.codepoint <= 32) {
      if (wordwidth || dc.codepoint == 10) {
        flushWord(dc.codepoint == 10);
      } else {
        // just in case
        currword = null;
        wordwidth = 0;
      }
    } else {
      if (dc.codepoint == lay.SoftHyphenCh) return;
      if (dc.codepoint == lay.NBHyphenCh) return;
      if (dc.codepoint == lay.HyphenPointCh) return;
      //if (dc.codepoint == lay.HyphenCh) return;
      if (dc.codepoint == lay.WordJoinerCh) return;
      if (dc.codepoint == lay.ZWSpaceCh) return;
      if (dc.codepoint == lay.ZWNBSpaceCh) return;
      char[4] buf;
      int ulen = utf8Encode(buf[], dc.codepoint);
      if (ulen > 0) {
        currword ~= buf[0..ulen];
        wordwidth += 1;
      }
    }
  }

  //conwriteln(text);
  foreach (char ch; text) addChar(ch);
  if (wordwidth) flushWord(false);
  if (currwidth) {
    if (res.length) res ~= "\n";
    res ~= pfx;
    res ~= currline;
  }
  if (res.length) res ~= "\n";
  return res;
}


//==========================================================================
//
//  main
//
//==========================================================================
void main (string[] args) {
  if (!TOX_VERSION_IS_ABI_COMPATIBLE()) assert(0, "invalid ToxCore library version");

  conRegVar!accountNameToLoad("starting_account", "account to load");
  conRegVar!optAccountAllowCreate("account_allow_creating", "create new account if BioAcid can't load it");
  conRegVar!optShowOffline("show_offline", "always show offline contacts?");
  conRegVar!optHRLastSeen("hr_lastseen", "human-readable lastseen?");

  //conwriteln("account '", acc.info.nick, "' loaded, ", acc.contacts.length, " contacts, ", acc.groups.length, " groups");

  //glconShowKey = "M-Grave";
  glconSetAndSealFPS(0); // draw-on-demand

  conProcessQueue(256*1024); // load config
  conProcessArgs!true(args);
  conProcessQueue(256*1024);

  if (accountNameToLoad.length == 0) assert(0, "no account to load");

  //setOpenGLContextVersion(3, 2); // up to GLSL 150
  setOpenGLContextVersion(2, 0); // it's enough

  loadAllFonts();


  NVGPathSet svp = null;

  //glconRunGLWindowResizeable(800, 600, "BioAcid", "BIOACID");
  sdpyWindowClass = "BIOACID";
  sdmain = new SimpleWindow(800, 600, "BioAcid", OpenGlOptions.yes, Resizability.allowResizing);
  glconCtlWindow = sdmain;

  setupToxCoreSender();
  setupToxEventListener(sdmain);

  sdmain.visibilityChanged = delegate (bool vis) { mainWindowVisible = vis; fixUnreadIndicators(); };
  sdmain.onFocusChange = delegate (bool focused) { mainWindowActive = focused; /*conwriteln("focus=", focused);*/ fixUnreadIndicators(); };

  MiniEdit sysedit = new MiniEdit();

  MiniEdit currEdit () { return (activeContact !is null ? activeContact.edit : sysedit); }

  try {
    if (globalHotkey.length > 0) {
      GlobalHotkeyManager.register(globalHotkey, delegate () { concmd("win_toggle"); glconPostDoConCommands!true(); });
    }
  } catch (Exception e) {
    conwriteln("ERROR registering hotkey!");
  }

  conRegFunc!(() {
    if (sdmain !is null) sdmain.close();
  })("quit", "quit BioAcid");

  conRegFunc!(() {
    import core.memory : GC;
    conwriteln("starting GC collection...");
    GC.collect();
    GC.minimize();
    conwriteln("GC collection complete.");
  })("gc_collect", "force GC collection cycle");

  conRegFunc!(() {
    if (sdmain !is null && !sdmain.closed) {
      //conwriteln("act=", mainWindowActive, "; vis=", mainWindowVisible, "; realvis=", sdmain.visible);
      if (!mainWindowVisible) {
        // this strange code brings window to the current desktop it if was on a different one
        sdmain.hide();
        sdmain.show();
      } else if (sdmain.visible) {
        sdmain.hide();
      } else {
        sdmain.show();
      }
      flushGui();
    }
  })("win_toggle", "show/hide main window");

  sdmain.addEventListener((GLConScreenRepaintEvent evt) {
    if (sdmain.closed) return;
    if (isQuitRequested) { sdmain.close(); return; }
    sdmain.redrawOpenGlSceneNow();
  });

  sdmain.addEventListener((GLConDoConsoleCommandsEvent evt) {
    glconProcessEventMessage();
  });

  sdmain.addEventListener((PopupCheckerEvent evt) {
    popupCheckExpirations();
  });


  // ////////////////////////////////////////////////////////////////////// //
  sdmain.onClosing = delegate () {
    clist.forEachAccount(delegate (acc) { acc.forceOnline = false; });
    popupKillAll();
    if (nvg !is null) {
      sdmain.setAsCurrentOpenGlContext();
      scope(exit) { flushGui(); sdmain.releaseCurrentOpenGlContext(); }
      svp.kill();
      nvg.kill();
    }
    assert(nvg is null);
    if (sdhint !is null) sdhint.close();
    if (trayicon !is null) trayicon.close();
  };

  sdmain.closeQuery = delegate () {
    concmd("quit");
    glconPostDoConCommands!true();
  };

  // first time setup
  sdmain.visibleForTheFirstTime = delegate () {
    if (sdmain.width > 1 && optCListWidth < 0) optCListWidth = sdmain.width/5;
    sdmain.setAsCurrentOpenGlContext(); // make this window active
    scope(exit) sdmain.releaseCurrentOpenGlContext();
    sdmain.vsync = false;

    glconInit(sdmain.width, sdmain.height);

    nvg = nvgCreateContext(NVGContextFlag.Antialias, NVGContextFlag.StencilStrokes, NVGContextFlag.FontNoAA);
    if (nvg is null) assert(0, "cannot initialize NanoVG");
    loadFonts();

    try {
      static immutable skullsPng = /*cast(immutable(ubyte)[])*/import("data/skulls.png");
      //nvgSkullsImg = nvg.createImage("skulls.png", NVGImageFlags.NoFiltering|NVGImageFlags.RepeatX|NVGImageFlags.RepeatY);
      auto xi = loadImageFromMemory(skullsPng[]);
      scope(exit) delete xi;
      //{ import core.stdc.stdio; printf("creating background image...\n"); }
      nvgSkullsImg = nvg.createImageFromMemoryImage(xi, NVGImageFlags.NoFiltering, NVGImageFlags.RepeatX, NVGImageFlags.RepeatY);
      //{ import core.stdc.stdio; printf("background image created\n"); }
      if (!nvgSkullsImg.valid) assert(0, "cannot load background image");
    } catch (Exception e) {
      assert(0, "cannot load background image");
    }
    buildStatusImages();

    prepareTrayIcon();

    lay = new LayTextClass(laf, sdmain.width/2);
    //conwriteln("wdt=", lay.width, "; text width=", lay.textWidth);
    lastWindowWidth = sdmain.width;

    clist = new CList();
    loadAccount(accountNameToLoad, optAccountAllowCreate);
    clist.onActivateContactCB = delegate (Contact ct) { doActivateContact(ct); };
    addContactCommands();

    sdmain.setMinSize(640, 480);

    fixTrayIcon();
    //sdmain.redrawOpenGlSceneNow();
  };

  sdmain.windowResized = delegate (int wdt, int hgt) {
    if (sdmain.closed) return;
    glconResize(wdt, hgt);
    glconPostScreenRepaint/*Delayed*/();
    //conwriteln("old=", lastWindowWidth, "; new=", wdt);
    if (wdt > 1 && optCListWidth > 0 && lastWindowWidth > 0 && lastWindowWidth != wdt) {
      immutable double frc = lastWindowWidth/optCListWidth;
      optCListWidth = cast(int)(wdt/frc);
      if (optCListWidth < 64) optCListWidth = 64;
      lastWindowWidth = wdt;
    }
    lay.relayout(wdt);
  };


  int mouseX = -666, mouseY = -666;


  sdmain.handleKeyEvent = delegate (KeyEvent event) {
    if (sdmain.closed) return;
    scope(exit) glconPostDoConCommands!true();
    if (glconKeyEvent(event)) return;

    auto acc = clist.mainAccount;

    if (event == "D-Escape") {
      if (sdmain !is null && !sdmain.closed && sdmain.visible) {
        sdmain.hide();
        flushGui();
      }
      return;
    }

    scope(exit) { if (event.pressed) glconPostScreenRepaint(); }

    if (event == "D-C-X") { concmd("quit"); return; }
    if (event == "D-C-1") { acc.status = ContactStatus.Online; return; }
    if (event == "D-C-2") { acc.status = ContactStatus.Away; return; }
    if (event == "D-C-3") { acc.status = ContactStatus.Busy; return; }
    if (event == "D-C-0") { acc.status = ContactStatus.Offline; return; }

    if (event == "D-C-W") { doActivateContact(null); return; }

    if (event == "D-C-C" || event == "D-C-Insert") {
      string text = lay.getMarkedText().xstripright;
      if (text.length) {
        setClipboardText(sdmain, text);
        setPrimarySelection(sdmain, text);
        setSecondarySelection(sdmain, text);
      }
    }

    if (event == "D-M-Q") {
      string text = quoteText(lay.getMarkedText());
      if (text.length) {
        auto etext = currEdit.text;
        if (etext.length && etext[$-1] != '\n') currEdit.addText("\n");
        currEdit.addText(text);
      }
    }

    if (clist !is null) {
      if (clist.onKey(event)) return;
    }

    /*
    if (event == "D-C-S-Enter") {
      static PopupWindow.Kind kind = PopupWindow.Kind.Incoming;
      //new PopupWindow(10, 10, "this is my text, lol (еб\u00adёна КОЧЕРГА!)");
      showPopup(kind, "TEST popup", "this is my text, lol (еб\u00adёна КОЧЕРГА!)");
      if (kind == PopupWindow.Kind.max) kind = PopupWindow.Kind.min; else ++kind;
      return;
    }
    */

    if (event == "D-Enter") {
      auto text = currEdit.text.xstrip;
      currEdit.clear();
      if (text.length == 0) return;

      // `null`: eol
      string getWord () {
        while (text.length && text[0] <= ' ') text = text[1..$];
        if (text.length == 0) return null;
        if (text[0] == '"' || text[0] == '\'') {
          string res;
          char ech = text[0];
          text = text[1..$];
          while (text.length && text[0] != ech) {
            if (text[0] == '\\') {
              text = text[1..$];
              if (text.length == 0) break;
              char ch = text[0];
              switch (ch) {
                case '\r': res ~= '\r'; break;
                case '\n': res ~= '\n'; break;
                case '\t': res ~= '\t'; break;
                default: res ~= ch; break;
              }
            } else {
              res ~= text[0];
            }
            text = text[1..$];
          }
          if (text.length) { assert(text[0] == ech); text = text[1..$]; }
          return res;
        } else {
          auto ep = 0;
          while (ep < text.length && text[ep] > ' ') ++ep;
          auto res = text[0..ep];
          text = text[ep..$];
          return res;
        }
      }

      if (activeContact !is null) {
        auto otext = text;
        if (text[0] == '/' && !text.startsWith("/me ") && !text.startsWith("/me\t")) {
          text = text[1..$];
          auto cmd = getWord();
          if (cmd == "help") {
            addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false,
              "/accept -- accept friend request\n"~
              "/remove -- remove contact\n"~
              "/kfd -- remove friend, block any further requests\n"~
              "/status -- set account status\n"~
              "/pubkey -- get contact public key\n"~
              "/always -- always visible\n"~
              "/normal -- normal visibility"~
              "", systimeNow);
          } else if (cmd == "accept") {
            if (toxCoreAddFriend(activeContact.acc.toxpk, activeContact.info.pubkey)) {
              addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, "accepted!", systimeNow);
              activeContact.kind = ContactInfo.Kind.Friend;
              activeContact.save();
            } else {
              addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, "ERROR accepting!", systimeNow);
            }
          } else if (cmd == "kfd") {
            toxCoreRemoveFriend(activeContact.acc.toxpk, activeContact.info.pubkey);
            addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, "KILL! FUCK! DIE!", systimeNow);
            activeContact.kind = ContactInfo.Kind.KillFuckDie;
            activeContact.save();
          } else if (cmd == "remove") {
            acc.removeContact(activeContact);
          } else if (cmd == "friend") {
            if (!isValidAddr(activeContact.info.fraddr)) { conwriteln("invalid address"); return; }
            string msg = getWord();
            if (msg.length == 0) { conwriteln("address: '", tox_hex(activeContact.info.fraddr), "'; please, specify message!"); return; }
            if (msg.length > tox_max_friend_request_length()) { conwriteln("address: '", tox_hex(activeContact.info.fraddr), "'; message too long"); return; }
            if (!acc.sendFriendRequest(activeContact.info.fraddr, msg)) { conwriteln("address: '", tox_hex(activeContact.info.fraddr), "'; error sending friend request"); return; }
          } else if (cmd == "status") {
            string msg = getWord();
            if (msg.length == 0) { conwriteln("current: ", acc.info.statusmsg); return; }
            if (!toxCoreSetStatusMessage(activeContact.acc.toxpk, msg)) { conwriteln("ERROR: cannot set status message"); return; }
            activeContact.acc.info.statusmsg = msg;
            activeContact.acc.save();
            try { activeContact.acc.save(); } catch (Exception e) { conwriteln("ERROR saving account: ", e.msg); }
          } else if (cmd == "pubkey") {
            addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, tox_hex(activeContact.info.pubkey), systimeNow);
          } else if (cmd == "always") {
            activeContact.showOffline = TriOption.Yes;
            addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, "always visible", systimeNow);
          } else if (cmd == "normal") {
            activeContact.showOffline = TriOption.Default;
            addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, "normal visibility", systimeNow);
          } else {
            addTextToLog(activeContact.acc, activeContact, LogFile.Msg.Kind.Notification, false, "wut?!", systimeNow);
          }
        } else {
          activeContact.send(text);
        }
      } else {
        // sysedit
        if (acc !is null && text.length && text[0] == '/') {
          text = text[1..$];
          // get command
          auto cmd = getWord();
          switch (cmd) {
            case "friend": // add new friend
              string addr = getWord();
              ToxAddr fraddr = decodeAddrStr(addr);
              if (!isValidAddr(fraddr)) { conwriteln("invalid address: '", addr, "'"); return; }
              string msg = getWord();
              if (msg.length == 0) { conwriteln("address: '", tox_hex(fraddr), "'; please, specify message!"); return; }
              if (msg.length > tox_max_friend_request_length()) { conwriteln("address: '", addr, "'; message too long"); return; }
              //if (!toxCoreSendFriendRequest(acc.toxpk, fraddr, msg)) { conwriteln("address: '", addr, "'; error sending friend request");
              if (!acc.sendFriendRequest(fraddr, msg)) { conwriteln("address: '", addr, "'; error sending friend request"); return; }
              break;
            case "status":
              string msg = getWord();
              if (msg.length == 0) { conwriteln("current: ", acc.info.statusmsg); return; }
              if (!toxCoreSetStatusMessage(acc.toxpk, msg)) { conwriteln("ERROR: cannot set status message"); return; }
              acc.info.statusmsg = msg;
              activeContact.acc.save();
              try { acc.save(); } catch (Exception e) { conwriteln("ERROR saving account: ", e.msg); }
              break;
            case "myaddr":
              currEdit.text = acc.getAddress();
              break;
            case "help":
              addTextToLog(null, null, LogFile.Msg.Kind.Notification, false,
                "/friend addr msg -- send friend request\n"~
                "/status -- set account status\n"~
                "/myaddr -- put my address into the editor"~
                "", systimeNow);
              break;
            default:
              conwriteln("unknown command: '", cmd, "'");
              break;
          }
        } else {
          if (text.length) conwriteln("NOT A COMMAND: ", text);
        }
      }
      return;
    }

    if (currEdit.onKey(event)) return;
  };

  int msLastPressX = -666, msLastPressY = -666;
  int msDoLogButton = 0; // =0: none; 1: left; 2: middle; 3: right; negative: release

  sdmain.handleMouseEvent = delegate (MouseEvent event) {
    if (sdmain.closed) return;
    scope(exit) glconPostDoConCommands!true();
    if (isConsoleVisible) return;
    mouseX = event.x;
    mouseY = event.y;

    // check for click in log
    //FIXME: process it here, not in renderer
    if (event == "LMB-Down") { msLastPressX = mouseX; msLastPressY = mouseY; msDoLogButton = 1; glconPostScreenRepaint(); }
    if (event == "MMB-Down") { msLastPressX = mouseX; msLastPressY = mouseY; msDoLogButton = 2; glconPostScreenRepaint(); }
    if (event == "RMB-Down") { msLastPressX = mouseX; msLastPressY = mouseY; msDoLogButton = 3; glconPostScreenRepaint(); }
    if (event == "LMB-Up") { msLastPressX = mouseX; msLastPressY = mouseY; msDoLogButton = -1; glconPostScreenRepaint(); }
    if (event == "MMB-Up") { msLastPressX = mouseX; msLastPressY = mouseY; msDoLogButton = -2; glconPostScreenRepaint(); }
    if (event == "RMB-Up") { msLastPressX = mouseX; msLastPressY = mouseY; msDoLogButton = -3; glconPostScreenRepaint(); }

    if (clist !is null) {
      if (clist.onMouse(event)) {
        mouseStartMarking = false;
        if (lay.hasMark()) {
          lay.resetMarks();
          glconPostScreenRepaint();
        }
        return;
      }
    }

    // log scroll
    if (layWinHeight > 0) {
      enum ScrollHeight = 32;
      if (event == "WheelUp") {
        layOffset += ScrollHeight;
        if (layOffset > lay.textHeight-layWinHeight) layOffset = lay.textHeight-layWinHeight;
      } else if (event == "WheelDown") {
        layOffset -= ScrollHeight;
      }
      if (layOffset < 0) layOffset = 0;
    }

    // don't spam with repaint events
    if (event.type != MouseEventType.motion) {
      glconPostScreenRepaint();
    } else {
      if (mouseStartMarking) {
        msDoLogButton = 666;
        msLastPressX = mouseX;
        msLastPressY = mouseY;
        glconPostScreenRepaint();
      }
    }
  };

  sdmain.handleCharEvent = delegate (dchar ch) {
    if (sdmain.closed) return;
    scope(exit) glconPostDoConCommands!true();
    if (glconCharEvent(ch)) return;

    scope(exit) glconPostScreenRepaint();

    if (currEdit.onChar(ch)) return;
  };

  // draw main screen
  sdmain.redrawOpenGlScene = delegate () {
    glconPostDoConCommands!true();
    if (sdmain.closed) return;
    sdmain.setAsCurrentOpenGlContext(); // make this window active
    scope(exit) sdmain.releaseCurrentOpenGlContext();
    glViewport(0, 0, sdmain.width, sdmain.height);

    // draw main screen
    scope(exit) glconDraw();

    if (nvg is null) return;

    //oglSetup2D(glconCtlWindow.width, glconCtlWindow.height);
    glViewport(0, 0, sdmain.width, sdmain.height);
    glMatrixMode(GL_MODELVIEW);
    //conwriteln(glconCtlWindow.width, "x", glconCtlWindow.height);

    glClearColor(0, 0, 0, 0);
    glClear(glNVGClearFlags/*|GL_COLOR_BUFFER_BIT*/);

    {
      nvg.beginFrame(sdmain.width, sdmain.height);
      scope(exit) nvg.endFrame();

      if (clist !is null && optCListWidth > 0) {
        // draw contact list
        int cx = 1;
        int cy = 1;
        int wdt = optCListWidth;
        int hgt = nvg.height-cy*2;

        nvg.shapeAntiAlias = true;
        nvg.nonZeroFill;
        nvg.strokeWidth = 1;

        {
          nvg.save();
          scope(exit) nvg.restore();

          nvg.newPath();
          nvg.roundedRect(cx+0.5f, cy+0.5f, wdt, hgt, 6);
          {
            int w, h;
            nvg.imageSize(nvgSkullsImg, w, h);
            nvg.fillPaint(nvg.imagePattern(0, 0, w, h, 0, nvgSkullsImg));
          }
          nvg.strokeColor(NVGColor("#f70"));
          nvg.fill();
          nvg.stroke();

          // draw contact list
          {
            nvg.save();
            scope(exit) nvg.restore();
            nvg.intersectScissor(cx+3.5f, cy+3.5f, wdt-3*2, hgt-3*2);
            clist.drawAt(nvg, cx+3, cy+3, wdt-3*2, hgt-3*2);
          }
          // draw scrollbar
          if (clist.sbSize > 0) {
            nvg.bndScrollBar(cx+3+(wdt-3*2)+3-BND_SCROLLBAR_WIDTH+0.5f, cy+1+0.5f, BND_SCROLLBAR_WIDTH, hgt-2, BND_DEFAULT, clist.sbPosition, clist.sbSize);
          }
        }

        {
          nvg.save();
          scope(exit) nvg.restore();

          // draw chat log
          cx += wdt+2;
          wdt = nvg.width-cx-1;
          auto baphHgt = hgt;

          // calculate editor dimensions and draw editor
          {
            nvg.save();
            scope(exit) nvg.restore();

            currEdit.setWidth(wdt-3*2);

            auto edheight = currEdit.calcHeight();

            int edy = cy+hgt-edheight-3*2;

            nvg.newPath();
            nvg.roundedRect(cx+0.5f, edy+0.5f, wdt, edheight+3*2, 6);
            nvg.fillColor(NVGColor.black);
            nvg.strokeColor(NVGColor("#f70"));
            nvg.fill();
            nvg.stroke();

            nvg.intersectScissor(cx+2.5f, edy+2.5f, wdt-3*2+2, edheight+2);
            currEdit.draw(nvg, cx+3, edy+3);

            hgt -= edheight+3*2;
          }

          nvg.newPath();
          nvg.roundedRect(cx+0.5f, cy+0.5f, wdt, hgt, 6);
          nvg.fillColor(NVGColor.black);
          nvg.strokeColor(NVGColor("#f70"));
          nvg.fill();
          nvg.stroke();

          nvg.intersectScissor(cx+3.5f, cy+3.5f, wdt-3*2, hgt-3*2);
          version(all) {
            immutable float scalex = (wdt-3*2-10*2)/BaphometDims;
            immutable float scaley = (baphHgt-3*2-10*2)/BaphometDims;
            immutable float scale = (scalex < scaley ? scalex : scaley)/1.5f;
            immutable float sz = BaphometDims*scale;
            nvg.strokeColor(NVGColor("#400"));
            nvg.fillColor(NVGColor("#400"));
            nvg.renderBaphomet(cx+10.5f+(wdt-3*2-10*2)/2-sz/2, cy+10.5f+(baphHgt-3*2-10*2)/2-sz/2, scale, scale);
          }

          immutable float sbx = cx+wdt-BND_SCROLLBAR_WIDTH-1.5f;
          immutable float sby = cy+3.5f;
          wdt -= BND_SCROLLBAR_WIDTH+1;
          wdt -= 3*2;
          hgt -= 3*2;

          lay.relayout(wdt); // this is harmess if width wasn't changed
          int ty = lay.textHeight-hgt+1-layOffset;
          if (ty < 0) ty = 0;
          layWinHeight = cast(int)hgt;

          if (lay.textHeight > hgt) {
            float h = lay.textHeight-hgt;
            nvg.bndScrollBar(sbx, sby, BND_SCROLLBAR_WIDTH, hgt, BND_DEFAULT, ty/h, hgt/h);
          } else {
            nvg.bndScrollBar(sbx, sby, BND_SCROLLBAR_WIDTH, hgt, BND_DEFAULT, 1, 1);
          }

          nvg.intersectScissor(cx+2.5f, cy+2.5f, wdt+2, hgt+2);
          nvg.drawLayouter(lay, ty, cx+3, cy+3, hgt);

          //conwriteln("msDoLogButton=", msDoLogButton, "; mouseStartMarking=", mouseStartMarking);
          if (msDoLogButton) {
            immutable int btn = msDoLogButton;
            msDoLogButton = 0;
            immutable int mx = msLastPressX-(cx+3);
            immutable int my = msLastPressY-(cy+3);
            if (mx >= 0 && my >= 0 && mx < wdt && my < hgt) {
              //conwriteln("MOUSE: ", mx, " : ", my, "; ty=", ty, "; thgt=", lay.textHeight);
              auto widx = lay.wordAtXY(mx, ty+my);
              //conwriteln("  widx=", widx);
              if (widx >= 0) {
                auto w = lay.wordByIndex(widx);
                //conwriteln("  widx=", widx, "; <", lay.wordText(*w), ">; udata=", w.udata);
                // lmb
                if (btn == 1) {
                  string url = findUrlByWordIndex(widx);
                  if (!mouseStartMarking && url.length) {
                    //conwriteln("URL CLICK: <", url, ">");
                    openUrl(url);
                    mouseStartMarking = false;
                  } else {
                    // mark text
                    if (!mouseStartMarking) {
                      //conwriteln("MARK START!");
                      mouseStartMarking = true;
                      lay.setMark(lay.MarkType.Both, mx, ty+my);
                      glconPostScreenRepaint();
                    }
                  }
                }
                // rmb
                if (btn == 3 && w.objectIdx >= 0) {
                  if (auto maw = cast(MessageOutMark)lay.objectAtIndex(w.objectIdx)) {
                    assert(activeContact !is null);
                    activeContact.removeFromResendQueue(maw.msgid, maw.digest, maw.time);
                    maw.msgid = -1;
                    // yeah, repaint
                    glconPostScreenRepaint();
                  }
                }
              }
            }
            // continue marking
            if (mouseStartMarking && btn == 666) {
              //conwriteln("MARK CONTINUE!");
              //conwriteln("  before: ", lay.getMarkWordFirst, ":", lay.getMarkWordCount);
              lay.setMark(lay.MarkType.End, mx, ty+my);
              //conwriteln("   after: ", lay.getMarkWordFirst, ":", lay.getMarkWordCount);
              glconPostScreenRepaint();
            }
            // releases
            if (btn == -1) {
              if (mouseStartMarking) {
                mouseStartMarking = false;
                //conwriteln("MARK END!");
              }
            }
          }
        }
      }
    }
  };

  flushGui();
  MonoTime lastCollect = MonoTime.currTime;
  sdmain.eventLoop(16000,
    // pulser: process resend queues here
    delegate () {
      if (sdmain.closed || clist is null) return;
      clist.forEachAccount(delegate (Account acc) { acc.processResendQueue(); });
      {
        immutable ctt = MonoTime.currTime;
        if ((ctt-lastCollect).total!"minutes" >= 1) {
          import core.memory : GC;
          lastCollect = ctt;
          GC.collect();
          GC.minimize();
        }
      }
    },
  );
  clist.forEachAccount(delegate (acc) { acc.forceOnline = false; });
  clist.forEachAccount(delegate (Account acc) { acc.saveResendQueue(); acc.saveResendQueue(); });
  toxCoreShutdownAll();
  popupKillAll();
  flushGui();
  conProcessQueue(int.max/4);
}
