/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module fonts is aliced;

import iv.cmdcon;
import iv.nanovega;
import iv.nanovega.textlayouter;


// ////////////////////////////////////////////////////////////////////////// //
alias LayTextClass = LayTextD;


// ////////////////////////////////////////////////////////////////////////// //
enum TTFontStyle : ubyte { Normal = 0, Italic = 1, Bold = 2 }

__gshared FONSContext fstash;
__gshared LayFontStash laf;

__gshared string[4] textFontNames = [
  "PT Sans", //"Arial:noaa", //"~/ttf/ms/arial.ttf:noaa", // normal
  "PT Sans:italic", //"~/ttf/ms/ariali.ttf:noaa", // italic
  "PT Sans:bold", //"~/ttf/ms/arialbd.ttf:noaa", // bold
  "PT Sans:italic:bold", //"~/ttf/ms/arialbi.ttf:noaa", // italic+bold
];
__gshared string[4] monoFontNames = [
  //"/usr/share/fonts/ms/andalemo.ttf:noaa", // normal
  "PT Mono", //"Courier New:noaa", //"~/ttf/ms/cour.ttf:noaa", // normal
  "PT Mono:italic", //"~/ttf/ms/couri.ttf:noaa", // italic
  "PT Mono:bold", //"~/ttf/ms/courbd.ttf:noaa", // bold
  "PT Mono:italic:bold", //"~/ttf/ms/courbi.ttf:noaa", // italic+bold
];
__gshared string[4] uiFontNames = [
  "PT Sans", //"Arial:noaa", //"~/ttf/ms/verdana.ttf:noaa", // normal
  "PT Sans:italic", //"~/ttf/ms/ariali.ttf:noaa", // italic
  "PT Sans:bold", //"~/ttf/ms/arialbd.ttf:noaa", // bold
  "PT Sans:italic:bold", //"~/ttf/ms/arialbi.ttf:noaa", // italic+bold
];


// ////////////////////////////////////////////////////////////////////////// //
void fixFontForStyleEx (string normalFontPfx, LayFontStash laf, ref LayFontStyle st) nothrow @trusted @nogc {
  //{ import core.stdc.stdio; printf("monospace=%d; italic=%d; bold=%d\n", (st.monospace ? 1 : 0), (st.italic ? 1 : 0), (st.bold ? 1 : 0)); }
  char[12] tbuf;
  int tbufpos = 0;
  void put (const(char)[] s...) nothrow @trusted @nogc {
    if (s.length == 0) return;
    if (tbuf.length-tbufpos < s.length) assert(0, "wtf?!");
    tbuf.ptr[tbufpos..tbufpos+s.length] = s[];
    tbufpos += cast(int)s.length;
  }
  put(st.monospace ? "mono" : normalFontPfx);
  if (st.italic) put("i");
  if (st.bold) put("b");
  st.fontface = laf.fontFaceId(tbuf[0..tbufpos]);
}

void fixFontForStyleText (LayFontStash laf, ref LayFontStyle st) nothrow @trusted @nogc { fixFontForStyleEx("text", laf, st); }
void fixFontForStyleUI (LayFontStash laf, ref LayFontStyle st) nothrow @trusted @nogc { fixFontForStyleEx("ui", laf, st); }

private void loadFmtFonts () {
  import std.functional : toDelegate;

  laf = new LayFontStash();
  laf.fs.addFontsFrom(fstash);
  laf.fixFontDG = toDelegate(&fixFontForStyleText);
}


// ////////////////////////////////////////////////////////////////////////// //
// create dummy fontstash and load fonts
void loadAllFonts () {
  assert(fstash is null);
  fstash = FONSContext.create(FONSParams.init);
  if (fstash is null) assert(0, "error creating font stash");

  void loadFont (string name, string path) {
    auto fid = fstash.addFont(name, path, false); // no AA
    if (fid < 0) assert(0, "can't load font '"~name~"' from '"~path~"'");
    //conwriteln("created font [", name, "] (", path, "): ", fid);
  }

  void loadFontSet (string namebase, string[] pathes) {
    static immutable string[4] pfx = ["", "i", "b", "ib"];
    assert(pathes.length >= 4);
    foreach (immutable idx; 0..4) loadFont(namebase~pfx[idx], pathes[idx]);
  }

  loadFontSet("text", textFontNames[]);
  loadFontSet("mono", monoFontNames[]);
  loadFontSet("ui", uiFontNames[]);

  //loadFont("arial", "arial");
  loadFont("arial", "/home/ketmar/sofonts/arialn.ttf");
  loadFont("tahoma", "tahoma");
  loadFont("verdana", "verdana");

  //loadFont("test", "/mnt/bigfoot/dprj/bioacid/_ttf/ms/ttc/Sitka.ttc");

  loadFmtFonts();
}


// ////////////////////////////////////////////////////////////////////////// //
//vg.fillColor(NVGColor.orange);
void drawLayouter(LT) (NVGContext vg, LT lay, int layTopY, int x0, int y0, int hgt)
if (is(LT : LayTextImpl!CT, CT))
{
  if (vg is null || lay is null || hgt < 1 || lay.lineCount == 0 || lay.width < 1) return;
  int drawY = y0;
  //FIXME: not GHeight!
  int lidx = lay.findLineAtY(layTopY);
  if (lidx >= 0 && lidx < lay.lineCount) {
    vg.save();
    scope(exit) vg.restore();
    vg.strokeWidth = 1;
    vg.beginPath();
    vg.intersectScissor(x0, y0, lay.width, hgt);
    drawY -= layTopY-lay.line(lidx).y;
    vg.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
    LayFontStyle lastStyle;
    int startx = x0;
    bool setColor = true;
    while (lidx < lay.lineCount && hgt > 0) {
      auto ln = lay.line(lidx);
      foreach (ref LayWord w; lay.lineWords(lidx)) {
        if (lastStyle != w.style || setColor) {
          if (w.style.fontface != lastStyle.fontface) vg.fontFace = lay.fontFace(w.style.fontface);
          vg.fontSize(w.style.fontsize);
          auto c = NVGColor(w.style.color);
          vg.fillColor = c;
          vg.strokeColor = c;
          lastStyle = w.style;
          setColor = false;
          //conprintfln("new color: 0x%08x; fontid=%d; fontsize=%d", w.style.color, w.style.fontface, w.style.fontsize);
        }
        // background color
        {
          auto c = NVGColor(w.style.bgcolor);
          if (!c.isTransparent) {
            {
              vg.save();
              scope(exit) vg.restore();
              vg.beginPath();
              vg.fillColor(c);
              vg.rect(cast(int)(startx+w.x)+0.5f, cast(int)drawY+0.5, cast(int)w.fullwidth, cast(int)ln.h);
              //vg.rect(startx+w.x+0.5f, drawY+0.5, w.fullwidth, ln.h);
              vg.fill();
            }
            vg.beginPath();
          }
        }
        // marked words
        if (lay.isMarkedWord(w.wordNum)) {
          vg.save();
          scope(exit) vg.restore();
          vg.beginPath();
          vg.fillColor(nvgRGB(0, 0, 255));
          vg.rect(cast(int)(startx+w.x)+0.5f, cast(int)drawY+0.5,
                  (lay.isMarkedWord(w.wordNum+1) ? cast(int)w.fullwidth : cast(int)w.width),
                  cast(int)ln.h);
          //vg.rect(startx+w.x+0.5f, drawY+0.5, w.fullwidth, ln.h);
          vg.fill();
          setColor = true;
        }
        // element
        auto oid = w.objectIdx;
        if (oid >= 0) {
          //vg.fill();
          {
            vg.save();
            scope(exit) vg.restore();
            vg.beginPath();
            lay.objectAtIndex(oid).draw(vg, startx+w.x, drawY+ln.h+ln.desc);
          }
          vg.beginPath();
        } else if (!w.expander) {
          vg.text(startx+w.x, drawY+ln.h+ln.desc, lay.wordText(w));
        }
        //TODO: draw lines over whitespace
        bool doStroke = false;
        if (lastStyle.underline || lastStyle.href) { doStroke = true; vg.rect(startx+w.x+0.5f, drawY+ln.h+ln.desc+1+0.5f, w.w, 1); }
        if (lastStyle.strike) { doStroke = true; vg.rect(startx+w.x+0.5f, drawY+ln.h+ln.desc-w.asc/3+0.5f, w.w, 2); }
        if (lastStyle.overline) { doStroke = true; vg.rect(startx+w.x+0.5f, drawY+ln.h+ln.desc-w.asc-1+0.5f, w.w, 1); }
        if (doStroke) {
          vg.stroke();
          vg.beginPath();
        }
      }
      drawY += ln.h;
      hgt -= ln.h;
      ++lidx;
    }
  }
}
