/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module notifyicon is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.gxx;
import iv.meta;
import iv.nanovega;
import iv.nanovega.textlayouter;
import iv.strex;
import iv.tox;
import iv.sdpyutil;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

import accdb;
import fonts;
import icondata;


// ////////////////////////////////////////////////////////////////////////// //
class TooltipWindow : SimpleWindow {
public: // utilities
  static string charArrayToString(T) (T[] str) if (isAnyCharType!(T, true)) {
    if (str.length == 0) return null;
    static if (isWideCharType!(T, true)) {
      int len = 0;
      char[4] ut;
      foreach (auto ch; str) len += utf8Encode(ut[], ch);
      string res;
      res.reserve(len);
      foreach (auto ch; str) {
        auto l = utf8Encode(ut[], ch);
        assert(l > 0);
        res ~= ut[0..l];
      }
      return res;
    } else {
      static if (is(T == immutable(char))) return str; else return str.idup;
    }
  }

private:
  static GxRect getWorkArea () {
    GxRect rc;
    getWorkAreaRect(rc.x0, rc.y0, rc.width, rc.height);
    return rc;
  }

  static GxSize hintWindowSize(T) (T[] hinttext) nothrow if (isAnyCharType!(T, true)) {
    laf.font = "ui";
    laf.size = 18;
    int textWidth = laf.textWidth(hinttext)+6;
    if (textWidth < 8) textWidth = 8;
    return GxSize(textWidth, laf.textHeight+4);
  }

private:
  NVGContext hintvg;
  string hinttext;
  int initX, initY;
  bool active;

private:
  void cbOnClosing () {
    active = false;
    if (hintvg !is null) {
      this.setAsCurrentOpenGlContext();
      scope(exit) { flushGui(); this.releaseCurrentOpenGlContext(); }
      hintvg.kill();
      assert(hintvg is null);
      //conwriteln("ONCLOSING!");
    }
  }

  void cbWindowResized (int wdt, int hgt) {
    if (this.closed) return;
    if (!this.visible) return;
    this.redrawOpenGlSceneNow();
  }

  void cbInit () {
    this.setAsCurrentOpenGlContext(); // make this window active
    scope(exit) this.releaseCurrentOpenGlContext();
    this.vsync = false;
    hintvg = nvgCreateContext(NVGContextFlag.FontNoAA);
    if (hintvg is null) assert(0, "cannot initialize NanoVG");
    //hintvg.createFont("ui", uiFontNames[0]);
    //if (nvg !is null) hintvg.addFontsFrom(nvg); else hintvg.createFont("ui", uiFontNames[0]);
    hintvg.fonsContext.addFontsFrom(fstash);
    //conwriteln("FIRST TIME");
    //this.redrawOpenGlSceneNow();
  }

  void cbOnPaint () {
    if (this.closed) return;
    if (hintvg !is null) {
      active = true;
      this.setAsCurrentOpenGlContext(); // make this window active
      scope(exit) this.releaseCurrentOpenGlContext();

      glViewport(0, 0, this.width, this.height);
      glClearColor(0, 0, 0, 0);
      glClear(glNVGClearFlags/*|GL_COLOR_BUFFER_BIT*/);

      hintvg.beginFrame(this.width, this.height);
      scope(exit) hintvg.endFrame();
      //hintvg.shapeAntiAlias = false;

      //auto wsz = hintWindowSize(hinttext);
      //conwritefln!"sz=(%d,%d); wsz=(%d,%d); nsz=(%d,%d)"(this.width, this.height, wsz.width, wsz.height, hintvg.width, hintvg.height);

      hintvg.newPath();
      hintvg.rect(0.5, 0.5, hintvg.width-1, hintvg.height-1);
      hintvg.strokeWidth = 1;
      hintvg.strokeColor(NVGColor.black);
      hintvg.fillColor(NVGColor.yellow);
      hintvg.fill();
      hintvg.stroke();

      hintvg.fontFace = "ui";
      hintvg.fontSize = 18;
      hintvg.textAlign = NVGTextAlign.H.Left;
      hintvg.textAlign = NVGTextAlign.V.Baseline;
      hintvg.fillColor(NVGColor.black);
      hintvg.text(3, 2+hintvg.textFontAscender, hinttext);
    }
    //flushGui();
  }

  void calcSizePos (out GxRect rc) {
    auto wsz = hintWindowSize(hinttext);
    auto wrc = getWorkArea();
    int nx = initX;
    int ny = initY;
    if (nx+wsz.width > wrc.x1) nx = wrc.x1-wsz.width+1;
    if (nx < wrc.x0) nx = wrc.x0;
    if (ny+wsz.height > wrc.y1) ny = wrc.y1-wsz.height+1;
    if (ny < wrc.y0) ny = wrc.y0;
    // set return value
    rc.x0 = nx;
    rc.y0 = ny;
    rc.width = wsz.width;
    rc.height = wsz.height;
  }

public:
  override void close () { active = false; super.close(); }

  this(T) (int ax, int ay, T[] atext) if (isAnyCharType!(T, true)) {
    assert(laf !is null);

    hinttext = charArrayToString(atext);
    initX = ax;
    initY = ay;

    GxRect wpos;
    calcSizePos(out wpos);

    this.onClosing = &cbOnClosing;
    this.windowResized = &cbWindowResized;
    this.visibleForTheFirstTime = &cbInit;
    this.redrawOpenGlScene = &cbOnPaint;
    //this.handleMouseEvent = &cbOnMouse;

    {
      auto oldWClass = sdpyWindowClass;
      scope(exit) sdpyWindowClass = oldWClass;
      sdpyWindowClass = "BIOACID_HINT_WINDOW";
      super(wpos.width, wpos.height, "BioAcidHint", OpenGlOptions.yes, Resizability.fixedSize, WindowTypes.undecorated, WindowFlags.skipTaskbar|WindowFlags.alwaysOnTop|WindowFlags.cannotBeActivated|WindowFlags.dontAutoShow);
    }
    XSetWindowBackground(this.impl.display, this.impl.window, gxRGB!(255, 255, 0));

    // sorry for this hack
    this.setNetWMWindowType(GetAtom!("_NET_WM_WINDOW_TYPE_DOCK", true)(this.display));
    //this.setNetWMWindowType(GetAtom!("_NET_WM_WINDOW_TYPE_TOOLTIP", true)(this.display));
    {
      Atom[4] atoms;
      atoms[0] = GetAtom!("_NET_WM_STATE_STICKY", true)(this.impl.display);
      atoms[1] = GetAtom!("_NET_WM_STATE_SKIP_TASKBAR", true)(this.impl.display);
      atoms[2] = GetAtom!("_NET_WM_STATE_SKIP_PAGER", true)(this.impl.display);
      atoms[3] = GetAtom!("_NET_WM_STATE_ABOVE", true)(this.impl.display);
      XChangeProperty(
        this.impl.display,
        this.impl.window,
        GetAtom!("_NET_WM_STATE", true)(this.impl.display),
        XA_ATOM,
        32 /* bits */,
        0 /*PropModeReplace*/,
        atoms.ptr,
        cast(int)atoms.length);
    }
    //if (this.hidden) this.show();
    XMoveWindow(this.impl.display, this.impl.window, 6000, 6000);
    this.show();
    //this.moveResize(wpos.x0, wpos.y0, wpos.width, wpos.height);
    XMoveResizeWindow(this.impl.display, this.impl.window, 6000, 6000, wpos.width, wpos.height);
    flushGui();
    //conwritefln!"CREATED! (%d,%d) [%s]"(wsz.width, wsz.height, hinttext);
    XMoveResizeWindow(this.impl.display, this.impl.window, wpos.x0, wpos.y0, wpos.width, wpos.height);
    flushGui();
  }

  string hint () const nothrow @safe @nogc { pragma(inline, true); return hinttext; }

  void hint(T) (T[] atext) if (isAnyCharType!(T, true)) {
    string ht = charArrayToString(atext);
    if (ht != hinttext) {
      //TODO: check if resize works (it doesn't)
      hinttext = ht;
      GxRect wpos;
      calcSizePos(out wpos);
      //conwritefln!"NEW: sz=(%d,%d); wsz=(%d,%d)"(this.width, this.height, wpos.width, wpos.height);
      if (!this.closed && active) {
        //this.setAsCurrentOpenGlContext(); // make this window active
        //scope(exit) this.releaseCurrentOpenGlContext();
        //this.moveResize(wpos.x0, wpos.y0, wpos.width, wpos.height);
        XMoveResizeWindow(this.impl.display, this.impl.window, wpos.x0, wpos.y0, wpos.width, wpos.height);
      }
      //flushGui();
    }
  }

  @property GxPoint initPos () const nothrow @safe @nogc { pragma(inline, true); return GxPoint(initX, initY); }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared TooltipWindow sdhint;
__gshared Timer hintHideTimer;
__gshared string lastHintText;


// ////////////////////////////////////////////////////////////////////////// //
void setHint(T:const(char)[]) (T str) {
  static if (is(T == typeof(null))) {
    setHint("");
  } else {
    lastHintText = TooltipWindow.charArrayToString(str);
    if (sdhint is null || sdhint.closed || sdhint.hidden) return;
    //sdhint.hint = lastHintText;
    auto pos = sdhint.initPos();
    sdhint.close();
    sdhint = new TooltipWindow(pos.x, pos.y, lastHintText);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared NotificationAreaIcon trayicon;
__gshared Image[6] trayimage; // offline, online, away; same as Status
__gshared ContactStatus traystatus = ContactStatus.Offline;
__gshared bool trayIsUnread = false;
__gshared bool trayBlinkStatus = true; // true: show kitty
__gshared Timer blinkTimer;


void setTrayUnread () {
  if (!trayIsUnread) {
    trayIsUnread = true;
    if (blinkTimer is null) {
      blinkTimer = new Timer(800, delegate () {
        trayBlinkStatus = !trayBlinkStatus;
        if (trayIsUnread) {
          trayicon.icon = (trayBlinkStatus ? trayimage[$-1] : trayimage[traystatus]);
          flushGui(); // or it may not redraw itself
        }
      });
    }
    if (trayBlinkStatus) {
      trayicon.icon = trayimage[$-1];
      flushGui(); // or it may not redraw itself
    }
  }
}


void setTrayStatus (ContactStatus status) {
  if (status != traystatus || trayIsUnread) {
    trayIsUnread = false;
    traystatus = status;
    trayicon.icon = trayimage[traystatus];
    flushGui(); // or it may not redraw itself
  }
}


void prepareTrayIcon () {
  static Image buildTrayIcon (const(uint)[] img) {
    auto icon = new TrueColorImage(16, 16);
    scope(exit) { delete icon.imageData.bytes; delete icon; }
    foreach (immutable y; 0..16) {
      foreach (immutable x; 0..16) {
        Color c;
        c.asUint = img[y*16+x];
        if (c.a < 0x20) c = Color.black;
        else if (c.a < 0xff) {
          c = c.toBW;
          c = Color(c.r*c.a/255, c.g*c.a/255, c.b*c.a/255);
        }
        icon.setPixel(x, y, c);
      }
    }
    return Image.fromMemoryImage(icon);
  }

  trayimage[ContactStatus.Offline] = buildTrayIcon(baph16Gray[]);
  trayimage[ContactStatus.Online] = buildTrayIcon(baph16Online[]);
  trayimage[ContactStatus.Away] = buildTrayIcon(baph16Away[]);
  trayimage[ContactStatus.Busy] = buildTrayIcon(baph16Busy[]);
  trayimage[ContactStatus.Connecting] = buildTrayIcon(baph16Orange[]);
  trayimage[$-1] = buildTrayIcon(kittyMessage[]);
  //traystatus = Contact.Status.Online;
  trayicon = new NotificationAreaIcon("BioAcid", trayimage[traystatus], (int x, int y, MouseButton button, ModifierState mods) {
    //conwritefln!"x=%d; y=%d; button=%u; mods=0x%04x"(x, y, button, mods);
    if (button == MouseButton.middle) {
      concmd("quit");
      glconPostDoConCommands!true();
      return;
    }
    if (button == MouseButton.left) {
      concmd("win_toggle");
      glconPostDoConCommands!true();
      return;
    }
  });
  trayicon.onEnter = delegate (int x, int y, ModifierState mods) {
    if (sdhint is null || sdhint.hidden) {
      if (sdhint && !sdhint.closed) sdhint.close();
      sdhint = null;
      sdhint = new TooltipWindow(x+18, y+2, lastHintText);
      if (hintHideTimer !is null) hintHideTimer.destroy();
      if (sdhint !is null) {
        hintHideTimer = new Timer(2000, delegate () {
          if (hintHideTimer !is null) {
            hintHideTimer.destroy();
            hintHideTimer = null;
            if (sdhint !is null && !sdhint.closed) {
              sdhint.close();
              sdhint = null;
              //sdhint.hide();
              flushGui();
            }
            sdhint = null; // just in case
          }
        });
      }
    }
  };
  trayicon.onLeave = delegate () {
  };
}
