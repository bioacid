/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module popups is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.gxx;
import iv.meta;
import iv.nanovega;
import iv.nanovega.textlayouter;
import iv.strex;
import iv.tox;
import iv.sdpyutil;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

import fonts;


// ////////////////////////////////////////////////////////////////////////// //
public class PopupCheckerEvent {} ///
__gshared PopupCheckerEvent evPopupChecker;
shared static this () { evPopupChecker = new PopupCheckerEvent(); }


void postPopupCheckerEvent () {
  if (glconCtlWindow !is null && !glconCtlWindow.eventQueued!PopupCheckerEvent) {
    glconCtlWindow.postTimeout(evPopupChecker, 500);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class PopupWindow : SimpleWindow {
public:
  enum Width = 300;
  enum MinHeight = 64;
  enum MaxHeight = 200;

  enum Kind {
    Info,
    Incoming,
    Status,
    Error,
  }

public:
  static GxRect getWorkArea () nothrow @trusted {
    GxRect rc;
    try {
      getWorkAreaRect(rc.x0, rc.y0, rc.width, rc.height);
    } catch (Exception e) {
      rc = GxRect(0, 0, 800, 600);
    }
    return rc;
  }

private:
  NVGContext vg;
  LayTextClass lay;
  int x0 = -6000, y0 = -6000;
  SysTime dieTime;
  char[128] titlestr;
  int titlelen;
  Kind kind;
  bool errTooMany;

private:
  void cbOnClosing () {
    if (lay !is null) delete lay; // this will free malloced memory
    if (vg !is null) {
      this.setAsCurrentOpenGlContext();
      scope (exit) { flushGui(); this.releaseCurrentOpenGlContext(); }
      vg.kill();
    }
    popupRemoved(this);
  }

  void cbWindowResized (int wdt, int hgt) {
    if (this.closed) return;
    if (!this.visible) return;
    this.redrawOpenGlSceneNow();
  }

  void cbInit () {
    this.setAsCurrentOpenGlContext(); // make this window active
    scope(exit) this.releaseCurrentOpenGlContext();
    this.vsync = false;
    vg = nvgCreateContext();
    if (vg is null) assert(0, "cannot initialize NanoVG");
    //vg.createFont("ui", uiFontNames[0]);
    vg.fonsContext.addFontsFrom(fstash);
    //this.redrawOpenGlSceneNow();
  }

  // frame color
  NVGColor colorFrame () {
    final switch (kind) {
      case Kind.Info: return NVGColor("#aaa");
      case Kind.Incoming: return NVGColor("#ccc");
      case Kind.Status: return NVGColor("#0cf");
      case Kind.Error: return NVGColor("#f33");
    }
    assert(0);
  }

  // titlebar background color
  NVGColor colorTitleBG () {
    final switch (kind) {
      case Kind.Info: return NVGColor("#006");
      case Kind.Incoming: return NVGColor("#666");
      case Kind.Status: return NVGColor("#00c");
      case Kind.Error: return NVGColor("#600");
    }
    assert(0);
  }

  // titlebar text color
  NVGColor colorTitleFG () {
    final switch (kind) {
      case Kind.Info: return NVGColor("#fff");
      case Kind.Incoming: return NVGColor("#fff");
      case Kind.Status: return NVGColor("#aaa");
      case Kind.Error: return NVGColor("#ff0");
    }
    assert(0);
  }

  // background color
  NVGColor colorBG () {
    final switch (kind) {
      case Kind.Info: return NVGColor("#00c");
      case Kind.Incoming: return NVGColor("#333");
      case Kind.Status: return NVGColor("#008");
      case Kind.Error: return NVGColor("#c00");
    }
    assert(0);
  }

  // text color
  NVGColor colorFG () {
    final switch (kind) {
      case Kind.Info: return NVGColor("#ccc");
      case Kind.Incoming: return NVGColor("#bbb");
      case Kind.Status: return NVGColor("#888");
      case Kind.Error: return NVGColor("#fff");
    }
    assert(0);
  }

  void cbOnPaint () {
    if (this.closed) return;
    if (vg !is null) {
      this.setAsCurrentOpenGlContext(); // make this window active
      scope(exit) this.releaseCurrentOpenGlContext();

      glViewport(0, 0, this.width, this.height);
      glClearColor(0, 0, 0, 0);
      glClear(glNVGClearFlags/*|GL_COLOR_BUFFER_BIT*/);

      vg.beginFrame(this.width, this.height);
      scope(exit) vg.endFrame();
      vg.shapeAntiAlias = false;

      // title height
      int th = 0;
      if (titlelen > 0) {
        vg.fontFace = "uib";
        vg.fontSize = 20;
        th = cast(int)vg.textFontHeight;
      }

      // draw background and title
      {
        vg.save();
        vg.newPath();
        vg.roundedRect(0.5, 0.5, vg.width-1, vg.height-1, 6);
        // draw titlebar
        if (th > 0) {
          vg.save();
          vg.intersectScissor(0, 0, vg.width, th+2);
          vg.fillColor(colorTitleBG);
          vg.fill();
          vg.restore();
          vg.intersectScissor(0, th+1.5, vg.width, vg.height); // .5: slightly visible transition line
        }
        vg.fillColor(colorBG);
        vg.fill();
        vg.restore();
        vg.strokeWidth = 1;
        vg.strokeColor(colorFrame);
        vg.stroke();
      }

      int hgt = vg.height-3*2;

      // draw title text
      if (th > 0) {
        vg.intersectScissor(3, 3, vg.width-3*2, hgt);
        vg.textAlign = NVGTextAlign.H.Center;
        vg.textAlign = NVGTextAlign.V.Baseline;
        vg.fillColor(colorTitleFG);
        vg.text(vg.width/2, 3+cast(int)vg.textFontAscender, titlestr[0..titlelen]);
        hgt -= th;
        vg.intersectScissor(3, 3+th, vg.width-3*2, hgt);
      } else {
        vg.intersectScissor(3, 3, vg.width-3*2, hgt);
      }

      int y0 = th+(lay.textHeight < hgt ? (hgt-lay.textHeight)/2 : 0);
      vg.drawLayouter(lay, 0, 3, y0, hgt);
    }
    //flushGui();
  }

protected:
  void cbOnMouse (MouseEvent event) {
    if (this.closed) return;
    if (event == "LMB-DOWN") { this.close(); return; }
  }

public:
  this(T) (Kind akind, const(char)[] atitle, T[] atext) if (isAnyCharType!(T, true)) {
    import std.algorithm : min, max;

    kind = akind;
    if (atitle.length > titlestr.length) atitle = atitle[0..titlestr.length];
    titlelen = cast(int)atitle.length;
    if (titlelen) titlestr[0..titlelen] = atitle[0..titlelen];

    {
      import std.functional : toDelegate;

      auto oldFixFontDG = laf.fixFontDG;
      scope(exit) laf.fixFontDG = oldFixFontDG;
      laf.fixFontDG = toDelegate(&fixFontForStyleUI);

      lay = new LayTextClass(laf, Width-6);
      /*
      lay.fontStyle.fontsize = 20;
      lay.fontStyle.color = NVGColor("#aaa").asUint;
      lay.fontStyle.bgcolor = NVGColor("#222").asUint;
      lay.fontStyle.bold = true;
      //lay.lineStyle.setCenter;
      lay.putExpander();
      lay.put("popup window");
      lay.putExpander();
      lay.endPara();
      */
      lay.fontStyle.color = colorFG.asUint;
      lay.fontStyle.bgcolor = NVGColor.transparent.asUint;
      lay.fontStyle.fontsize = 18;
      lay.fontStyle.bold = false;
      lay.lineStyle.setCenter;
      lay.put(atext);
      lay.finalize();
    }
    //conwriteln("wdt=", lay.width, "; text width=", lay.textWidth);

    int wdt = Width;
    int hgt = min(max(MinHeight, lay.textHeight), MaxHeight);

    {
      auto oldWClass = sdpyWindowClass;
      scope(exit) sdpyWindowClass = oldWClass;
      sdpyWindowClass = "BIOACID_POPUP";
      super(wdt, hgt, "PopupWindow", OpenGlOptions.yes, Resizability.fixedSize, WindowTypes.undecorated, WindowFlags.skipTaskbar|WindowFlags.alwaysOnTop|WindowFlags.cannotBeActivated|WindowFlags.dontAutoShow);
    }
    //XSetWindowBackground(impl.display, impl.window, gxRGB!(0, 0, 0));

    this.onClosing = &cbOnClosing;
    this.windowResized = &cbWindowResized;
    this.visibleForTheFirstTime = &cbInit;
    this.redrawOpenGlScene = &cbOnPaint;
    this.handleMouseEvent = &cbOnMouse;

    // sorry for this hack
    setNetWMWindowType(GetAtom!("_NET_WM_WINDOW_TYPE_DOCK", true)(impl.display));
    //setNetWMWindowType(GetAtom!("_NET_WM_WINDOW_TYPE_TOOLTIP", true)(impl.display));
    {
      Atom[4] atoms;
      atoms[0] = GetAtom!("_NET_WM_STATE_STICKY", true)(impl.display);
      atoms[1] = GetAtom!("_NET_WM_STATE_SKIP_TASKBAR", true)(impl.display);
      atoms[2] = GetAtom!("_NET_WM_STATE_SKIP_PAGER", true)(impl.display);
      atoms[3] = GetAtom!("_NET_WM_STATE_ABOVE", true)(impl.display);
      XChangeProperty(
        impl.display,
        impl.window,
        GetAtom!("_NET_WM_STATE", true)(impl.display),
        XA_ATOM,
        32 /* bits */,
        0 /*PropModeReplace*/,
        atoms.ptr,
        cast(int)atoms.length);
    }
    //if (hidden) show();
    //auto wrc = getWorkArea();
    //this.moveResize(wrc.x0+x, wrc.y0+y, wdt, hgt);
    this.moveResize(6000, 6000, wdt, hgt);
    show();

    import core.time;
    dieTime = Clock.currTime+5.seconds;
    //flushGui();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared PopupWindow[] popups;


void popupKillAll () {
  if (popups.length) {
    auto pwlist = popups;
    scope(exit) delete pwlist;
    popups = null;
    foreach (ref PopupWindow pw; pwlist) if (pw !is null) { pw.close(); pw = null; }
    flushGui();
  }
}


void popupArrange () {
  import std.algorithm : min, max;
  auto wrc = PopupWindow.getWorkArea();
  int px0 = 0, py0 = wrc.y1;
  int rowWdt = 0;
  foreach (immutable pwidx, PopupWindow pw; popups) {
    if (pw is null || pw.closed || pw.hidden) continue;
    // new row?
    if (rowWdt > 0 && py0-pw.height < wrc.y0) {
      py0 = wrc.y1;
      px0 += rowWdt;
      if (px0 >= wrc.x1) break;
    }
    rowWdt = max(rowWdt, pw.width);
    int wx = px0;
    int wy = py0-pw.height;
    if (wx != pw.x0 || wy != pw.y0) {
      /*if (pw.x0 < 0)*/ pw.move(wx, wy);
      pw.x0 = wx;
      pw.y0 = wy;
    } else {
      //conwriteln("idx=", pwidx, ": SAME!");
    }
    py0 -= pw.height;
  }
  flushGui();
}


void popupRemoved (PopupWindow win) {
  if (win is null) return;
  foreach (immutable idx, PopupWindow pw; popups) {
    if (pw is win) {
      foreach (immutable c; idx+1..popups.length) popups[c-1] = popups[c];
      popups.length -= 1;
      popups.assumeSafeAppend;
      popupArrange();
      return;
    }
  }
}


void popupCheckExpirations () {
  if (popups.length == 0) return;
  auto tm = Clock.currTime;
  bool smthChanged = false;
  usize idx = 0;
  while (idx < popups.length) {
    PopupWindow pw = popups[idx];
    if (pw !is null && !pw.closed && tm >= pw.dieTime) {
      //conwriteln("tm=", tm, "; dt=", pw.dieTime, "; ", pw.dieTime >= tm);
      // first, remove from list, so `popupRemoved()` will turn to noop
      foreach (immutable c; idx+1..popups.length) popups[c-1] = popups[c];
      popups.length -= 1;
      popups.assumeSafeAppend;
      // now close
      pw.close();
      smthChanged = true;
    } else {
      ++idx;
    }
  }
  if (smthChanged) popupArrange();
  if (popups.length) postPopupCheckerEvent();
}


void showPopup(T) (PopupWindow.Kind akind, const(char)[] atitle, T[] atext) if (isAnyCharType!(T, true)) {
  if (popups.length >= 32) {
    foreach (PopupWindow pw; popups) if (pw.errTooMany) return;
    popups ~= new PopupWindow(PopupWindow.Kind.Error, "POPUP FLOOD", "too many popup windows!");
    popups[$-1].errTooMany = true;
  } else {
    while (atext.length > 0 && atext[0] <= ' ') atext = atext[1..$];
    while (atext.length > 0 && atext[$-1] <= ' ') atext = atext[0..$-1];
    if (atext.length == 0) return;
    popups ~= new PopupWindow(akind, atitle, atext);
  }
  popupArrange();
  postPopupCheckerEvent();
}
