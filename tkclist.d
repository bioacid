/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module tkclist is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.nanovega;
import iv.nanovega.blendish;
import iv.strex;
import iv.vfs.io;

import accdb;
import accobj;
import fonts;
import popups;
import icondata;
import notifyicon;
import toxproto;

import tkmain;


// ////////////////////////////////////////////////////////////////////////// //
__gshared int optCListWidth = -1;
__gshared bool optHRLastSeen = true;


// ////////////////////////////////////////////////////////////////////////// //
private extern(C) const(char)[] xfmt (const(char)* fmt, ...) {
  import core.stdc.stdio : vsnprintf;
  import core.stdc.stdarg;
  static char[64] buf = void;
  va_list ap;
  va_start(ap, fmt);
  auto len = vsnprintf(buf.ptr, buf.length, fmt, ap);
  va_end(ap);
  return buf[0..len];
}


// ////////////////////////////////////////////////////////////////////////// //
class ListItemBase {
private:
  this (CList aowner) { owner = aowner; }

protected:
  CList owner;

public:
  @property Account ownerAcc () => null;

  // setup font face for this item
  void setFontAlign () { fstash.textAlign = NVGTextAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline); }
  void setFont () { fstash.fontId = "ui"; fstash.size = 19; }
  void setSmallFont () { fstash.fontId = "ui"; fstash.size = 15; }

  @property int height () => cast(int)fstash.fontHeight;
  @property bool visible () => true;

  bool onMouse (MouseEvent event, bool meActive) => false; // true: eaten; will modify fstash
  bool onKey (KeyEvent event, bool meActive) => false; // true: eaten; will modify fstash

  protected void drawPrepare (NVGContext nvg) {
    setFont();
    setFontAlign();
    nvg.setupCtxFrom(fstash);
  }

  // no need to save context state here
  void drawAt (NVGContext nvg, int x0, int y0, int wdt, bool selected=false) {} // real rect is scissored
}


// ////////////////////////////////////////////////////////////////////////// //
class ListItemAccount : ListItemBase {
public:
  Account acc;

public:
  this (CList aowner, Account aAcc) { assert(aAcc !is null); acc = aAcc; super(aowner); }

  override @property Account ownerAcc () => acc;

  override void setFont () { fstash.fontId = "uib"; fstash.size = 20; }

  override void drawAt (NVGContext nvg, int x0, int y0, int wdt, bool selected=false) {
    drawPrepare(nvg);

    int hgt = height;
    nvg.newPath();
    nvg.rect(x0+0.5f, y0+0.5f, wdt, hgt);
    nvg.fillColor(selected ? NVGColor("#5ff0") : NVGColor("#5fff"));
    nvg.fill();

    final switch (acc.status) {
      case ContactStatus.Connecting: nvg.fillColor(NVGColor.k8orange); break;
      case ContactStatus.Offline: nvg.fillColor(NVGColor("#f00")); break;
      case ContactStatus.Online: nvg.fillColor(NVGColor("#fff")); break;
      case ContactStatus.Away: nvg.fillColor(NVGColor("#7557C7")); break;
      case ContactStatus.Busy: nvg.fillColor(NVGColor("#0057C7")); break;
    }
    if (acc.isConnecting) nvg.fillColor(NVGColor.k8orange);

    nvg.textAlign = NVGTextAlign.V.Baseline;
    nvg.textAlign = NVGTextAlign.H.Center;
    nvg.text(x0+wdt/2, y0+cast(int)nvg.textFontAscender, acc.info.nick);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class ListItemGroup : ListItemBase {
public:
  Group group;

public:
  this (CList aowner, Group aGroup) { assert(aGroup !is null); group = aGroup; super(aowner); }

  override @property Account ownerAcc () => group.acc;

  override @property bool visible () => group.visible;

  // true: eaten
  override bool onMouse (MouseEvent event, bool meActive) {
    if (event == "LMB-Down") {
      //conwriteln("!!! ", group.opened);
      group.opened = !group.opened;
      glconPostScreenRepaint();
      return true;
    }
    return false;
  }

  override void drawAt (NVGContext nvg, int x0, int y0, int wdt, bool selected=false) {
    drawPrepare(nvg);

    int hgt = height;
    nvg.newPath();
    nvg.rect(x0+0.5f, y0+0.5f, wdt, hgt);
    nvg.fillColor(selected ? NVGColor("#5880") : NVGColor("#5888"));
    nvg.fill();

    nvg.fillColor(selected ? NVGColor.white : NVGColor.yellow);
    nvg.textAlign = NVGTextAlign.V.Baseline;
    nvg.textAlign = NVGTextAlign.H.Center;
    nvg.text(x0+wdt/2, y0+cast(int)nvg.textFontAscender, group.name);

    // opened/closed sign
    //string xop = (group.opened ? "\u2bde" : "\u2bdf");
    // alas: pt sans doesn't have alot of special symbols
    string xop = (group.opened ? "\u221a" : "\u2248");
    float xopwdt = nvg.textWidth(xop);
    nvg.fillColor(group.opened ? NVGColor("#aaa") : NVGColor("#666"));
    nvg.text(x0+wdt-2-xopwdt, y0+cast(int)nvg.textFontAscender, xop);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class ListItemContact : ListItemBase {
public:
  Contact ct;

public:
  this (CList aowner, Contact aCt) { assert(aCt !is null); ct = aCt; super(aowner); }

  override @property Account ownerAcc () => ct.acc;

  override @property bool visible () => ct.visible;

  //FIXME: 16 is icon height
  override @property int height () {
    import std.algorithm : max;
    setFont();
    auto hgt = fstash.fontHeight;
    setSmallFont();
    if (ct.info.statusmsg.length) hgt += fstash.fontHeight; // status message
    if (!ct.online) hgt += fstash.fontHeight; // last seen
    return max(cast(int)hgt, 16);
  }

  // true: eaten
  override bool onMouse (MouseEvent event, bool meActive) {
    if (event == "LMB-Down") {
      owner.activeItem = this;
      if (owner.onActivateContactCB !is null) owner.onActivateContactCB(ct);
      glconPostScreenRepaint();
      return true;
    }
    return false;
  }

  override void drawAt (NVGContext nvg, int x0, int y0, int wdt, bool selected=false) {
    import std.algorithm : max;

    drawPrepare(nvg);

    int hgt = max(cast(int)nvg.textFontHeight, 16);

    if (selected) {
      int fullhgt = height;
      nvg.newPath();
      nvg.rect(x0+0.5f, y0+0.5f, wdt, fullhgt);
      nvg.fillColor(NVGColor("#9600"));
      nvg.fill();
    }

    // draw icon
    nvg.newPath();
    int iw, ih;
    NVGImage icon;

    if (ct.requestPending) {
      icon = kittyOut;
    } else if (ct.acceptPending) {
      icon = kittyFish;
    } else {
      if (ct.unreadCount == 0) icon = statusImgId[ct.status]; else icon = kittyMsg;
    }

    nvg.imageSize(icon, iw, ih);
    nvg.rect(x0, y0+(hgt-ih)/2, iw, ih);
    nvg.fillPaint(nvg.imagePattern(x0, y0+(hgt-ih)/2, iw, ih, 0, icon));
    nvg.fill();

    auto ty = y0+hgt/2;

    nvg.fillColor(NVGColor("#f70"));
    //nvg.textAlign = NVGTextAlign.V.Top;
    nvg.textAlign = NVGTextAlign.V.Middle;
    nvg.textAlign = NVGTextAlign.H.Left;
    nvg.text(x0+4+iw+4, ty, ct.displayNick);

    // draw last status and lastseen
    setSmallFont();
    nvg.setupCtxFrom(fstash);
    ty = y0+hgt+cast(int)nvg.textFontHeight/2;
    //ty += cast(int)nvg.textFontDescender;

    // draw status text
    if (ct.info.statusmsg.length) {
      nvg.fillColor = NVGColor("#777");
      nvg.textAlign = NVGTextAlign.H.Left;
      nvg.text(x0+4+iw+4, ty, ct.info.statusmsg);
      ty += cast(int)nvg.textFontHeight;
    }

    // draw lastseen
    if (!ct.online) {
      nvg.fillColor = NVGColor("#b30");
      nvg.textAlign = NVGTextAlign.H.Right;
      if (ct.info.lastonlinetime == 0) {
        nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, "last seen: never");
      } else {
        if (optHRLastSeen) {
          enum OneDay = 60*60*24;
          auto nowut = Clock.currTime.toUnixTime/OneDay;
          auto ltt = ct.info.lastonlinetime/OneDay;
               if (ltt >= nowut) nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, "last seen: today");
          else if (nowut-ltt == 1) nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, "last seen: yesterday");
          else if (nowut-ltt <= 15) nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("last seen: %u days ago", cast(int)(nowut-ltt)));
          else if (nowut-ltt <= 28) nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("last seen: %u weeks ago", cast(int)(nowut-ltt)/7));
          else if (nowut-ltt <= 31) nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, "last seen: month ago");
          else if (nowut-ltt < 365) {
            if ((nowut-ltt)%31 <= 15) {
              nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("last seen: %u month ago", cast(int)(nowut-ltt)/31));
            } else {
              nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("last seen: %u.5 month ago", cast(int)(nowut-ltt)/31));
            }
          } else {
            if ((nowut-ltt)%365 < 365/10) {
              nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("last seen: %u years ago", cast(int)(nowut-ltt)/365));
            } else {
              nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("last seen: %u.%u years ago", cast(int)(nowut-ltt)/365, cast(int)(nowut-ltt)%365/(365/10)));
            }
          }
        } else {
          SysTime dt = SysTime.fromUnixTime(ct.info.lastonlinetime);
          //auto len = snprintf(buf.ptr, buf.length, "ls: %04u/%02u/%02u %02u:%02u:%02u", cast(uint)dt.year, cast(uint)dt.month, cast(uint)dt.day, cast(uint)dt.hour, cast(uint)dt.minute, cast(uint)dt.second);
          //nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, buf[0..len]);
          nvg.text(x0+wdt-BND_SCROLLBAR_WIDTH-1, ty, xfmt("ls: %04u/%02u/%02u %02u:%02u:%02u", cast(uint)dt.year, cast(uint)dt.month, cast(uint)dt.day, cast(uint)dt.hour, cast(uint)dt.minute, cast(uint)dt.second));
        }
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// visible contact list
final class CList {
private import core.time;

private:
  int mActiveItem = -1; // active item (may be different from selected with cursor)
  int mTopY;
  int mLastX = -666, mLastY = -666, mLastHeight, mLastWidth;
  //MonoTime mLastClick = MonoTime.zero;
  //int mLastClickItem = -1;

public:
  ListItemBase[] items;

public:
  this () {}

  // the first one is "main"; can return `null`
  Account mainAccount () {
    foreach (ListItemBase li; items) if (auto lc = cast(ListItemAccount)li) return lc.acc;
    return null;
  }

  // the first one is "main"; can return `null`
  Account accountByPK (in ref PubKey pk) {
    foreach (ListItemBase li; items) {
      if (auto lc = cast(ListItemAccount)li) {
        if (lc.acc.toxpk[] == pk[]) return lc.acc;
      }
    }
    return null;
  }

  void forEachAccount (scope void delegate (Account acc) dg) {
    if (dg is null) return;
    foreach (ListItemBase li; items) if (auto lc = cast(ListItemAccount)li) dg(lc.acc);
  }

  void opOpAssign(string op:"~") (ListItemBase li) {
    if (li is null) return;
    items ~= li;
  }

  void setFont () {
    fstash.fontId = "ui";
    fstash.size = 20;
    //fstash.blur = 0;
    fstash.textAlign = NVGTextAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Baseline);
  }

  void removeAccount (Account acc) {
    if (acc is null) return;
    usize pos = 0, dest = 0;
    while (pos < items.length) {
      if (items[pos].ownerAcc !is acc) {
        if (pos != dest) items[dest++] = items[pos];
      }
      ++pos;
    }
    items.length = dest;
  }

  void buildAccount (Account acc) {
    if (acc is null) return;
    //FIXME
    mActiveItem = -1;
    mTopY = 0;
    removeAccount(acc);
    items ~= new ListItemAccount(this, acc);
    Contact[] css;
    scope(exit) delete css;
    foreach (Group g; acc.groups) {
      items ~= new ListItemGroup(this, g);
      css.length = 0;
      css.assumeSafeAppend;
      foreach (Contact c; acc.contacts.byValue) if (c.gid == g.gid) css ~= c;
      import std.algorithm : sort;
      css.sort!((a, b) {
        string s0 = a.displayNick;
        string s1 = b.displayNick;
        auto xlen = (s0.length < s1.length ? s0.length : s1.length);
        foreach (immutable idx, char c0; s0[0..xlen]) {
          if (c0 >= 'A' && c0 <= 'Z') c0 += 32; // poor man's tolower()
          char c1 = s1[idx];
          if (c1 >= 'A' && c1 <= 'Z') c1 += 32; // poor man's tolower()
          if (auto d = c0-c1) return (d < 0);
        }
        return (s0.length < s1.length);
      });
      foreach (Contact c; css) items ~= new ListItemContact(this, c);
    }
  }

  // -1: oops
  // should be called after clist was drawn at least once
  int itemAtY (int aty) {
    if (aty < 0 || mLastHeight < 1 || aty >= mLastHeight) return -1;
    aty += mTopY;
    setFont();
    foreach (immutable iidx, ListItemBase li; items) {
      if (iidx != mActiveItem && !li.visible) continue;
      li.setFont();
      int lh = li.height;
      if (lh < 1) continue;
      if (aty < lh) return cast(int)iidx;
      aty -= lh;
    }
    return -1;
  }

  final int calcTotalHeight () {
    setFont();
    int totalHeight = 0;
    foreach (immutable iidx, ListItemBase li; items) {
      if (iidx != mActiveItem && !li.visible) continue;
      li.setFont();
      int lh = li.height;
      if (lh < 1) continue;
      totalHeight += lh;
    }
    return totalHeight;
  }

  static struct AIPos {
    int y, hgt;
    @property bool valid () const pure nothrow @safe @nogc => (hgt > 0);
  }

  AIPos calcActiveItemPos () {
    AIPos res;
    if (mActiveItem < 0 || mActiveItem >= items.length) return res;
    setFont();
    foreach (immutable iidx, ListItemBase li; items) {
      if (iidx != mActiveItem && !li.visible) continue;
      li.setFont();
      int lh = li.height;
      if (lh < 1) continue;
      if (iidx == mActiveItem) { res.hgt = lh; return res; }
      res.y += lh;
    }
    res = res.init;
    return res;
  }

  final void resetActiveItem () nothrow @safe @nogc { mActiveItem = -1; }

  final @property int activeItemIndex () const pure nothrow @safe @nogc => mActiveItem;

  final @property void activeItemIndex (int idx) nothrow @trusted {
    if (idx < 0 || idx >= items.length) idx = -1;
    if (mActiveItem == idx) return;
    mActiveItem = idx;
    try { glconPostScreenRepaint(); } catch (Exception e) {} // sorry
  }

  final @property inout(ListItemBase) activeItem () inout pure nothrow @trusted @nogc {
    pragma(inline, true);
    return (mActiveItem >= 0 && mActiveItem < items.length ? cast(inout)items[mActiveItem] : null);
  }

  final @property void activeItem (ListItemBase it) nothrow @trusted {
    int idx = -1;
    if (it !is null) {
      foreach (immutable ii, const ListItemBase li; items) {
        if (li is it) { idx = cast(int)ii; break; }
      }
    }
    activeItemIndex = idx;
  }

  final bool isActiveContact (in ref PubKey pk) const nothrow @trusted {
    if (mActiveItem < 0 || mActiveItem >= items.length) return false;
    if (auto ct = cast(const(ListItemContact))items[mActiveItem]) return (ct.ct.info.pubkey[] == pk[]);
    return false;
  }

  // if called with `null` ct, deactivate
  void delegate (Contact ct) onActivateContactCB;

  // true: eaten
  bool onMouse (MouseEvent event) {
    if (mLastWidth < 1 || mLastHeight < 1) return false;
    int mx = event.x-mLastX;
    int my = event.y-mLastY;
    if (mx < 0 || my < 0 || mx >= mLastWidth || my >= mLastHeight) return false;

    int it = itemAtY(my);
    if (it >= 0) {
      if (items[it].onMouse(event, it == mActiveItem)) return true;
    }

    enum ScrollHeight = 32;

    if (event == "WheelUp") {
      if (mTopY > 0) {
        mTopY -= ScrollHeight;
        if (mTopY < 0) mTopY = 0;
        glconPostScreenRepaint();
      }
      return true;
    }

    if (event == "WheelDown") {
      auto th = calcTotalHeight()-mLastHeight;
      if (th > 0 && mTopY < th) {
        mTopY += ScrollHeight;
        if (mTopY > th) mTopY = th;
        glconPostScreenRepaint();
      }
      return true;
    }

    return true;
  }

  void makeActiveItemVisible () {
    if (mActiveItem < 0 || mActiveItem >= items.length || mLastHeight < 1) return;
    auto pos = calcActiveItemPos();
    if (!pos.valid) return;
    auto oty = mTopY;
    if (mTopY > pos.y) {
      mTopY = pos.y;
    } else if (mTopY+mLastHeight < pos.y+pos.hgt) {
      mTopY = pos.y-(mLastHeight-pos.hgt);
      if (mTopY < 0) mTopY = 0;
    }
    if (mTopY != oty) glconPostScreenRepaint();
  }

  // true: eaten
  bool onKey (KeyEvent event) {
    // jump to next contact with unread messages
    if (event == "*-C-U") {
      if (event == "D-C-U") {
        foreach (immutable iidx, ListItemBase li; items) {
          if (auto lc = cast(ListItemContact)li) {
            if (lc.ct.kfd) continue;
            if (lc.ct.unreadCount > 0) {
              if (mActiveItem != iidx) {
                mActiveItem = iidx;
                makeActiveItemVisible();
                glconPostScreenRepaint();
                if (onActivateContactCB !is null) onActivateContactCB(lc.ct);
                return true; // oops; stop right here ;-)
              }
            }
          }
        }
      }
      return true;
    }
    return false;
  }

  private float sbpos, sbsize; // set in `drawAt()`

  // the following two should be called after `drawAt()`; if `sbSize()` is <0, draw nothing
  final float sbPosition () pure const nothrow @safe @nogc => sbpos;
  final float sbSize () pure const nothrow @safe @nogc => sbsize;

  // real rect is scissored
  void drawAt (NVGContext nvg, int x0, int y0, int wdt, int hgt) {
    mLastX = x0;
    mLastY = y0;
    mLastHeight = hgt;
    mLastWidth = wdt;

    nvg.save();
    scope(exit) nvg.restore();

    setFont();
    nvg.setupCtxFrom(fstash);

    int totalHeight = calcTotalHeight();
    if (mTopY > totalHeight-hgt) mTopY = totalHeight-hgt;
    if (mTopY < 0) mTopY = 0;

    int y = -mTopY;
    foreach (immutable iidx, ListItemBase li; items) {
      if (iidx != mActiveItem && !li.visible) continue;
      li.setFont();
      int lh = li.height;
      if (lh < 1) continue;
      if (y+lh > 0 && y < hgt) {
        nvg.save();
        scope(exit) nvg.restore();
        version(all) {
          nvg.intersectScissor(x0, y0+y, wdt, lh);
          li.drawAt(nvg, x0, y0+y, wdt, (iidx == mActiveItem));
        } else {
          nvg.translate(x0+0.5f, y0+y+0.5f);
          nvg.intersectScissor(0, 0, wdt, lh);
          li.drawAt(nvg, 0, 0, wdt);
        }
      }
      y += lh;
      if (y >= hgt) break;
    }

    if (totalHeight > hgt) {
      //nvg.bndScrollBar(x0+wdt-BND_SCROLLBAR_WIDTH+0.5f, y0+0.5f, BND_SCROLLBAR_WIDTH, hgt, BND_DEFAULT, mTopY/cast(float)(totalHeight-hgt), hgt/cast(float)totalHeight);
      sbpos = mTopY/cast(float)(totalHeight-hgt);
      sbsize = hgt/cast(float)totalHeight;
    } else {
      //nvg.bndScrollBar(x0+wdt-BND_SCROLLBAR_WIDTH+0.5f, y0+0.5f, BND_SCROLLBAR_WIDTH, hgt, BND_DEFAULT, 1, 1);
      sbpos = 0;
      sbsize = -1;
    }
  }
}
