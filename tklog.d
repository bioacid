/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module tklog is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.nanovega;
import iv.nanovega.blendish;
import iv.nanovega.textlayouter;
import iv.strex;
import iv.tox;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

import accdb;
import accobj;
import fonts;
import popups;
import icondata;
import notifyicon;
import toxproto;

import tkmain;


// ////////////////////////////////////////////////////////////////////////// //
alias LayTextClass = LayTextD;


// ////////////////////////////////////////////////////////////////////////// //
__gshared LayTextClass lay;
__gshared int layWinHeight = 0;
__gshared int layOffset = 0; // from bottom

static struct LayUrl {
  string url;
  union {
    usize refcount;
    usize nextFree; // index+1
  }

  @property bool isFree () const nothrow @safe @nogc => (url.length == 0);
}

private __gshared usize[string] layUrlIndex;
private __gshared usize layUrlFreeHead = 0;
private __gshared LayUrl[] layUrlArray;


private usize allocUrl (string url) {
  usize res = layUrlFreeHead;
  if (res) {
    --res; // normalize
    layUrlFreeHead = layUrlArray[res].nextFree;
  } else {
    res = layUrlArray.length;
    layUrlArray.length += 1;
    layUrlArray.assumeSafeAppend;
  }
  layUrlArray[res].url = url;
  layUrlArray[res].refcount = 1;
  layUrlIndex[url] = res;
  return res;
}


private void freeUrl (usize idx) {
  assert(idx < layUrlArray.length);
  assert(!layUrlArray[idx].isFree);
  if (--layUrlArray[idx].refcount) return;
  //conwriteln("FREE URL idx=", idx, "; url=[", layUrlArray[idx].url, "]");
  layUrlIndex.remove(layUrlArray[idx].url);
  layUrlArray[idx].url = null;
  layUrlArray[idx].nextFree = layUrlFreeHead;
  layUrlFreeHead = idx+1;
}


string findUrlByWordIndex (uint widx) {
  auto w = lay.wordByIndex(widx);
  if (w is null) return null;
  usize pos = w.udata;
  if (pos < 1 || pos > layUrlArray.length) return null;
  //conwriteln("  pos=", pos, "; url=[", layUrlArray[pos-1].url, "]");
  --pos;
  return layUrlArray[pos].url;
}


private void appendUrl (uint widx, string url) {
  auto w = lay.wordByIndex(widx);
  if (w is null) return;
  assert(w.udata == 0);
  if (url.length == 0) return;
  auto ip = url in layUrlIndex;
  if (ip) {
    assert(!layUrlArray[*ip].isFree);
    ++layUrlArray[*ip].refcount;
    w.udata = (*ip)+1;
    //conwriteln("OLD URL widx=", widx, "; udata=", w.udata, "; rc=", layUrlArray[*ip].refcount, "; url=[", layUrlArray[*ip].url, "]");
  } else {
    usize pos = allocUrl(url);
    w.udata = pos+1;
    //conwriteln("NEW URL widx=", widx, "; udata=", w.udata, "; rc=", layUrlArray[pos].refcount, "; url=[", layUrlArray[pos].url, "]");
  }
}


private void urlWordsRemoved (uint widx, int count) {
  if (count < 1) return;
  foreach (uint wpos; widx..widx+count) {
    auto w = lay.wordByIndex(wpos);
    if (w is null) continue;
    usize pos = w.udata;
    if (!pos) continue;
    --pos;
    assert(pos < layUrlArray.length);
    assert(!layUrlArray[pos].isFree);
    assert(layUrlArray[pos].refcount);
    //conwriteln("FREE WORD URL widx=", wpos, "; udata=", w.udata, "; rc=", layUrlArray[pos].refcount, "; url=[", layUrlArray[pos].url, "]");
    freeUrl(pos);
  }
}


private void urlWipeAll () {
  layUrlArray.unsafeArrayClear();
  layUrlIndex.clear();
  layUrlFreeHead = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
class MessageStart : LayObject {
  long msgid; // >0: outgoing, unacked yet

  this (long aid=-1) nothrow @safe @nogc { msgid = aid; }

  override int width () => 0;
  override int spacewidth () => 0;
  override int height () => 0;
  override int ascent () => 0;
  override int descent () => 0;
  override bool canbreak () => true;
  override bool spaced () => false;
  // y is at baseline
  override void draw (NVGContext ctx, float x, float y) {}
}


class MessageDividerStart : LayObject {
  long msgid; // >0: outgoing, unacked yet

  this (long aid=-1) nothrow @safe @nogc { msgid = aid; }

  override int width () => 0;
  override int spacewidth () => 0;
  override int height () => 0;
  override int ascent () => 0;
  override int descent () => 0;
  override bool canbreak () => true;
  override bool spaced () => false;
  // y is at baseline
  override void draw (NVGContext ctx, float x, float y) {}
}


class MessageOutMark : LayObject {
  long msgid; // >=0: outgoing, unacked yet (0: sent in offline mode)
  TextDigest digest; // text digest
  SysTime time;

  this (long aid, SysTime atime, const(void)[] atext) nothrow @safe @nogc { msgid = aid; time = atime; digest = textDigest(atext); }

  override int width () => kittyOut.width;
  override int spacewidth () => 4;
  override int height () => kittyOut.height;
  override int ascent () => 0;
  override int descent () => 0;
  override bool canbreak () => true;
  override bool spaced () => false;
  // y is at baseline
  override void draw (NVGContext ctx, float x, float y) {
    if (msgid >= 0) {
      nvg.save();
      scope(exit) nvg.restore;
      nvg.newPath();
      // +3 is a hack
      nvg.rect(x, y-height+3, width, height);
      nvg.fillPaint(nvg.imagePattern(x, y-height+3, width, height, 0, kittyOut));
      nvg.fill();
    }
  }

  final bool isOurMark (const(void)[] atext, SysTime atime) nothrow @trusted {
    pragma(inline, true);
    return (atime == time && digest[] == textDigest(atext));
  }

  final bool isOurMark (long aid, const(void)[] atext, SysTime atime) nothrow @trusted {
    pragma(inline, true);
    return (aid > 0 ? (msgid == aid) : (atime == time && digest[] == textDigest(atext)));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void logFixAckMessageId (long oldid, long newid, const(void)[] text, SysTime time) {
  if (oldid == newid) return; // nothing to do
  foreach (immutable uint widx; 0..lay.wordCount) {
    auto w = lay.wordByIndex(widx);
    int oidx = w.objectIdx;
    if (oidx >= 0) {
      if (auto maw = cast(MessageOutMark)lay.objectAtIndex(oidx)) {
        if (maw.isOurMark(oldid, text, time)) {
          maw.msgid = newid;
          glconPostScreenRepaint(); // redraw
        }
      }
    }
  }
}

void logResetAckMessageIds () {
  bool doRefresh = false;
  foreach (immutable uint widx; 0..lay.wordCount) {
    auto w = lay.wordByIndex(widx);
    int oidx = w.objectIdx;
    if (oidx >= 0) {
      if (auto maw = cast(MessageOutMark)lay.objectAtIndex(oidx)) {
        if (maw.msgid > 0) {
          maw.msgid = 0;
          doRefresh = true;
        }
      }
    }
  }
  if (doRefresh) glconPostScreenRepaint(); // redraw
}

void ackLogMessage (long msgid) {
  if (msgid <= 0) return;
  foreach (immutable uint widx; 0..lay.wordCount) {
    auto w = lay.wordByIndex(widx);
    int oidx = w.objectIdx;
    if (oidx >= 0) {
      if (auto maw = cast(MessageOutMark)lay.objectAtIndex(oidx)) {
        if (maw.msgid == msgid) {
          maw.msgid = -1; // reset mark
          glconPostScreenRepaint(); // redraw
        }
      }
    }
  }
}


// coordinates are adjusted so (0, 0) points to logical layouter top-left
/*
bool logCheckCancelMark (int mx, int my) {
  bool removeFromResendQueue (long msgid, in ref TextDigest digest, SysTime time) {
*/


// ////////////////////////////////////////////////////////////////////////// //
void wipeLog () {
  lay.wipeAll(true); // clear log, but delete objects
  urlWipeAll();
  layOffset = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
void addDividerLine (bool doflushgui=false) {
  if (glconCtlWindow is null || glconCtlWindow.closed) return;

  {
    bool inFrame = nvg.inFrame;
    if (!inFrame) {
      glconCtlWindow.setAsCurrentOpenGlContext(); // make this window active
      glViewport(0, 0, glconCtlWindow.width, glconCtlWindow.height);
      nvg.beginFrame(glconCtlWindow.width, glconCtlWindow.height);
    }
    scope(exit) {
      if (!inFrame) {
        nvg.endFrame();
        if (doflushgui) flushGui();
        glconCtlWindow.releaseCurrentOpenGlContext();
      }
    }

    uint widx = 0;
    while (widx < lay.wordCount) {
      auto w = lay.wordByIndex(widx);
      int oidx = w.objectIdx;
      if (oidx >= 0) {
        if (auto maw = cast(MessageDividerStart)lay.objectAtIndex(oidx)) {
          // this always followed by expander; remove them both
          lay.removeWordsAt(widx, 2);
          urlWordsRemoved(widx, 2);
          continue;
        }
      }
      ++widx;
    }

    lay.fontStyle.fontsize = 2;
    lay.fontStyle.color = NVGColor.k8orange.asUint;
    lay.fontStyle.bgcolor = NVGColor("#aa0").asUint;
    lay.fontStyle.monospace = true;

    lay.putObject(new MessageDividerStart());
    lay.putExpander();
    lay.endPara();
    lay.finalize();
  }

  // redraw
  glconPostScreenRepaint();
}


// ////////////////////////////////////////////////////////////////////////// //
// `ct` can be `null` for "my message" or "system message"
void addTextToLog (Account acc, Contact ct, LogFile.Msg.Kind kind, bool action,
                   const(char)[] msg, SysTime time, long msgid=-1, bool doflushgui=false)
{
  if (glconCtlWindow is null || glconCtlWindow.closed) return;

  {
    bool inFrame = nvg.inFrame;
    if (!inFrame) {
      glconCtlWindow.setAsCurrentOpenGlContext(); // make this window active
      nvg.beginFrame(glconCtlWindow.width, glconCtlWindow.height);
    }
    scope(exit) {
      if (!inFrame) {
        nvg.endFrame();
        if (doflushgui) flushGui();
        glconCtlWindow.releaseCurrentOpenGlContext();
      }
    }

    // create header

    // add "message start" mark
    lay.putObject(new MessageStart(msgid));

    lay.fontStyle.fontsize = 16;
    lay.fontStyle.color = NVGColor.k8orange.asUint;
    //lay.fontStyle.bgcolor = NVGColor("#222").asUint;
    lay.fontStyle.bgcolor = NVGColor("#5888").asUint;
    lay.fontStyle.monospace = true;

    NVGColor textColor;
    int fontSize = 20;

    final switch (kind) {
      case LogFile.Msg.Kind.Outgoing:
        textColor = NVGColor.k8orange; lay.fontStyle.color = NVGColor("#c40").asUint;
        lay.put(acc.info.nick);
        //fontSize = 19;
        break;
      case LogFile.Msg.Kind.Incoming:
        textColor = NVGColor("#aaa");
        lay.fontStyle.color = NVGColor("#777").asUint;
        lay.put(ct.info.nick);
        //fontSize = 21;
        break;
      case LogFile.Msg.Kind.Notification:
        textColor = NVGColor("#0c0");
        lay.fontStyle.color = textColor.asUint;
        lay.put("*system*");
        //fontSize = 20;
        break;
    }

    lay.fontStyle.monospace = false;
    //lay.putHardSpace(64);
    lay.putExpander();
    // add "message outgoing" mark
    if (kind == LogFile.Msg.Kind.Outgoing) {
      //conwriteln("OG: msgid=", msgid);
      if (msgid <= 0 && ct !is null) {
        msgid = ct.findInResendQueue(msg, time);
        //conwriteln("  new msgid=", msgid);
      }
      lay.putObject(new MessageOutMark(msgid, time, msg));
      lay.putExpander();
    }

    // date
    if (kind == LogFile.Msg.Kind.Incoming) lay.fontStyle.color = NVGColor("#888").asUint;
    {
      import std.datetime;
      import std.format : format;
      auto dt = cast(DateTime)time;
      string tstr = "%04u/%02u/%02u".format(dt.year, dt.month, dt.day);
      lay.put(tstr);
      lay.putNBSP();
    }

    // time
    lay.fontStyle.color = (kind == LogFile.Msg.Kind.Incoming ? NVGColor("#888").asUint : NVGColor("#666").asUint);
    lay.fontStyle.bgcolor = NVGColor("#006").asUint;
    {
      import std.datetime;
      import std.format : format;
      auto dt = cast(DateTime)time;
      string tstr = "%02u:%02u:%02u".format(dt.hour, dt.minute, dt.second);
      lay.put(tstr);
      lay.putNBSP();
    }
    lay.endPara();

    // message text
    lay.fontStyle.bgcolor = NVGColor.transparent.asUint;
    lay.fontStyle.fontsize = fontSize;

    // action mark
    if (action) {
      lay.fontStyle.color = NVGColor("#fff").asUint;
      lay.put("/me ");
    }

    static bool isWordDelim (dchar ch) {
      import std.uni : isWhite;
      return
        ch == lay.EndLineCh || ch == lay.EndParaCh ||
        //ch == lay.NBSpaceCh || ch == lay.NarrowNBSpaceCh ||
        ch == lay.SoftHyphenCh ||
        ch <= ' ' || isWhite(ch);
    }

    void layPutSplitLongWords (const(char)[] str) {
      enum MaxCharsSplit = 16;
      Utf8Decoder dec;
      dec.reset();
      int lastWordLength = 0;
      while (str.length) {
        dchar curCh = dec.decode(cast(ubyte)str[0]);
        str = str[1..$];
        if (curCh > dchar.max) continue;
        if (isWordDelim(curCh)) {
          lastWordLength = 0;
        } else {
          if (++lastWordLength >= MaxCharsSplit) {
            lay.putSoftHypen();
            lastWordLength = 0;
          }
        }
        lay.put(curCh);
      }
    }

    bool wasNL = !action;
    bool inQuote = false;

    void setFontStyle () {
      if (inQuote) {
        lay.fontStyle.color = NVGColor("#ff0").asUint;
        lay.fontStyle.fontsize = fontSize-1;
        lay.fontStyle.italic = true;
      } else {
        lay.fontStyle.color = textColor.asUint;
        lay.fontStyle.fontsize = fontSize;
        lay.fontStyle.italic = false;
      }
    }

    void checkQuote (const(char)[] str) {
      if (wasNL) {
        inQuote = (str[0] == '>');
        setFontStyle();
      }
    }

    void xput (const(char)[] str) {
      while (str.length) {
        checkQuote(str);
        auto nl = str.indexOf('\n');
        if (nl < 0) {
          layPutSplitLongWords(str);
          break;
        }
        if (nl > 0) {
          layPutSplitLongWords(str[0..nl]);
        }
        lay.endPara();
        wasNL = !action;
        inQuote = false;
        str = str[nl+1..$];
      }
    }

    msg = msg.xstripright;

    lay.fontStyle.color = textColor.asUint;
    while (msg.length) {
      auto nfo = urlDetect(msg);
      if (!nfo.valid) {
        xput(msg);
        break;
      }
      // url found
      xput(msg[0..nfo.pos]);
      string url = msg[nfo.pos..nfo.end].idup;
      msg = msg[nfo.end..$];
      auto stword = lay.nextWordIndex;
      lay.pushStyles();
      auto c = lay.fontStyle.color;
      scope(exit) { lay.popStyles; lay.fontStyle.color = c; }
      lay.fontStyle.href = true;
      lay.fontStyle.underline = true;
      lay.fontStyle.color = NVGColor("#06f").asUint;
      lay.put(url);
      if (msg.length) {
        lay.endWord(spaced:(msg[0] <= ' ' && msg[0] != '\n'));
      } else {
        lay.endWord(spaced:false);
      }
      while (stword < lay.nextWordIndex) {
        appendUrl(stword, url);
        ++stword;
      }
      setFontStyle();
    }

    lay.endPara();
    lay.finalize();
  }
  // redraw
  glconPostScreenRepaint();
}


void addTextToLog (Account acc, Contact ct, in ref LogFile.Msg msg, long msgid=-1, bool doflushgui=false) {
  import iv.utfutil : utf8Encode;
  char[] text;
  text.reserve(4096);
  scope(exit) delete text;
  // decode text
  foreach (dchar dc; msg.byDChar) {
    char[4] buf = void;
    auto len = utf8Encode(buf[], dc);
    text ~= buf[0..len];
  }
  addTextToLog(acc, ct, msg.kind, msg.isMe, text, msg.time, msgid, doflushgui);
}
