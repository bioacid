/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module tkmain is aliced;

import std.datetime;

import arsd.color;
import arsd.image;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.gxx;
import iv.meta;
import iv.nanovega;
import iv.nanovega.blendish;
import iv.nanovega.textlayouter;
import iv.strex;
import iv.tox;
import iv.sdpyutil;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

version(sfnt_test) import iv.nanovega.simplefont;

import accdb;
import accobj;
import fonts;
import icondata;
import notifyicon;
import toxproto;

import tkclist;
import tklog;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool mainWindowActive = false;
__gshared bool mainWindowVisible = false;
__gshared SimpleWindow sdmain;
__gshared CList clist;


void setupToxCoreSender () {
  toxCoreSendEvent = delegate (Object msg) {
    if (msg is null) return; // just in case
    try {
      if (glconCtlWindow is null || glconCtlWindow.closed) return;
      glconCtlWindow.postEvent(msg);
    } catch (Exception e) {}
  };
}


// ////////////////////////////////////////////////////////////////////////// //
string getBrowserCommand (bool forceOpera=false) {
  __gshared string browser;
  if (forceOpera) return "opera";
  if (browser.length == 0) {
    import core.stdc.stdlib : getenv;
    const(char)* evar = getenv("BROWSER");
    if (evar !is null && evar[0]) {
      import std.string : fromStringz;
      browser = evar.fromStringz.idup;
    } else {
      browser = "opera";
    }
  }
  return browser;
}


void openUrl (ConString url, bool forceOpera=false) {
  if (url.length) {
    import std.stdio : File;
    import std.process;
    try {
      auto frd = File("/dev/null");
      auto fwr = File("/dev/null", "w");
      spawnProcess([getBrowserCommand(forceOpera), url.idup], frd, fwr, fwr, null, Config.detached);
    } catch (Exception e) {
      conwriteln("ERROR executing URL viewer (", e.msg, ")");
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct UrlInfo {
  int pos = -1, len = 0;

  @property bool valid () const pure nothrow @safe @nogc => (pos >= 0 && len > 0);
  @property int end () const pure nothrow @safe @nogc => (pos >= 0 && len > 0 ? pos+len : 0);

  static UrlInfo Invalid () pure nothrow @safe @nogc => UrlInfo.init;
}


UrlInfo urlDetect (const(char)[] text) nothrow @trusted @nogc {
  import iv.strex;
  UrlInfo res;
  auto dlpos = text.indexOf("://");
  if (dlpos < 3) return res;

  //{ import core.stdc.stdio; printf("det: <%.*s>\n", cast(uint)text.length, text.ptr); }

  bool isProto (const(char)[] prt) nothrow @trusted @nogc {
    if (dlpos < prt.length) return false;
    if (!strEquCI(prt, text[dlpos-prt.length..dlpos])) return false;
    // check word boundary
    if (dlpos == prt.length) return true;
    return !isalpha(text[dlpos-prt.length-1]);
  }

       if (isProto("ftp")) res.pos = cast(int)(dlpos-3);
  else if (isProto("http")) res.pos = cast(int)(dlpos-4);
  else if (isProto("https")) res.pos = cast(int)(dlpos-5);
  else return res;

  dlpos += 3; // skip "://"

  // skip host name
  for (; dlpos < text.length; ++dlpos) {
    char ch = text[dlpos];
    if (ch == '/') break;
    if (!(isalnum(ch) || ch == '.' || ch == '-' || ch == ':' || ch == '@')) break;
  }

  // skip path
  char[64] brcStack;
  int brcSP = 0;
  bool wasSharp = false;

  for (; dlpos < text.length; ++dlpos) {
    char ch = text[dlpos];
    if (ch <= ' ' || ch == '<' || ch == '>' || ch == '"' || ch == '\'' || ch >= 127) break;
    // hash
    if (ch == '#') {
      if (wasSharp) break;
      wasSharp = true;
      brcSP = 0;
      continue;
    }
    // next path part
    if (ch == '/') {
      brcSP = 0;
      continue;
    }
    // opening bracket
    if (ch == '(' || ch == '[' || ch == '{') {
      if (dlpos+1 >= text.length || (!isalnum(text[dlpos+1]) && text[dlpos+1] != '_')) break;
      final switch (ch) {
        case '(': ch = ')'; break;
        case '[': ch = ']'; break;
        case '{': ch = '}'; break;
      }
      if (brcSP < brcStack.length) brcStack[brcSP++] = ch;
      continue;
    }
    // closing bracket
    if (ch == ')' || ch == ']' || ch == '}') {
      if (brcSP == 0 || brcStack[brcSP-1] != ch) break;
      --brcSP;
      continue;
    }
    if (brcSP == 0) {
      if (ch == '.') {
        if (brcSP == 0) {
          if (dlpos+1 >= text.length || (!isalnum(text[dlpos+1]) && text[dlpos+1] != '_')) break;
        }
        continue;
      }
      if (!isalnum(ch)) {
        // other special chars
        if (dlpos+1 >= text.length) break; // no more chars, ignore
        if (ch == '-' && dlpos < text.length && (isalnum(text[dlpos+1]) || text[dlpos+1] == '%')) continue;
        if (!isalnum(text[dlpos+1]) && text[dlpos+1] != '_') {
          if (ch == '.' || ch == '!' || ch == ';' || ch == ',' || text[dlpos+1] != ch) break; // ignore
        }
        continue;
      }
    }
  }

  res.len = cast(int)(dlpos-res.pos);

  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared NVGContext nvg = null;
__gshared NVGImage nvgSkullsImg;
__gshared NVGImage kittyOut, kittyMsg, kittyFish;
__gshared NVGImage[5] statusImgId;

__gshared int lastWindowWidth = -1;


shared static ~this () {
  //{ import core.stdc.stdio; printf("******************************\n"); }
  nvgSkullsImg.clear();
  //{ import core.stdc.stdio; printf("---\n"); }
  foreach (ref img; statusImgId[]) img.clear();
  kittyOut.clear();
  kittyMsg.clear();
  kittyFish.clear();
}


void buildStatusImages () {
  version(none) {
    statusImgId[ContactStatus.Offline] = nvg.createImageRGBA(16, 16, ctiOffline[], NVGImageFlags.NoFiltering);
    statusImgId[ContactStatus.Online] = nvg.createImageRGBA(16, 16, ctiOnline[], NVGImageFlags.NoFiltering);
    statusImgId[ContactStatus.Away] = nvg.createImageRGBA(16, 16, ctiAway[], NVGImageFlags.NoFiltering);
    statusImgId[ContactStatus.Connecting] = nvg.createImageRGBA(16, 16, ctiOffline[], NVGImageFlags.NoFiltering);
  } else {
    //{ import core.stdc.stdio; printf("creating status image: Offline\n"); }
    statusImgId[ContactStatus.Offline] = nvg.createImageRGBA(16, 16, baph16Gray[], NVGImageFlags.NoFiltering);
    //{ import core.stdc.stdio; printf("creating status image: Online\n"); }
    statusImgId[ContactStatus.Online] = nvg.createImageRGBA(16, 16, baph16Online[], NVGImageFlags.NoFiltering);
    //{ import core.stdc.stdio; printf("creating status image: Away\n"); }
    statusImgId[ContactStatus.Away] = nvg.createImageRGBA(16, 16, baph16Away[], NVGImageFlags.NoFiltering);
    //{ import core.stdc.stdio; printf("creating status image: Busy\n"); }
    statusImgId[ContactStatus.Busy] = nvg.createImageRGBA(16, 16, baph16Busy[], NVGImageFlags.NoFiltering);
    //{ import core.stdc.stdio; printf("creating status image: Connecting\n"); }
    statusImgId[ContactStatus.Connecting] = nvg.createImageRGBA(16, 16, baph16Orange[], NVGImageFlags.NoFiltering);
    //{ import core.stdc.stdio; printf("+++ creatied status images...\n"); }
    kittyOut = nvg.createImageRGBA(16, 16, kittyOutgoing[], NVGImageFlags.NoFiltering);
    kittyMsg = nvg.createImageRGBA(16, 16, kittyMessage[], NVGImageFlags.NoFiltering);
    kittyFish = nvg.createImageRGBA(16, 16, kittyFish16[], NVGImageFlags.NoFiltering);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void loadFonts () {
  nvg.fonsContext.addFontsFrom(fstash);
  bndSetFont(nvg.findFont("ui"));
}


// ////////////////////////////////////////////////////////////////////////// //
void loadAccount (string nick, bool allowCreate) {
  if (nick.length == 0) assert(0, "wtf?!");

  Account acc;
  bool newAcc = false;

  try {
    acc = new Account(nick);
  } catch (Exception e) {
    conwriteln("error opening account... (error: ", e.msg, ")");
    if (!allowCreate) throw e;
    acc = Account.CreateNew(nick, nick);
    newAcc = true;
  }

  // create fake contact
  if (newAcc && acc.contacts.length == 0 && nick == "_fakeacc") {
    conwriteln("creating fake contact...");
    auto c = acc.createEmptyContact();
    c.info.nick = "test contact";
    c.info.pubkey[] = 0x55;
    c.save();
  }

  clist.buildAccount(acc);
}


// ////////////////////////////////////////////////////////////////////////// //
// null: deactivate
void doActivateContact (Contact ct) {
  if (sdmain is null || sdmain.closed) return;
  if (activeContact is ct) return;

  activeContact = ct;
  wipeLog();
  if (ct is null) {
    //conwriteln("clear log");
    if (clist !is null) clist.resetActiveItem();
    sdmain.title = "BioAcid";
    glconPostScreenRepaint();
  } else if (ct.acceptPending) {
    addTextToLog(ct.acc, ct, LogFile.Msg.Kind.Notification, false, (ct.statusmsg.length ? ct.statusmsg : "I brought you a tasty fish!"), systimeNow);
  } else {
    import std.format : format;
    sdmain.title = "%s [%s] -- BioAcid".format(ct.info.nick, tox_hex(ct.info.pubkey));
    LogFile log;
    ct.loadLogInto(log);
    auto mcount = cast(int)log.messages.length;
    int left = ct.hmcOnOpen;
    if (left < ct.unreadCount) left = ct.unreadCount;
    if (left > mcount) left = mcount;
    if (mcount > left) mcount = left;
    if (mcount > 0) {
      foreach (const ref msg; log.messages[$-mcount..$]) {
        if (left == ct.unreadCount) addDividerLine();
        addTextToLog(ct.acc, ct, msg);
        --left;
      }
    }
    if (ct.unreadCount != 0) { ct.unreadCount = 0; ct.saveUnreadCount(); }
  }

  fixTrayIcon();
}


//FIXME: scan all accounts
void fixTrayIcon () {
  if (clist is null) return;
  auto acc = clist.mainAccount;
  if (acc is null) return;
  int unc = 0;
  foreach (Contact ct; acc) unc += ct.unreadCount;
  if (unc) {
    import std.format : format;
    setTrayUnread();
    setHint("unread: %d".format(unc));
  } else {
    setTrayStatus(acc.status);
    final switch (acc.status) {
      case ContactStatus.Connecting: setHint("connecting..."); break;
      case ContactStatus.Offline: setHint("offline"); break;
      case ContactStatus.Online: setHint("online"); break;
      case ContactStatus.Away: setHint("away"); break;
      case ContactStatus.Busy: setHint("busy"); break;
    }
  }
}


void fixUnreadIndicators () {
  if (!mainWindowVisible || !mainWindowActive) return; // nothing to do
  if (activeContact is null || activeContact.unreadCount == 0) return; // nothing to do
  activeContact.unreadCount = 0;
  activeContact.unreadCount = 0;
  activeContact.saveUnreadCount();
  fixTrayIcon();
}


// ////////////////////////////////////////////////////////////////////////// //
void addContactCommands () {
  conRegFunc!((ConString grpname) {
    if (clist is null || sdmain is null || sdmain.closed) return;
    auto acc = clist.mainAccount;
    if (acc is null) return;
    if (grpname.length == 0) { conwriteln("group name?"); return; }
    if (acc.findGroupByName(grpname) != uint.max) { conwriteln("group <", grpname, "> already exists"); return; }
    auto gid = acc.createGroup(grpname);
    if (gid == uint.max) { conwriteln("cannot create group <", grpname, ">"); return; }
    clist.buildAccount(acc);
    glconPostScreenRepaint();
  })("group_create", "create group");


  conRegFunc!((ConString grpname) {
    if (clist is null || sdmain is null || sdmain.closed) return;
    if (activeContact is null) { conwriteln("please, select contact first"); return; }
    if (grpname.length == 0) { conwriteln("group name?"); return; }
    auto gid = activeContact.acc.createGroup(grpname);
    if (gid == uint.max) { conwriteln("cannot create group <", grpname, ">"); return; }
    if (activeContact.acc.moveContactToGroup(activeContact, gid)) {
      clist.buildAccount(activeContact.acc);
      glconPostScreenRepaint();
    } else {
      conwriteln("cannot move contact to new group");
    }
  })("move_to_group", "move current contact to named group");


  conRegFunc!(() {
    if (clist is null || sdmain is null || sdmain.closed) return;
    if (activeContact is null) { conwriteln("please, select contact first"); return; }
    auto acc = activeContact.acc;
    if (!acc.isOnline) { conwriteln("you must be online to remove contacts"); return; }
    if (!acc.removeContact(activeContact)) { conwriteln("cannot remove current contact"); return; }
    wipeLog();
    activeContact = null;
    clist.buildAccount(acc);
    glconPostScreenRepaint();
  })("contact_remove", "unfriend and remove current contact");
}
