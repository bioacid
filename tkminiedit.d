/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module tkminiedit is aliced;
private:

import arsd.color;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.nanovega;
import iv.strex;
import iv.sdpyutil;
import iv.unarray;
import iv.utfutil;
import iv.vfs.io;

import fonts;


// ////////////////////////////////////////////////////////////////////////// //
private struct MiniEditKB { string evt; }


// ////////////////////////////////////////////////////////////////////////// //
public final class MiniEdit {
private import iv.utfutil : Utf8Decoder;
private:
  dchar[] dtext;
  Utf8Decoder ec; // encoder for clipboard gets
  int curpos;
  int lastWidth = -1;
  bool allowMultiline = true;

private:
  void setFont () nothrow @trusted @nogc {
    fstash.size = 20;
    fstash.fontId = "ui";
    fstash.textAlign = NVGTextAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);
  }

  void setFont (NVGContext nvg) nothrow @trusted @nogc {
    setFont();
    nvg.setupCtxFrom(fstash);
  }

private:
  static struct Line {
    uint stpos;
    const(dchar)[] text; // never has final EOL
    bool wrap; // true: this line was soft-wrapped

    string utftext () const nothrow @safe {
      import iv.utfutil : utf8Encode;
      string res;
      res.reserve(text.length*4);
      foreach (dchar dc; text) {
        char[4] buf = void;
        auto len = utf8Encode(buf[], dc);
        res ~= buf[0..len];
      }
      return res;
    }
  }

  bool isNewlineChar (char ch) const nothrow @safe @nogc { pragma(inline, true); return (allowMultiline && ch == '\n'); }
  bool isNewlineChar (dchar ch) const nothrow @safe @nogc { pragma(inline, true); return (allowMultiline && ch == '\n'); }

  // `line` in delegate cannot be empty
  // if `line` ends with '\n', this is hard newline, otherwise it is a wrap
  void byLine() (scope void delegate (scope Line line) nothrow dg) {
    assert(dg !is null);

    if (dtext.length == 0) return;

    setFont();
    int width = lastWidth;
    if (width < 1) width = 1; // just for fun

    auto tbi = FONSTextBoundsIterator(fstash);
    uint linestart = 0;
    uint pos = 0;

    while (pos < dtext.length) {
      // forced newline?
      if (isNewlineChar(dtext[pos])) {
        dg(Line(linestart, dtext[linestart..pos], false)); // not wrapped
        tbi.reset(fstash);
        linestart = ++pos;
        continue;
      }
      // find word end
      uint epos = pos;
      uint lastgoodpos = epos; // for faster wrapping
      while (epos < dtext.length && !isNewlineChar(dtext[epos]) && dtext[epos] <= ' ') {
        if (tbi.advance < width) lastgoodpos = epos;
        tbi.put(dtext[epos++]);
      }
      while (epos < dtext.length && dtext[epos] > ' ') {
        if (tbi.advance < width) lastgoodpos = epos;
        tbi.put(dtext[epos++]);
      }
      // if we have spaces, they should fit too
      while (epos < dtext.length && !isNewlineChar(dtext[epos]) && dtext[epos] <= ' ') {
        if (tbi.advance < width) lastgoodpos = epos;
        tbi.put(dtext[epos++]);
      }
      // does it fit?
      if (tbi.advance < width) {
        pos = epos;
        continue;
      }
      // if we have some words, generate line
      if (pos > linestart) {
        dg(Line(linestart, dtext[linestart..pos], true)); // wrapped
        tbi.reset(fstash);
        linestart = pos;
        continue;
      }
      //conwriteln("  pos=", pos, "; epos=", epos, "; adv=", tbi.advance, "; width=", width);
      // we need at least one char, and it is guaranteed by `+1`
      epos = lastgoodpos+(lastgoodpos == linestart ? 1 : 0);
      dg(Line(linestart, dtext[linestart..epos], true)); // wrapped
      tbi.reset(fstash);
      linestart = (pos = epos);
    }

    // last line (if any)
    if (pos > linestart) {
      assert(pos == dtext.length);
      dg(Line(linestart, dtext[linestart..pos], true)); // wrapped
    }
  }

  static struct CurXY {
    int x, y; // pixels
    int line; // line number
  }

  CurXY calcCurPixXY (float x=0, float y=0) nothrow @trusted {
    CurXY res;

    setFont();

    immutable float lineh = fstash.fontHeight;

    bool cursorDrawn = false;

    void drawCursorAt (float cx, float cy) {
      if (cursorDrawn) return;
      cursorDrawn = true;
      res.x = cast(int)cx;
      res.y = cast(int)cy;
      res.line = cast(int)(cy/lineh);
    }

    float ex = x;

    bool lastWasHardEOL = true;
    byLine(delegate (scope Line line) {
      if (cursorDrawn) return;
      // check if we should draw cursor
      if (curpos == line.stpos-1) {
        // after hard EOL
        drawCursorAt(x, y);
        return;
      }
      if (curpos >= line.stpos) {
        // if cursor at EOL, and line ends with space, don't draw it
        bool isEndsWithSpace = (line.text.length && dtext[line.stpos+line.text.length-1] <= ' ' && !isNewlineChar(dtext[line.stpos+line.text.length-1]));
        if (curpos <= line.stpos+line.text.length-(isEndsWithSpace ? 1 : 0)) {
          // in line
          immutable float cx = x+fstash.getTextBounds(0, y, line.text[0..curpos-line.stpos], null);
          drawCursorAt(cx, y);
          return;
        }
      }
      // go to next line
      if (x != 0) ex = x+fstash.getTextBounds(0, y, line.text, null);
      y += lineh;
      lastWasHardEOL = !line.wrap;
    });

    // this will draw cursor which is after the last line char
    if (!cursorDrawn) {
      if (!lastWasHardEOL) y -= lineh; else ex = x;
      drawCursorAt(ex, y);
    }

    return res;
  }

private:
  void insertChar (dchar ch) nothrow {
    assert(curpos >= 0);
    if (!allowMultiline && ch == '\n') ch = ' ';
    if (ch < ' ' && (ch != '\n' && ch != '\t')) return;
    // append?
    if (curpos >= dtext.length) {
      dtext ~= ch;
      curpos = cast(int)dtext.length;
    } else {
      // insert
      dtext.length += 1;
      dtext.assumeSafeAppend;
      foreach (immutable idx; curpos..dtext.length-1; reverse) dtext[idx+1] = dtext[idx];
      dtext[curpos++] = ch;
    }
  }

  void putChar (char ch) nothrow {
    dchar dc = ec.decode(cast(ubyte)ch);
    if (dc <= dchar.max) insertChar(dc); // insert if decoded
  }

private:
  void doBackspace () nothrow {
    if (dtext.length == 0 || curpos == 0) return;
    --curpos;
    foreach (immutable idx; curpos+1..dtext.length) dtext[idx-1] = dtext[idx];
    dtext.length -= 1;
    dtext.assumeSafeAppend;
  }

  void doDelete () nothrow {
    if (dtext.length == 0 || curpos >= dtext.length) return;
    foreach (immutable idx; curpos+1..dtext.length) dtext[idx-1] = dtext[idx];
    dtext.length -= 1;
    dtext.assumeSafeAppend;
  }

  void doLeftWord () nothrow {
    if (curpos == 0) return;
    --curpos;
    if (dtext[curpos] <= ' ') {
      while (curpos > 0 && dtext[curpos] <= ' ') --curpos;
      if (dtext[curpos] > ' ') ++curpos;
    } else {
      while (curpos > 0 && dtext[curpos] > ' ') --curpos;
      if (dtext[curpos] <= ' ') ++curpos;
    }
  }

  void doRightWord () nothrow {
    if (curpos == dtext.length) return;
    if (dtext[curpos] <= ' ') {
      while (curpos < dtext.length && dtext[curpos] <= ' ') ++curpos;
    } else {
      while (curpos < dtext.length && dtext[curpos] > ' ') ++curpos;
    }
  }

  void doDeleteWord () nothrow {
    if (curpos == 0) return;
    if (dtext[curpos-1] <= ' ') {
      while (curpos > 0 && dtext[curpos-1] <= ' ') doBackspace();
    } else {
      while (curpos > 0 && dtext[curpos-1] > ' ') doBackspace();
    }
  }

  void doUp () nothrow {
    if (lastWidth < 1) return;
    auto cpos = calcCurPixXY();
    if (cpos.line == 0) { curpos = 0; return; } // nothing more to do
    // as we won't have really long texts here, let's use Shlemiel's algorithm. boo!
    while (curpos > 0) {
      --curpos;
      auto ppos = calcCurPixXY();
      if (ppos.line != cpos.line-1) continue;
      if (ppos.x == cpos.x) return; // i found her!
      if (ppos.x < cpos.x) {
        import std.math : abs;
        ++curpos;
        auto npos = calcCurPixXY();
        if (abs(cpos.x-ppos.x) < abs(cpos.x-npos.x)) --curpos;
        return;
      }
    }
  }

  void doDown () nothrow {
    if (lastWidth < 1) return;
    auto cpos = calcCurPixXY();
    // as we won't have really long texts here, let's use Shlemiel's algorithm. boo!
    while (curpos < dtext.length) {
      ++curpos;
      auto npos = calcCurPixXY();
      if (npos.line != cpos.line+1) continue;
      if (npos.x == cpos.x) return; // i found her!
      if (npos.x > cpos.x) {
        import std.math : abs;
        --curpos;
        auto ppos = calcCurPixXY();
        if (abs(cpos.x-npos.x) > abs(cpos.x-npos.x)) ++curpos;
        return;
      }
    }
  }

public:
  this () nothrow {}

  string text () const nothrow {
    import iv.utfutil : utf8Encode;
    string res;
    res.reserve(dtext.length*4);
    foreach (dchar dc; dtext) {
      char[4] buf = void;
      auto len = utf8Encode(buf[], dc);
      res ~= buf[0..len];
    }
    return res;
  }

  void text (const(char)[] s) nothrow {
    clear();
    addText(s);
  }

  void addText (const(char)[] s) nothrow {
    foreach (char ch; s) putChar(ch);
  }

  void clear () nothrow {
    if (dtext.length) {
      dtext.length = 0;
      dtext.assumeSafeAppend;
    }
    curpos = 0;
    ec.reset();
  }

public:
  @property bool multiline () const nothrow @safe @nogc { pragma(inline, true); return allowMultiline; }
  @property void multiline (bool v) {
    if (allowMultiline == v) return;
    allowMultiline = v;
    //TODO: mark dirty
  }

  void setWidth (int wdt) nothrow @safe @nogc {
    if (wdt < 1) wdt = 1;
    lastWidth = wdt;
  }

  // for the given width
  int calcHeight () nothrow @trusted {
    setFont();

    //if (dtext.length) conwriteln("text: <", text, ">");
    bool lastWasHardEOL = true; // at least one line should be here
    int lineCount = 0;
    byLine(delegate (scope Line line) {
      //if (dtext.length) conwriteln("  line(", line.stpos, "): text: <", line.utftext, ">");
      ++lineCount;
      lastWasHardEOL = !line.wrap;
      //if (lineCount >= 4) assert(0, "oops");
    });
    //if (dtext.length) conwriteln("===");

    if (lastWasHardEOL) ++lineCount;

    return cast(int)(fstash.fontHeight*lineCount);
  }

  void draw (NVGContext nvg, float x, float y) {
    if (lastWidth < 1) return;

    auto cpos = calcCurPixXY(x, y);
    immutable float lineh = fstash.fontHeight;

    // draw text
    setFont(nvg); // this sets `fstash` too
    nvg.fillColor = NVGColor.k8orange; // text color
    byLine(delegate (scope Line line) {
      nvg.text(x, y, line.text);
      y += lineh;
    });

    // draw cursor
    nvg.beginPath();
    nvg.strokeColor = NVGColor.yellow;
    nvg.rect(cpos.x, cpos.y, 1, cast(int)lineh); // ensure that cursor looks a little blurry
    nvg.stroke();

    // reset path
    nvg.beginPath();
  }

  // find and call event handler
  final bool processKeyEvent(ME=typeof(this)) (KeyEvent event) nothrow {
    import std.traits;
    try {
      foreach (string memn; __traits(allMembers, ME)) {
        static if (is(typeof(&__traits(getMember, ME, memn)))) {
          import std.meta : AliasSeq;
          alias mx = AliasSeq!(__traits(getMember, ME, memn))[0];
          static if (isCallable!mx && hasUDA!(mx, MiniEditKB)) {
            //pragma(msg, memn);
            foreach (const MiniEditKB attr; getUDAs!(mx, MiniEditKB)) {
              //pragma(msg, "  ", attr.evt);
              if (event == attr.evt) { mx(); return true; }
              if (!event.pressed) {
                event.pressed = true;
                if (event == attr.evt) return true;
                event.pressed = false;
              }
            }
          }
        }
      }
    } catch (Exception e) {
      conwriteln("processKeyEvent EXCEPTION: ", e.msg);
    }
    return false;
  }

  bool onKey (KeyEvent event) nothrow {
    // enter
    if (allowMultiline && event.key == Key.Enter) {
      if (event.pressed) insertChar('\n');
      return true;
    }

    if (processKeyEvent(event)) { try { glconPostScreenRepaint(); } catch (Exception e) {} return true; }

    return false;
  }

  bool onChar (dchar ch) nothrow {
    // 127 is "delete"
    if (/*ch == '\t' ||*/ (ch >= ' ' && ch != 127)) { insertChar(ch); return true; } // enter is processed in key handler
    return false;
  }

  // keybindings
final public:
  @MiniEditKB("D-S-Insert") void oeFromClip () { glconCtlWindow.getClipboardText(delegate (str) { foreach (immutable char ch; str) putChar(ch); }); }
  @MiniEditKB("D-C-Insert") void oeToClip () { glconCtlWindow.setClipboardText(text); }

  @MiniEditKB("D-Backspace") void oeBackspace () { doBackspace(); }
  @MiniEditKB("D-Delete") void oeDelete () { doDelete(); }
  @MiniEditKB("D-C-A") @MiniEditKB("D-Home") @MiniEditKB("D-Pad7") void oeGoHome () { curpos = 0; }
  @MiniEditKB("D-C-E") @MiniEditKB("D-End") @MiniEditKB("D-Pad1") void oeGoEnd () { curpos = cast(int)dtext.length; }
  @MiniEditKB("D-C-Backspace") @MiniEditKB("D-M-Backspace") void oeDelWord () { doDeleteWord(); }

  @MiniEditKB("D-C-Y") void oeKillAll () { clear(); }

  @MiniEditKB("D-Left") @MiniEditKB("D-Pad4") void oeGoLeft () { if (curpos > 0) --curpos; }
  @MiniEditKB("D-Right") @MiniEditKB("D-Pad6") void oeGoRight () { if (curpos < dtext.length) ++curpos; }

  @MiniEditKB("D-Up") @MiniEditKB("D-Pad8") void oeGoUp () { doUp(); }
  @MiniEditKB("D-Down") @MiniEditKB("D-Pad2") void oeGoDown () { doDown(); }

  @MiniEditKB("D-C-Left") @MiniEditKB("D-C-Pad4") void oeGoLeftWord () { doLeftWord(); }
  @MiniEditKB("D-C-Right") @MiniEditKB("D-C-Pad6") void oeGoRightWord () { doRightWord(); }
}
