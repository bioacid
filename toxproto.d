/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// Tox protocol thread
// Each Tox connection spawns a thread that does all the communication.
// Incoming messages are posted to [glconCtlWindow].
// Asking for outgoing actions are done with public interface.
// No tox internals are exposed to the outer world.
module toxproto is aliced;
private:

import core.time;

import std.concurrency;
import std.datetime;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.strex;
import iv.tox;
import iv.txtser;
import iv.unarray;
import iv.utfutil;
import iv.vfs;
import iv.vfs.util;

import accdb;

//version = ToxCoreDebug;
version = ToxCoreUseBuiltInDataFileParser;

static assert(TOX_PUBLIC_KEY_SIZE == ToxSavedFriend.CRYPTO_PUBLIC_KEY_SIZE);

import iv.vfs;
import accdb : ContactStatus, ContactInfo;


// ////////////////////////////////////////////////////////////////////////// //
public:


// ////////////////////////////////////////////////////////////////////////// //
string buildNormalizedString(bool noteSpacesOnly=false) (const(char)[] s) nothrow {
  auto anchor = s;
  if (s.length == 0) return null;
  s = s.xstrip;
  if (s.length == 0) { static if (noteSpacesOnly) return "<spaces>"; else return null; }
  char[] res;
  res.reserve(s.length);
  foreach (char ch; s) {
    if (ch <= ' ') {
      if (res.length == 0 || res[$-1] > ' ') res ~= ' ';
    } else {
      res ~= ch;
    }
  }
  static if (noteSpacesOnly) { if (res.length == 0) return "<spaces>"; }
  return cast(string)res; // it is safe to cast here
}


// ////////////////////////////////////////////////////////////////////////// //
///
struct ToxCoreDataFile {
  string nick; // user nick
  string statusmsg;
  ubyte[TOX_PUBLIC_KEY_SIZE] pubkey = toxCoreEmptyKey;
  ToxAddr addr = toxCoreEmptyAddr;
  uint nospam;
  ContactStatus status = ContactStatus.Offline;

  ContactInfo[] friends;

  @property bool valid () const pure nothrow @safe @nogc => isValidKey(pubkey);

  static ToxAddr buildAddress (in ref ubyte[TOX_PUBLIC_KEY_SIZE] pubkey, uint nospam) nothrow @trusted @nogc {
    static ushort calcChecksum (const(ubyte)[] data) nothrow @trusted @nogc {
      ubyte[2] checksum = 0;
      foreach (immutable idx, ubyte b; data) checksum[idx%2] ^= b;
      return *cast(ushort*)checksum.ptr;
    }

    ToxAddr res = void;
    res[0..TOX_PUBLIC_KEY_SIZE] = pubkey[];
    *cast(uint*)(res.ptr+TOX_PUBLIC_KEY_SIZE) = nospam;
    *cast(ushort*)(res.ptr+TOX_PUBLIC_KEY_SIZE+uint.sizeof) = calcChecksum(res[0..$-2]);
    return res[];
  }
}


// ////////////////////////////////////////////////////////////////////////// //
///
class ProtocolException : Exception {
  this (string msg, string file=__FILE__, usize line=__LINE__, Throwable next=null) pure nothrow @safe @nogc {
    super(msg, file, line, next);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// messages thread sends to [glconCtlWindow].
// [who] can be the same as [self] to indicate account state changes

class ToxEventBase {
  PubKey self; // account
  PubKey who; // can be same as [self]
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho) { self[] = aself[]; who = awho[]; }
}

// connection state changed
class ToxEventConnection : ToxEventBase {
  bool connected;
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, bool aconnected) { super(aself, awho); connected = aconnected; }
}

// online status changed
class ToxEventStatus : ToxEventBase {
  ContactStatus status;
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, ContactStatus astatus) { super(aself, awho); status = astatus; }
}

// nick changed
class ToxEventNick : ToxEventBase {
  string nick; // new nick
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, string anick) { super(aself, awho); nick = anick; }
}

// status message changed
class ToxEventStatusMsg : ToxEventBase {
  string message; // new message
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, string amessage) { super(aself, awho); message = amessage; }
}

// typing status changed
class ToxEventTyping : ToxEventBase {
  bool typing;
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, bool atyping) { super(aself, awho); typing = atyping; }
}

// new message comes
class ToxEventMessage : ToxEventBase {
  bool action; // is this an "action" message? (/me)
  string message;
  SysTime time;

  this (in ref PubKey aself, in ref PubKey awho, bool aaction, string amessage) {
    super(aself, awho);
    action = aaction;
    message = amessage;
    time = systimeNow;
  }

  this (in ref PubKey aself, in ref PubKey awho, bool aaction, string amessage, SysTime atime) {
    super(aself, awho);
    action = aaction;
    message = amessage;
    time = atime;
  }
}

// send message ack comes
class ToxEventMessageAck : ToxEventBase {
  long msgid;
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, long amsgid) { super(aself, awho); msgid = amsgid; }
}

// friend request comes
class ToxEventFriendReq : ToxEventBase {
  string message; // request message
nothrow @trusted @nogc:
  this (in ref PubKey aself, in ref PubKey awho, string amessage) { super(aself, awho); message = amessage; }
}


// ////////////////////////////////////////////////////////////////////////// //

/// shutdown protocol module
void toxCoreShutdownAll () {
  int acksLeft = 0;
  synchronized(TPInfo.classinfo) {
    foreach (TPInfo ti; allProtos) {
      ti.tid.send(TrdCmdQuit.init);
      ++acksLeft;
    }
  }
  while (acksLeft > 0) {
    receive(
      (TrdCmdQuitAck ack) { --acksLeft; },
      (Variant v) {},
    );
  }

  foreach (TPInfo tpi; allProtos) {
    // save toxcode data, if there is any
    if (tpi.tox !is null && tpi.toxDataDiskName.length) saveToxCoreData(tpi.tox, tpi.toxDataDiskName);
    if (tpi.tox !is null) { tox_kill(tpi.tox); tpi.tox = null; }
  }
}


// ////////////////////////////////////////////////////////////////////////// //

///
static immutable PubKey toxCoreEmptyKey = 0;
static immutable ToxAddr toxCoreEmptyAddr = 0;

/// delegate that will be used to send messages.
/// can be called from any thread, so it should be thread-safe, and should avoid deadlocks.
/// the delegate should be set on program startup, and should not be changed anymore.
/// FIXME: add API to change this!
__gshared void delegate (Object msg) nothrow toxCoreSendEvent;

///
bool isValidKey (in ref PubKey key) pure nothrow @safe @nogc => (key[] != toxCoreEmptyKey[]);

///
bool isValidAddr (in ref ToxAddr key) pure nothrow @safe @nogc => (key[] != toxCoreEmptyAddr[]);


/// parse toxcore data file.
/// sorry, i had to do it manually, 'cause there is no way to open toxcore data without going online.
public ToxCoreDataFile toxCoreLoadDataFile (VFile fl) nothrow {
  ToxCoreDataFile res;
  try {
    version(ToxCoreUseBuiltInDataFileParser) {
      auto sz = fl.size-fl.tell;
      if (sz < 8) throw new ProtocolException("data file too small");
      if (fl.readNum!uint != 0) throw new ProtocolException("invalid something");
      if (fl.readNum!uint != ToxCoreDataId) throw new ProtocolException("not a ToxCore data file");
      while (sz >= 8) {
        auto len = fl.readNum!uint;
        auto id = fl.readNum!uint;
        if (id>>16 != ToxCoreChunkTypeHi) throw new ProtocolException("invalid chunk hitype");
        id &= 0xffffU;
        switch (id) {
          case ToxCoreChunkNoSpamKeys:
            if (len == ToxSavedFriend.CRYPTO_PUBLIC_KEY_SIZE+ToxSavedFriend.CRYPTO_SECRET_KEY_SIZE+uint.sizeof) {
              res.nospam = fl.readNum!uint;
              fl.rawReadExact(res.pubkey[]);
              ubyte[ToxSavedFriend.CRYPTO_PUBLIC_KEY_SIZE] privkey;
              fl.rawReadExact(privkey[]);
              privkey[] = 0;
              len = 0;
              res.addr[] = res.buildAddress(res.pubkey, res.nospam);
            }
            break;
          case ToxCoreChunkDHT: break;
          case ToxCoreChunkFriends:
            if (len%ToxSavedFriend.TOTAL_DATA_SIZE != 0) throw new ProtocolException("invalid contact list");
            while (len > 0) {
              ToxSavedFriend fr;
              auto st = fl.tell;
              fr.load(fl);
              st = fl.tell-st;
              if (st > len || st != ToxSavedFriend.TOTAL_DATA_SIZE) throw new ProtocolException("invalid contact list");
              len -= cast(uint)st;
              if (fr.status == ToxSavedFriend.Status.NoFriend) continue;
              ContactInfo ci;
              ci.nick = fr.name[0..fr.name_length].buildNormalizedString!true;
              ci.lastonlinetime = cast(uint)fr.last_seen_time;
              final switch (fr.status) {
                case ToxSavedFriend.Status.NoFriend: assert(0, "wtf?!");
                // or vice versa?
                case ToxSavedFriend.Status.Added: ci.kind = ContactInfo.Kind.PengingAuthAccept; break;
                case ToxSavedFriend.Status.Requested: ci.kind = ContactInfo.Kind.PengingAuthRequest; break;
                case ToxSavedFriend.Status.Confirmed:
                case ToxSavedFriend.Status.Online:
                  ci.kind = ContactInfo.Kind.Friend;
                  break;
              }
              if (ci.kind != ContactInfo.Kind.Friend) {
                ci.statusmsg = fr.info[0..fr.info_size].buildNormalizedString!true;
              } else {
                ci.statusmsg = fr.statusmessage[0..fr.statusmessage_length].buildNormalizedString;
              }
              ci.nospam = fr.friendrequest_nospam;
              ci.pubkey[] = fr.real_pk[];
              res.friends ~= ci;
            }
            break;
          case ToxCoreChunkName:
            if (len) {
              auto name = new char[](len);
              fl.rawReadExact(name);
              len = 0;
              res.nick = cast(string)name; // it is safe to cast here
            }
            break;
          case ToxCoreChunkStatusMsg:
            if (len) {
              auto msg = new char[](len);
              fl.rawReadExact(msg);
              len = 0;
              res.statusmsg = cast(string)msg; // it is safe to cast here
            }
            break;
          case ToxCoreChunkStatus:
            if (len == 1) {
              auto st = fl.readNum!ubyte;
              if (st < ToxSavedFriend.UserStatus.Invalid) {
                switch (st) {
                  case ToxSavedFriend.UserStatus.None: res.status = ContactStatus.Online; break;
                  case ToxSavedFriend.UserStatus.Away: res.status = ContactStatus.Away; break;
                  case ToxSavedFriend.UserStatus.Busy: res.status = ContactStatus.Busy; break;
                  default: res.status = ContactStatus.Offline; break;
                }
              }
              len = 0;
            }
            break;
          case ToxCoreChunkTcpRelay: break;
          case ToxCoreChunkPathNode: break;
          default: break;
        }
        if (id == ToxCoreChunkEnd) break;
        fl.seek(len, Seek.Cur);
      }
    } else {
      // nope, not found, try to open account
      ProtoOptions protoOpts;
      auto tox = toxCreateInstance(toxdatafname, protoOpts, allowNew:false);
      if (tox is null) throw new ProtocolException("cannot load toxcore data file");
      scope(exit) tox_kill(tox); // and immediately kill it, or it will go online. fuck.

      tox_self_get_public_key(tox, res.pubkey.ptr);
      tox_self_get_address(tox, res.addr.ptr);
      res.nospam = tox_self_get_nospam(tox);

      // get user name
      auto nsz = tox_self_get_name_size(tox);
      if (nsz != 0) {
        if (nsz > tox_max_name_length()) nsz = tox_max_name_length(); // just in case
        auto xbuf = new char[](tox_max_name_length());
        xbuf[] = 0;
        tox_self_get_name(tox, xbuf.ptr);
        res.nick = cast(string)(xbuf[0..nsz]); // ah, cast it directly here
      }

      auto msz = tox_self_get_status_message_size(tox);
      if (msz != 0) {
        if (msz > tox_max_status_message_length()) msz = tox_max_status_message_length(); // just in case
        auto xbuf = new char[](tox_max_status_message_length());
        xbuf[] = 0;
        tox_self_get_status_message(tox, xbuf.ptr);
        res.statusmsg = cast(string)(xbuf[0..nsz]); // ah, cast it directly here
      }

      // TODO: online status, friend list
      assert(0, "not finished");
    }
  } catch (Exception e) {
    res = res.init;
  }
  return res;
}


/// returns public key for account with the given data file, or [toxCoreEmptyKey].
PubKey toxCoreGetAccountPubKey (const(char)[] toxdatafname) nothrow {
  version(ToxCoreUseBuiltInDataFileParser) {
    try {
      auto data = toxCoreLoadDataFile(VFile(toxdatafname));
      if (isValidKey(data.pubkey)) return data.pubkey[];
    } catch (Exception e) {}
  } else {
    TPInfo tacc = null;
    synchronized(TPInfo.classinfo) {
      foreach (TPInfo ti; allProtos) if (ti.toxDataDiskName == toxdatafname) { tacc = ti; break; }
      // not found?
      if (tacc is null) {
        // nope, not found, try to open account
        ProtoOptions protoOpts;
        auto tox = toxCreateInstance(toxdatafname, protoOpts, allowNew:false);
        if (tox is null) return toxCoreEmptyKey;
        PubKey self; tox_self_get_public_key(tox, self.ptr);
        // and immediately kill it, or it will go online. fuck.
        tox_kill(tox);
        return self[];
      }
    }
    if (tacc !is null) {
      synchronized(tacc) {
        PubKey self; tox_self_get_public_key(tacc.tox, self.ptr);
        return self[];
      }
    }
  }
  return toxCoreEmptyKey;
}


/// returns address public key for account with the given data file, or [toxCoreEmptyKey].
ToxAddr toxCoreGetAccountAddress (const(char)[] toxdatafname) nothrow {
  version(ToxCoreUseBuiltInDataFileParser) {
    try {
      auto data = toxCoreLoadDataFile(VFile(toxdatafname));
      if (isValidKey(data.pubkey)) return data.addr[];
    } catch (Exception e) {}
  } else {
    TPInfo tacc = null;
    synchronized(TPInfo.classinfo) {
      foreach (TPInfo ti; allProtos) if (ti.toxDataDiskName == toxdatafname) { tacc = ti; break; }
      // not found?
      if (tacc is null) {
        // nope, not found, try to open account
        ProtoOptions protoOpts;
        auto tox = toxCreateInstance(toxdatafname, protoOpts, allowNew:false);
        if (tox is null) return toxCoreEmptyAddr;
        ToxAddr addr; tox_self_get_address(tox, addr.ptr);
        // and immediately kill it, or it will go online. fuck.
        tox_kill(tox);
        return addr[];
      }
    }
    if (tacc !is null) {
      synchronized(tacc) {
        ToxAddr res;
        if (tacc.tox is null) {
          res[] = tacc.addr[];
        } else {
          tox_self_get_address(ti.tox, res.ptr);
        }
        return res[];
      }
    }
  }
  return toxCoreEmptyAddr;
}


/// returns address that can be given to other people, so they can friend you.
/// returns zero-filled array on invalid request.
ToxAddr toxCoreGetSelfAddress (in ref PubKey self) nothrow {
  ToxAddr res = 0;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) {
      res[] = ti.addr[];
    } else {
      tox_self_get_address(ti.tox, res.ptr);
    }
  });
  return res[];
}


/// checks if we have a working thread for `self`.
bool toxCoreIsAccountOpen (in ref PubKey self) nothrow {
  synchronized(TPInfo.classinfo) {
    foreach (TPInfo ti; allProtos) {
      if (ti.self[] == self[]) return true;
    }
  }
  return false;
}


/// returns nick for the given account, or `null`.
string toxCoreGetNick (in ref PubKey self) nothrow {
  synchronized(TPInfo.classinfo) {
    foreach (TPInfo ti; allProtos) {
      if (ti.self[] == self[]) return ti.nick;
    }
  }
  return null;
}


/// creates new Tox account (or opens old), stores it in data file, returns public key for new account.
/// returns [toxCoreEmptyKey] on error.
/// TODO: pass protocol options here
/// WARNING! don't call this from more than one thread with the same `toxdatafname`
PubKey toxCoreCreateAccount (const(char)[] toxdatafname, const(char)[] nick) nothrow {
  if (toxdatafname.length == 0) return toxCoreEmptyKey;

  if (nick.length == 0 || nick.length > tox_max_name_length()) return toxCoreEmptyKey;

  try {
    import std.path : absolutePath;

    string toxDataDiskName = toxdatafname.idup.absolutePath;

    synchronized(TPInfo.classinfo) {
      foreach (TPInfo ti; allProtos) {
        if (ti.toxDataDiskName == toxDataDiskName) return ti.self[];
      }
    }

    ProtoOptions protoOpts;
    protoOpts.udp = true;

    auto tox = toxCreateInstance(toxdatafname, protoOpts, allowNew:true);
    if (tox is null) return toxCoreEmptyKey;

    auto ti = new TPInfo();
    ti.toxDataDiskName = toxDataDiskName;
    tox_self_get_public_key(tox, ti.self.ptr);
    tox_self_get_address(tox, ti.addr.ptr);
    ti.nick = nick.idup;

    // set user name
    int error = 0;
    tox_self_set_name(tox, nick.ptr, nick.length, &error);
    saveToxCoreData(tox, toxDataDiskName);

    // and immediately kill it, or it will go online. fuck.
    tox_kill(tox);

    synchronized(TPInfo.classinfo) {
      allProtos ~= ti;
      startThread(ti);
    }
    return ti.self[];
  } catch (Exception e) {
  }

  return toxCoreEmptyKey;
}


/// starts working thread for account with the given data file.
/// returns account public key, or [toxCoreEmptyKey] on error.
/// TODO: pass protocol options here
/// WARNING! don't call this from more than one thread with the same `toxdatafname`
PubKey toxCoreOpenAccount (const(char)[] toxdatafname) nothrow {
  if (toxdatafname.length == 0) return toxCoreEmptyKey;

  try {
    import std.path : absolutePath, dirName;

    string toxDataDiskName = toxdatafname.idup.absolutePath;

    synchronized(TPInfo.classinfo) {
      foreach (TPInfo ti; allProtos) {
        if (ti.toxDataDiskName == toxDataDiskName) return ti.self[];
      }
    }

    version(ToxCoreUseBuiltInDataFileParser) {
      // manual parsing
      auto data = toxCoreLoadDataFile(VFile(toxDataDiskName));
      if (!isValidKey(data.pubkey)) return toxCoreEmptyKey;

      auto ti = new TPInfo();
      ti.toxDataDiskName = toxDataDiskName;
      ti.self[] = data.pubkey[];
      ti.addr[] = data.addr[];
      ti.nick = data.nick;
    } else {
      // use toxcore to parse data file
      ProtoOptions protoOpts;
      try {
        protoOpts.txtunser(VFile(toxDataDiskName.dirName~"/proto.rc"));
      } catch (Exception e) {
        protoOpts = protoOpts.default;
        protoOpts.udp = true;
      }

      auto tox = toxCreateInstance(toxdatafname, protoOpts, allowNew:false);
      if (tox is null) return toxCoreEmptyKey;
      PubKey self; tox_self_get_public_key(tox, self.ptr);

      auto ti = new TPInfo();
      ti.toxDataDiskName = toxDataDiskName;
      tox_self_get_public_key(tox, ti.self.ptr);
      tox_self_get_address(tox, ti.addr.ptr);

      // get user name
      auto nsz = tox_self_get_name_size(tox);
      if (nsz != 0) {
        if (nsz > tox_max_name_length()) nsz = tox_max_name_length(); // just in case
        // k8: toxcore developers are idiots, so we have to do dynalloc here
        auto xbuf = new char[](tox_max_name_length());
        xbuf[] = 0;
        tox_self_get_name(tox, xbuf.ptr);
        ti.nick = cast(string)(xbuf[0..nsz]); // ah, cast it directly here
      }

      // and immediately kill it, or it will go online. fuck.
      tox_kill(tox);
    }

    synchronized(TPInfo.classinfo) {
      allProtos ~= ti;
      startThread(ti);
    }

    return ti.self[];
  } catch (Exception e) {
  }

  return toxCoreEmptyKey;
}


/// stops working thread for account with the given data file.
/// returns success flag.
bool toxCoreCloseAccount (in ref PubKey self) nothrow {
  try {
    TPInfo tpi = null;
    synchronized(TPInfo.classinfo) {
      foreach (TPInfo ti; allProtos) if (ti.self[] == self[]) { tpi = ti; break; }
    }
    if (tpi is null) return false;

    // send "quit" signal
    tpi.tid.send(TrdCmdQuit.init);
    for (;;) {
      bool done = false;
      receive(
        (TrdCmdQuitAck ack) { done = true; },
        (Variant v) {},
      );
      if (done) break;
    }

    // save toxcode data, if there is any
    clearToxCallbacks(tpi.tox);
    if (tpi.tox !is null && tpi.toxDataDiskName.length) saveToxCoreData(tpi.tox, tpi.toxDataDiskName);
    if (tpi.tox !is null) { tox_kill(tpi.tox); tpi.tox = null; }

    // remove from the list
    synchronized(TPInfo.classinfo) {
      foreach (immutable idx, TPInfo ti; allProtos) {
        if (ti.self[] == self[]) {
          foreach (immutable c; idx+1..allProtos.length) allProtos[c-1] = allProtos[c];
          delete allProtos[$-1];
          allProtos.length -= 1;
          allProtos.assumeSafeAppend;
          break;
        }
      }
    }

    return true;
  } catch (Exception e) {
  }
  return false;
}


enum MsgIdError = -1;
enum MsgIdOffline = 0;

/// sends message. returns message id that can be used to track acks.
/// returns [MsgIdError] for unknown account, or unknown `dest`.
/// returns [MsgIdOffline] if `self` or `dest` is offline (message is not queued to send in this case).
/// note that `0` is a not a valid message id.
/// cannot send empty messages, and messages longer than `TOX_MAX_MESSAGE_LENGTH` chars.
long toxCoreSendMessage (in ref PubKey self, in ref PubKey dest, const(char)[] msg, bool action=false) nothrow {
  long res = MsgIdError;
  if (msg.length == 0) return res; // cannot send empty messages
  TOX_MESSAGE_TYPE tt = (action ? TOX_MESSAGE_TYPE_ACTION : TOX_MESSAGE_TYPE_NORMAL);
  /*
  if (msg.startsWith("/me ")) {
    msg = msg[4..$].xstripleft;
    if (msg.length == 0) return res; // cannot send empty messages
    tt = TOX_MESSAGE_TYPE_ACTION;
  }
  */
  if (msg.length > tox_max_message_length()) return res; // message too long
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) { res = MsgIdOffline; return; }
    //if (tox_self_get_connection_status(ti.tox) == TOX_CONNECTION_NONE) { res = MsgIdOffline; return; }
    // find friend
    TOX_ERR_FRIEND_BY_PUBLIC_KEY errfpk = 0;
    uint frnum = tox_friend_by_public_key(ti.tox, dest.ptr, &errfpk);
    if (errfpk) return; // error
    // offline?
    //if (tox_friend_get_connection_status(ti.tox, frnum) == TOX_CONNECTION_NONE) { res = MsgIdOffline; return; }
    // nope, online; queue the message
    TOX_ERR_FRIEND_SEND_MESSAGE err = 0;
    uint msgid = tox_friend_send_message(ti.tox, frnum, tt, msg.ptr, msg.length, &err);
    switch (err) {
      case TOX_ERR_FRIEND_SEND_MESSAGE_OK: res = (cast(long)msgid)+1; break;
      case TOX_ERR_FRIEND_SEND_MESSAGE_NULL: res = MsgIdError; break;
      case TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_FOUND: res = MsgIdError; break;
      case TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_CONNECTED: res = MsgIdOffline; break;
      case TOX_ERR_FRIEND_SEND_MESSAGE_SENDQ: res = MsgIdError; break;
      case TOX_ERR_FRIEND_SEND_MESSAGE_TOO_LONG: res = MsgIdError; break;
      case TOX_ERR_FRIEND_SEND_MESSAGE_EMPTY: res = MsgIdError; break;
      default: res = MsgIdError; break;
    }
    if (res > 0) ti.ping();
  });
  return res;
}


/// sets new status.
/// returns `false` if `self` is invalid, or some error occured.
/// won't send "going offline" events (caller should know this already).
bool toxCoreSetStatus (in ref PubKey self, ContactStatus status) nothrow {
  if (status == ContactStatus.Connecting) return false; // oops
  bool res = false;
  bool waitKillAck = false;
  bool waitCreateAck = false;

  doWithLockedTPByKey(self, delegate (ti) {
    // if we're going offline, kill toxcore instance
    if (status == ContactStatus.Offline) {
      try {
        ti.tid.send(cast(shared)TrdCmdKillToxCore(thisTid));
        waitKillAck = true;
        res = true;
      } catch (Exception e) {
        res = false;
      }
      return;
    }

    ToxP tox;

    // want to go online
    if (ti.tox is null) {
      // need to create toxcore object
      try {
        import std.path : absolutePath, dirName;

        ProtoOptions protoOpts;
        try {
          protoOpts.txtunser(VFile(ti.toxDataDiskName.dirName~"/proto.rc"));
        } catch (Exception e) {
          protoOpts = protoOpts.default;
          protoOpts.udp = true;
        }

        conwriteln("creating ToxCore...");
        tox = toxCreateInstance(ti.toxDataDiskName, protoOpts, allowNew:false);
        if (tox is null) {
          conwriteln("can't create ToxCore...");
          return;
        }
        ti.tid.send(cast(shared)TrdCmdSetToxCore(tox, thisTid));
        waitCreateAck = true;
      } catch (Exception e) {
        return;
      }
    } else {
      tox = ti.tox;
    }
    assert(tox !is null);

    TOX_USER_STATUS st;
    final switch (status) {
      case ContactStatus.Offline: assert(0, "wtf?!");
      case ContactStatus.Online: st = TOX_USER_STATUS_NONE; break;
      case ContactStatus.Away: st = TOX_USER_STATUS_AWAY; break;
      case ContactStatus.Busy: st = TOX_USER_STATUS_BUSY; break;
      case ContactStatus.Connecting: assert(0, "wtf?!");
    }
    tox_self_set_status(tox, st);
    ti.needSave = true;
    res = true;
    ti.ping();
  });

  if (waitKillAck) {
    try {
      receive((TrdCmdKillToxCoreAck cmd) {});
      conwriteln("got killer ack");
    } catch (Exception e) {
      res = false;
    }
  }

  if (waitCreateAck) {
    try {
      receive((TrdCmdSetToxCoreAck cmd) {});
      conwriteln("got actioneer ack");
    } catch (Exception e) {
      res = false;
    }
  }

  return res;
}


/// sets new status message.
/// returns `false` if `self` is invalid, message is too long, or on any other error.
bool toxCoreSetStatusMessage (in ref PubKey self, const(char)[] message) nothrow {
  if (message.length > tox_max_status_message_length()) return false;
  bool res = false;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return;
    TOX_ERR_SET_INFO err = 0;
    res = tox_self_set_status_message(ti.tox, message.ptr, message.length, &err);
    if (res) ti.needSave = true;
    if (res) ti.ping();
  });
  return res;
}


/// sends friend request.
/// returns `false` if `self` is invalid, message is too long, or on any other error.
bool toxCoreSendFriendRequest (in ref PubKey self, in ref ToxAddr dest, const(char)[] message) nothrow {
  if (message.length > tox_max_friend_request_length()) return false; // cannot send long requests
  bool res = false;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    //if (tox_self_get_connection_status(ti.tox) == TOX_CONNECTION_NONE) return; // this account is offline
    // find friend
    TOX_ERR_FRIEND_BY_PUBLIC_KEY errfpk = 0;
    uint frnum = tox_friend_by_public_key(ti.tox, dest.ptr, &errfpk);
    if (!errfpk) {
      // we already befriend it, do nothing
      res = true;
      return;
    }
    TOX_ERR_FRIEND_ADD errfa = 0;
    frnum = tox_friend_add(ti.tox, dest.ptr, message.ptr, message.length, &errfa);
    res = !errfa;
    if (res) ti.needSave = true;
    if (res) ti.ping();
  });
  return res;
}


/// unconditionally adds a friend.
/// returns `false` if `self` is invalid, message is too long, friend not found, or on any other error.
bool toxCoreAddFriend (in ref PubKey self, in ref PubKey dest) nothrow {
  bool res = false;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    //if (tox_self_get_connection_status(ti.tox) == TOX_CONNECTION_NONE) return; // this account is offline
    // find friend
    TOX_ERR_FRIEND_BY_PUBLIC_KEY errfpk = 0;
    uint frnum = tox_friend_by_public_key(ti.tox, dest.ptr, &errfpk);
    if (!errfpk) {
      // we already has such friend, do nothing
      res = true;
      return;
    }
    TOX_ERR_FRIEND_ADD errfa = 0;
    frnum = tox_friend_add_norequest(ti.tox, dest.ptr, &errfa);
    res = !errfa;
    if (res) ti.needSave = true;
    if (res) ti.ping();
  });
  return res;
}


/// unconditionally removes a friend.
/// returns `false` if `self` is invalid, message is too long, friend not found, or on any other error.
bool toxCoreRemoveFriend (in ref PubKey self, in ref PubKey dest) nothrow {
  bool res = false;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    //if (tox_self_get_connection_status(ti.tox) == TOX_CONNECTION_NONE) return; // this account is offline
    // find friend
    TOX_ERR_FRIEND_BY_PUBLIC_KEY errfpk = 0;
    uint frnum = tox_friend_by_public_key(ti.tox, dest.ptr, &errfpk);
    if (errfpk) { res = false; return; } // no such friend
    TOX_ERR_FRIEND_DELETE errfd = 0;
    res = tox_friend_delete(ti.tox, frnum, &errfd);
    if (res) ti.needSave = true;
    if (res) ti.ping();
  });
  return res;
}


/// checks if the given accound has a friend with the given pubkey.
/// returns `false` if `self` is invalid or offline, or on any other error, or if there is no such friend.
bool toxCoreHasFriend (in ref PubKey self, in ref PubKey dest) nothrow {
  bool res = false;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    // find friend
    TOX_ERR_FRIEND_BY_PUBLIC_KEY errfpk = 0;
    uint frnum = tox_friend_by_public_key(ti.tox, dest.ptr, &errfpk);
    res = !errfpk;
  });
  return res;
}


/// calls delegate for each known friend.
/// return `true` from delegate to stop.
/// WARNING! all background operations are locked, so don't spend too much time in delegate!
void toxCoreForEachFriend (in ref PubKey self, scope bool delegate (in ref PubKey self, in ref PubKey frpub, scope const(char)[] nick) dg) nothrow {
  if (dg is null) return;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    auto frcount = tox_self_get_friend_list_size(ti.tox);
    if (frcount == 0) return;
    auto list = new uint[](frcount);
    scope(exit) delete list;
    char[] nick;
    scope(exit) delete nick;
    tox_self_get_friend_list(ti.tox, list.ptr);
    PubKey fpk;
    foreach (immutable fidx, immutable fid; list[]) {
      TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk = 0;
      if (!tox_friend_get_public_key(ti.tox, fid, fpk.ptr, &errgpk)) continue;
      TOX_ERR_FRIEND_QUERY errgns = 0;
      auto nsz = tox_friend_get_name_size(ti.tox, fid, &errgns);
      if (errgns) nsz = 0;
      if (nsz > nick.length) nick.length = nsz;
      if (nsz != 0) {
        TOX_ERR_FRIEND_QUERY errfq = 0;
        tox_friend_get_name(ti.tox, fid, nick.ptr, &errfq);
        if (errfq) nick[0] = 0;
      }
      try {
        dg(self, fpk, nick[0..nsz]);
      } catch (Exception e) {
        break;
      }
    }
  });
}


/// returns the time when this friend was seen online.
/// returns `SysTime.min` if `self` is invalid, `dest` is invalid, or on any other error.
SysTime toxCoreLastSeen (in ref PubKey self, in ref PubKey dest) nothrow {
  SysTime res = SysTime.min;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    // find friend
    TOX_ERR_FRIEND_BY_PUBLIC_KEY errfpk = 0;
    uint frnum = tox_friend_by_public_key(ti.tox, dest.ptr, &errfpk);
    if (errfpk) return; // unknown friend
    TOX_ERR_FRIEND_GET_LAST_ONLINE err;
    auto ut = tox_friend_get_last_online(ti.tox, frnum, &err);
    try {
      if (err == 0) res = SysTime.fromUnixTime(ut);
    } catch (Exception e) {
      res = SysTime.min;
    }
  });
  return res;
}


/// calls delegate with ToxP. ugly name is intentional.
void toxCoreCallWithToxP (in ref PubKey self, scope void delegate (ToxP tox) dg) nothrow {
  if (dg is null) return;
  doWithLockedTPByKey(self, delegate (ti) {
    if (ti.tox is null) return; // this account is offline
    try {
      dg(ti.tox);
    } catch (Exception e) {}
  });
}


// ////////////////////////////////////////////////////////////////////////// //
// private ToxCore protocol implementation details
private:

class TPInfo {
  static struct FileRejectEvent {
    uint frnum;
    uint flnum;
  }

  //ToxProtocol pr;
  string toxDataDiskName;
  ToxP tox;
  Tid tid;
  PubKey self;
  ToxAddr addr; // will be used if `tox` is `null`
  string nick;
  FileRejectEvent[] rej;
  bool doBootstrap; // do bootstrapping
  bool needSave;

  void ping () nothrow {
    if (tox is null) return;
    try { tid.send(TrdCmdPing.init); } catch (Exception e) {}
  }
}

__gshared TPInfo[] allProtos;


// ////////////////////////////////////////////////////////////////////////// //
struct TrdCmdQuit {} // quit thread loop
struct TrdCmdQuitAck {} // quit thread loop
struct TrdCmdPing {} // do something
struct TrdCmdSetToxCore { ToxP tox; Tid replytid; }
struct TrdCmdSetToxCoreAck {}
struct TrdCmdKillToxCore { Tid replytid; }
struct TrdCmdKillToxCoreAck {}


// ////////////////////////////////////////////////////////////////////////// //
// this should be called with registered `ti`
void startThread (TPInfo ti) {
  assert(ti !is null);
  ti.tid = spawn(&toxCoreThread, thisTid, *cast(immutable(void)**)&ti);
}


// ////////////////////////////////////////////////////////////////////////// //
void clearToxCallbacks (ToxP tox) {
  if (tox is null) return;

  tox_callback_self_connection_status(tox, null);

  tox_callback_friend_name(tox, null);
  tox_callback_friend_status_message(tox, null);
  tox_callback_friend_status(tox, null);
  tox_callback_friend_connection_status(tox, null);
  tox_callback_friend_typing(tox, null);
  tox_callback_friend_read_receipt(tox, null);
  tox_callback_friend_request(tox, null);
  tox_callback_friend_message(tox, null);

  tox_callback_file_recv_control(tox, null);
  tox_callback_file_chunk_request(tox, null);
  tox_callback_file_recv(tox, null);
}


// ////////////////////////////////////////////////////////////////////////// //
static void toxCoreThread (Tid ownerTid, immutable(void)* tiptr) {
  uint mswait = 0;
  MonoTime lastSaveTime = MonoTime.zero;
  TPInfo ti = *cast(TPInfo*)&tiptr; // HACK!
  ToxP newTox = null;
  Tid ackreptid;
  try {
    for (;;) {
      bool doQuit = false;
      bool doKillTox = false;

      if (ti.tox !is null && ti.doBootstrap) {
        conwriteln("TOX(", ti.nick, "): bootstrapping");
        ti.doBootstrap = false;
        bootstrap(ti);
        mswait = tox_iteration_interval(ti.tox);
        if (mswait < 1) mswait = 1;
        conwriteln("TOX(", ti.nick, "): bootstrapping complete (mswait=", mswait, ")");
      }

      receiveTimeout((mswait ? mswait.msecs : 10.hours),
        (TrdCmdQuit cmd) { doQuit = true; },
        (TrdCmdPing cmd) {},
        (shared TrdCmdSetToxCore cmd) { newTox = cast(ToxP)cmd.tox; ackreptid = cast(Tid)cmd.replytid; },
        (shared TrdCmdKillToxCore cmd) { doKillTox = true; ackreptid = cast(Tid)cmd.replytid; },
        (Variant v) { conwriteln("WUTAFUCK?! "); },
      );
      if (doQuit) break;

      if (newTox !is null) {
        conwriteln("TOX(", ti.nick, "): got new toxcore pointer");
        if (ti.tox !is newTox) {
          clearToxCallbacks(ti.tox);
          if (ti.tox !is null) tox_kill(ti.tox);
          ti.tox = newTox;
        }
        newTox = null;
        ti.doBootstrap = true;
        mswait = 1;
        ackreptid.send(TrdCmdSetToxCoreAck.init);
        ackreptid = Tid.init;
        continue;
      }

      if (ti.tox is null) { mswait = 0; continue; }

      if (doKillTox) {
        conwriteln("TOX(", ti.nick, "): killing toxcore pointer");
        doKillTox = false;
        if (ti.tox !is null) {
          clearToxCallbacks(ti.tox);
          if (ti.toxDataDiskName.length) saveToxCoreData(ti.tox, ti.toxDataDiskName);
          tox_kill(ti.tox);
          ti.tox = null;
        } else {
          conwriteln("TOX(", ti.nick, "): wtf?!");
        }
        mswait = 0;
        ackreptid.send(TrdCmdKillToxCoreAck.init);
        ackreptid = Tid.init;
        continue;
      }

      tox_iterate(ti.tox, null);
      mswait = tox_iteration_interval(ti.tox);
      //conwriteln("TOX(", ti.nick, "): interval is ", mswait);
      if (mswait < 1) mswait = 1;
      synchronized(ti) {
        if (ti.needSave) {
          auto ctt = MonoTime.currTime;
          if ((ctt-lastSaveTime).total!"minutes" > 1) {
            ti.needSave = false;
            lastSaveTime = ctt;
            saveToxCoreData(ti.tox, ti.toxDataDiskName);
          }
        }
      }
    }
    ownerTid.send(TrdCmdQuitAck.init);
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    import core.memory : GC;
    import core.thread : thread_suspendAll;
    GC.disable(); // yeah
    thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    abort(); // die, you bitch!
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// find TPInfo object for the given tox handle; used in callbacks
TPInfo findTP (ToxP tox) nothrow {
  if (tox is null) return null;
  synchronized(TPInfo.classinfo) {
    foreach (TPInfo ti; allProtos) {
      if (ti.tox is tox) return ti;
    }
  }
  return null;
}


// returns `true` if found and executed without errors
bool doWithLockedTPByKey (in ref PubKey self, scope void delegate (TPInfo ti) dg) nothrow {
  TPInfo tpi = null;
  if (dg is null) return false;
  synchronized(TPInfo.classinfo) {
    foreach (TPInfo ti; allProtos) if (ti.self[] == self[]) { tpi = ti; break; }
  }
  if (tpi !is null) {
    synchronized(tpi) {
      try {
        dg(tpi);
        return true;
      } catch (Exception e) {
        try {
          conprintfln("\nTOX CB EXCEPTION: %s\n\n", e.toString);
        } catch (Exception e) {}
        return false;
      }
    }
  }
  return false;
}


// returns `true` if found and executed without errors
bool doWithLockedTP (ToxP tox, scope void delegate (TPInfo ti) dg) nothrow {
  TPInfo tpi = null;
  if (tox is null || dg is null) return false;
  synchronized(TPInfo.classinfo) {
    foreach (TPInfo ti; allProtos) if (ti.tox is tox) { tpi = ti; break; }
  }
  if (tpi !is null) {
    synchronized(tpi) {
      try {
        dg(tpi);
        return true;
      } catch (Exception e) {
        try {
          conprintfln("\nTOX CB EXCEPTION: %s\n\n", e.toString);
        } catch (Exception e) {}
        return false;
      }
    }
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
// toxcore callbacks

// self connection state was changed
static extern(C) void connectionCB (Tox* tox, TOX_CONNECTION status, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    conwriteln("TOX(", ti.nick, "): ", (status != TOX_CONNECTION_NONE ? "" : "dis"), "connected.");

    tcmsg = new ToxEventConnection(self, self, status != TOX_CONNECTION_NONE);
  })) return;
  toxCoreSendEvent(tcmsg);
}


// friend connection state was changed
static extern(C) void friendConnectionCB (Tox* tox, uint frnum, TOX_CONNECTION status, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!
    conwriteln("TOX(", ti.nick, "): friend #", frnum, " ", (status != TOX_CONNECTION_NONE ? "" : "dis"), "connected.");

    tcmsg = new ToxEventConnection(self, who, status != TOX_CONNECTION_NONE);
    if (status != TOX_CONNECTION_NONE) ti.needSave = true;
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void friendStatusCB (Tox* tox, uint frnum, TOX_USER_STATUS status, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!
    conwriteln("TOX(", ti.nick, "): friend #", frnum, " changed status to ", status);

    ContactStatus cst = ContactStatus.Offline;
    switch (status) {
      case TOX_USER_STATUS_NONE: cst = ContactStatus.Online; break;
      case TOX_USER_STATUS_AWAY: cst = ContactStatus.Away; break;
      case TOX_USER_STATUS_BUSY: cst = ContactStatus.Busy; break;
      default: return;
    }
    tcmsg = new ToxEventStatus(self, who, cst);
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void friendNameCB (Tox* tox, uint frnum, const(char)* name, usize length, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!
    conwriteln("TOX(", ti.nick, "): friend #", frnum, " changed name to <", name[0..length], ">");

    tcmsg = new ToxEventNick(self, who, name[0..length].buildNormalizedString!true);
    ti.needSave = true;
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void friendStatusMessageCB (Tox* tox, uint frnum, const(char)* msg, usize msglen, void* user_data) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!
    conwriteln("TOX(", ti.nick, "): friend #", frnum, " changed status to <", msg[0..msglen], ">");

    tcmsg = new ToxEventStatusMsg(self, who, msg[0..msglen].buildNormalizedString);
    ti.needSave = true;
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void friendReqCB (Tox* tox, const(ubyte)* pk, const(char)* msg, usize msglen, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; who[] = pk[0..PubKey.length];
    conwriteln("TOX(", ti.nick, "): friend request comes: <", msg[0..msglen], ">");

    tcmsg = new ToxEventFriendReq(self, who, msg[0..msglen].idup);
    ti.needSave = true;
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void friendMsgCB (Tox* tox, uint frnum, TOX_MESSAGE_TYPE type, const(char)* msg, usize msglen, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!
    conwriteln("TOX(", ti.nick, "): friend #", frnum, " sent a message.");

    tcmsg = new ToxEventMessage(self, who, (type == TOX_MESSAGE_TYPE_ACTION), msg[0..msglen].idup);
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void friendReceiptCB (Tox* tox, uint frnum, uint msgid, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  ToxEventBase tcmsg;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!
    conwriteln("TOX(", ti.nick, "): friend #", frnum, " acked message #", msgid);

    tcmsg = new ToxEventMessageAck(self, who, (cast(long)msgid)+1);
  })) return;
  toxCoreSendEvent(tcmsg);
}


static extern(C) void fileRecvCB (Tox* tox, uint frnum, uint flnum, TOX_FILE_KIND kind, ulong flsize, const(char)* name, usize namelen, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    //TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    //PubKey self; tox_self_get_public_key(tox, self.ptr);
    //PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return false; // wtf?!

    bool found = false;
    foreach (ref TPInfo.FileRejectEvent fre; ti.rej) if (fre.frnum == frnum && fre.flnum == flnum) { found = true; break; }
    if (!found) ti.rej ~= TPInfo.FileRejectEvent(frnum, flnum);
  })) return;

  /*
  if (kind == TOX_FILE_KIND_AVATAR) {
    if (flsize < 16 || flsize > 1024*1024) {
      timp.addRejectEvent(frnum, flnum);
      return;
    }
  }
  timp.addRejectEvent(frnum, flnum);

  tox_file_control(tox, rej.frnum, rej.flnum, TOX_FILE_CONTROL_CANCEL, null);
  */
}


static extern(C) void fileRecvCtlCB (Tox* tox, uint frnum, uint flnum, TOX_FILE_CONTROL ctl, void* udata) nothrow {
  /+
  auto timp = findTP(tox);
  if (timp is null) return;
  TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
  PubKey who;
  if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return false; // wtf?!
  try {
    conprintfln("got recv ctl for friend #%s, file #%s, ctl:%s", frnum, flnum, ctl);
    /*
    synchronized(timp) {
      timp.friendMessageAck(who, msgid);
    }
    */
  } catch (Exception e) {
    conprintfln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
  }

  auto timp = *cast(Account*)&udata;
  if (timp is null || timp.tox is null) return;
  try {
    final switch (ctl) {
      case TOX_FILE_CONTROL_RESUME: /*timp.resumeSendByNums(frnum, flnum);*/ break;
      case TOX_FILE_CONTROL_PAUSE: /*timp.pauseSendByNums(frnum, flnum);*/ break;
      case TOX_FILE_CONTROL_CANCEL: /*timp.abortSendByNums(frnum, flnum);*/ break;
    }
  } catch (Exception e) {
    try {
      conprintfln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
      timp.fatalError();
    } catch (Exception) {}
  }
  +/
}


static extern(C) void fileChunkReqCB (Tox* tox, uint frnum, uint flnum, ulong pos, usize len, void* udata) nothrow {
  if (toxCoreSendEvent is null) return;
  if (!tox.doWithLockedTP(delegate (TPInfo ti) {
    TOX_ERR_FRIEND_GET_PUBLIC_KEY errgpk;
    PubKey self; tox_self_get_public_key(tox, self.ptr);
    PubKey who; if (!tox_friend_get_public_key(tox, frnum, who.ptr, &errgpk)) return; // wtf?!

    bool found = false;
    foreach (ref TPInfo.FileRejectEvent fre; ti.rej) if (fre.frnum == frnum && fre.flnum == flnum) { found = true; break; }
    if (!found) ti.rej ~= TPInfo.FileRejectEvent(frnum, flnum);
  })) return;

  //logwritefln("got chunk req recv ctl for friend #%s, file #%s, ctl:%s", frnum, flnum, ctl);
  /*
  try {
    timp.fschunks ~= ChunkToSend(frnum, flnum, pos, len);
  } catch (Exception e) {
    try {
      conprintfln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
      timp.dropNetIOConnection();
    } catch (Exception) {}
  }
  */
}


// ////////////////////////////////////////////////////////////////////////// //
void bootstrap(string mode="any") (TPInfo ti) nothrow if (mode == "udp" || mode == "tcp" || mode == "any") {
  if (ti is null || ti.tox is null) return;

  ToxBootstrapServer[] loadBootNodes () nothrow {
    import std.path;
    auto bfname = buildPath(ti.toxDataDiskName.dirName, "tox_bootstrap.rc");
    ToxBootstrapServer[] bootnodes = null;
    try {
      bootnodes.txtunser(VFile(bfname));
      if (bootnodes.length > 0) return bootnodes;
    } catch (Exception e) {}
    // try to download
    try {
      bootnodes = tox_download_bootstrap_list();
      if (bootnodes.length > 0) serialize(bootnodes, bfname);
      return bootnodes;
    } catch (Exception e) {}
    return null;
  }

  conprintfln("Tox: loading bootstrap nodes...");
  auto nodes = loadBootNodes();
  conprintfln("Tox: %s nodes loaded", nodes.length);
  if (nodes.length == 0) return;
  foreach (const ref ToxBootstrapServer srv; nodes) {
    if (srv.ipv4.length < 2) continue;
    assert(srv.ipv4[$-1] == 0);
    //conprintfln("  node ip: %s:%u (maintainer: %s)", srv.ipv4[0..$-1], srv.port, srv.maintainer);
    static if (mode == "udp") {
      if (srv.udp) {
        tox_bootstrap(ti.tox, srv.ipv4.ptr, srv.port, srv.pubkey.ptr, null);
      }
    } else static if (mode == "tcp") {
      if (srv.tcp) {
        foreach (immutable ushort port; srv.tcpports) {
          tox_add_tcp_relay(ti.tox, srv.ipv4.ptr, port, srv.pubkey.ptr, null);
        }
      }
    } else {
      if (srv.udp) {
        tox_bootstrap(ti.tox, srv.ipv4.ptr, srv.port, srv.pubkey.ptr, null);
      } else if (srv.udp) {
        foreach (immutable ushort port; srv.tcpports) {
          tox_add_tcp_relay(ti.tox, srv.ipv4.ptr, port, srv.pubkey.ptr, null);
        }
      }
    }
    tox_iterate(ti.tox, null);
  }
  //conprintfln("Tox[%s]: %s nodes added", srvalias, nodes.length);
}


// ////////////////////////////////////////////////////////////////////////// //
// returns `null` if there is no such file or file cannot be loaded
ubyte[] loadToxCoreData (const(char)[] toxdatafname) nothrow {
  import core.stdc.stdio : FILE, fopen, fclose, rename, fread, ferror, fseek, ftell, SEEK_SET, SEEK_END;
  import core.stdc.stdlib : malloc, free;
  import core.sys.posix.unistd : unlink;

  if (toxdatafname.length == 0) return null;

  static char* namebuf = null;
  static uint nbsize = 0;

  if (nbsize < toxdatafname.length+1024) {
    import core.stdc.stdlib : realloc;
    nbsize = cast(uint)toxdatafname.length+1024;
    namebuf = cast(char*)realloc(namebuf, nbsize);
    if (namebuf is null) assert(0, "out of memory");
  }

  auto origName = expandTilde(namebuf[0..nbsize-6], toxdatafname);
  if (origName == null) return null; // oops
  origName.ptr[origName.length] = 0; // zero-terminate

  FILE* fi = fopen(namebuf, "r");
  if (fi is null) return null;
  scope(exit) fclose(fi);

  for (;;) {
    if (fseek(fi, 0, SEEK_END) == -1) {
      import core.stdc.errno;
      if (errno == EINTR) continue;
      return null;
    }
    break;
  }

  long fsize;
  for (;;) {
    fsize = ftell(fi);
    if (fsize == -1) {
      import core.stdc.errno;
      if (errno == EINTR) continue;
      return null;
    }
    break;
  }

  if (fsize > 1024*1024*256) { conwriteln("toxcore data file too big"); return null; }
  if (fsize == 0) return null; // it cannot be zero-sized

  for (;;) {
    if (fseek(fi, 0, SEEK_SET) == -1) {
      import core.stdc.errno;
      if (errno == EINTR) continue;
      return null;
    }
    break;
  }

  auto res = new ubyte[](cast(int)fsize);
  conwriteln("loading toxcore data; size=", fsize);

  auto left = res;
  while (left.length > 0) {
    auto rd = fread(left.ptr, 1, left.length, fi);
    if (rd == 0) { delete res; return null; } // error
    if (rd < cast(int)left.length) {
      if (!ferror(fi)) { delete res; return null; } // error
      import core.stdc.errno;
      if (errno != EINTR) { delete res; return null; } // error
      if (rd > 0) left = left[rd..$];
      continue;
    }
    if (rd > left.length) { delete res; return null; } // error
    left = left[rd..$];
  }

  if (left.length) { delete res; return null; } // error

  return res;
}


bool saveToxCoreData (ToxP tox, const(char)[] toxdatafname) nothrow {
  import core.stdc.stdio : rename;
  import core.stdc.stdlib : malloc, free;
  import core.sys.posix.fcntl : open, O_WRONLY, O_CREAT, O_TRUNC;
  import core.sys.posix.unistd : unlink, close, write, fdatasync;

  if (tox is null || toxdatafname.length == 0) return false;

  auto size = tox_get_savedata_size(tox);
  if (size > int.max/8) return false; //throw new Exception("save data too big");

  char* savedata = cast(char*)malloc(size);
  if (savedata is null) return false;
  scope(exit) if (savedata !is null) free(savedata);

  tox_get_savedata(tox, savedata);
  conwriteln("save toxcore data; size=", size);

  static char* namebuf = null;
  static char* namebuf1 = null;
  static uint nbsize = 0;

  if (nbsize < toxdatafname.length+1024) {
    import core.stdc.stdlib : realloc;
    nbsize = cast(uint)toxdatafname.length+1024;
    namebuf = cast(char*)realloc(namebuf, nbsize);
    if (namebuf is null) assert(0, "out of memory");
    namebuf1 = cast(char*)realloc(namebuf1, nbsize);
    if (namebuf1 is null) assert(0, "out of memory");
  }

  auto origName = expandTilde(namebuf[0..nbsize-6], toxdatafname);
  if (origName == null) return false; // oops
  origName.ptr[origName.length] = 0; // zero-terminate

  // create temporary name
  namebuf1[0..origName.length] = origName[];
  namebuf1[origName.length..origName.length+5] = ".$$$\x00";

  int fo = open(namebuf1, O_WRONLY|O_CREAT|O_TRUNC, 0o600);
  if (fo == -1) {
    conwriteln("failed to create file: '", origName, ".$$$'");
    return false;
  }

  auto left = savedata[0..size];
  while (left.length > 0) {
    auto wr = write(fo, left.ptr, left.length);
    if (wr == 0) {
      // out of disk space; oops
      close(fo);
      unlink(namebuf1);
      return false;
    }
    if (wr < 0) {
      import core.stdc.errno;
      if (errno == EINTR) continue;
      // some other error; oops
      close(fo);
      unlink(namebuf1);
      return false;
    }
    if (wr > left.length) {
      // wtf?!
      close(fo);
      unlink(namebuf1);
      return false;
    }
    left = left[wr..$];
  }

  fdatasync(fo);
  close(fo);

  if (rename(namebuf1, namebuf) != 0) {
    unlink(namebuf1);
    return false;
  }

  return true;
}


version(ToxCoreDebug) {
static extern(C) void toxCoreLogCB (Tox* tox, TOX_LOG_LEVEL level, const(char)* file, uint line, const(char)* func, const(char)* message, void* user_data) nothrow {
  static inout(char)[] sz (inout(char)* s) nothrow @trusted @nogc {
    if (s is null) return null;
    uint idx = 0;
    while (s[idx]) ++idx;
    return cast(inout(char)[])(s[0..idx]);
  }

  conwriteln("level=", level, "; file=", sz(file), ":", line, "; func=", sz(func), "; msg=", sz(message));
}
}


// ////////////////////////////////////////////////////////////////////////// //
// returns `false` if can't open/create
ToxP toxCreateInstance (const(char)[] toxdatafname, in ref ProtoOptions protoOpts, bool allowNew, bool* wasCreated=null) nothrow {
  if (wasCreated !is null) *wasCreated = false;

  if (toxdatafname.length == 0) return null;

  // create toxcore
  char* toxProxyHost = null;
  scope(exit) {
    import core.stdc.stdlib : free;
    if (toxProxyHost !is null) free(toxProxyHost);
  }
  ubyte[] savedata;
  scope(exit) delete savedata;

  TOX_ERR_OPTIONS_NEW errton = 0;
  auto toxOpts = tox_options_new(&errton);
  if (errton) toxOpts = null;
  assert(toxOpts !is null);
  scope(exit) if (toxOpts !is null) tox_options_free(toxOpts);
  tox_options_default(toxOpts);

  bool createNewToxCoreAccount = false;
  toxOpts.tox_options_set_ipv6_enabled(protoOpts.ipv6);
  toxOpts.tox_options_set_udp_enabled(protoOpts.udp);
  toxOpts.tox_options_set_local_discovery_enabled(protoOpts.localDiscovery);
  toxOpts.tox_options_set_hole_punching_enabled(protoOpts.holePunching);
  toxOpts.tox_options_set_start_port(protoOpts.startPort);
  toxOpts.tox_options_set_end_port(protoOpts.endPort);
  toxOpts.tox_options_set_tcp_port(protoOpts.tcpPort);

  toxOpts.tox_options_set_proxy_type(protoOpts.proxyType);
  if (protoOpts.proxyType != TOX_PROXY_TYPE_NONE) {
    import core.stdc.stdlib : malloc;
    toxOpts.tox_options_set_proxy_port(protoOpts.proxyPort);
    // create proxy address string
    toxProxyHost = cast(char*)malloc(protoOpts.proxyAddr.length+1);
    if (toxProxyHost is null) assert(0, "out of memory");
    toxProxyHost[0..protoOpts.proxyAddr.length] = protoOpts.proxyAddr[];
    toxProxyHost[protoOpts.proxyAddr.length] = 0;
    toxOpts.tox_options_set_proxy_host(toxProxyHost);
  }

  savedata = loadToxCoreData(toxdatafname);
  if (savedata is null) {
    // create new tox instance
    if (wasCreated !is null) *wasCreated = true;
    if (!allowNew) return null;
    toxOpts.tox_options_set_savedata_type(TOX_SAVEDATA_TYPE_NONE);
    conwriteln("creating new ToxCore account...");
  } else {
    // load data
    conwriteln("setting ToxCore account data (", savedata.length, " bytes)");
    toxOpts.tox_options_set_savedata_type(TOX_SAVEDATA_TYPE_TOX_SAVE);
    toxOpts.tox_options_set_savedata_length(savedata.length);
    toxOpts.tox_options_set_savedata_data(savedata.ptr, savedata.length);
  }
  scope(exit) delete savedata;

  version(ToxCoreDebug) {
    toxOpts.tox_options_set_log_callback(&toxCoreLogCB);
  }

  // create tox instance
  TOX_ERR_NEW error;
  ToxP tox = tox_new(toxOpts, &error);
  if (tox is null) {
    conwriteln("cannot create ToxCore instance: error is ", error);
    return null;
  }

  tox_callback_self_connection_status(tox, &connectionCB);

  tox_callback_friend_name(tox, &friendNameCB);
  tox_callback_friend_status_message(tox, &friendStatusMessageCB);
  tox_callback_friend_status(tox, &friendStatusCB);
  tox_callback_friend_connection_status(tox, &friendConnectionCB);
  //tox_callback_friend_typing(tox, &friendTypingCB);
  tox_callback_friend_read_receipt(tox, &friendReceiptCB);
  tox_callback_friend_request(tox, &friendReqCB);
  tox_callback_friend_message(tox, &friendMsgCB);

  tox_callback_file_recv_control(tox, &fileRecvCtlCB);
  tox_callback_file_chunk_request(tox, &fileChunkReqCB);
  tox_callback_file_recv(tox, &fileRecvCB);
  //tox_callback_file_recv_chunk

  //tox_callback_conference_invite
  //tox_callback_conference_message
  //tox_callback_conference_title
  //tox_callback_conference_namelist_change

  //tox_callback_friend_lossy_packet
  //tox_callback_friend_lossless_packet

  return tox;
}


// ////////////////////////////////////////////////////////////////////////// //
private:
enum ToxCoreDataId = 0x15ed1b1fU;

enum ToxCoreChunkTypeHi = 0x01ceU;
enum ToxCoreChunkNoSpamKeys = 1;
enum ToxCoreChunkDHT = 2;
enum ToxCoreChunkFriends = 3;
enum ToxCoreChunkName = 4;
enum ToxCoreChunkStatusMsg = 5;
enum ToxCoreChunkStatus = 6;
enum ToxCoreChunkTcpRelay = 10;
enum ToxCoreChunkPathNode = 11;
enum ToxCoreChunkEnd = 255;


struct ToxSavedFriend {
  enum TOTAL_DATA_SIZE = 2216;
  enum CRYPTO_PUBLIC_KEY_SIZE = 32;
  enum CRYPTO_SECRET_KEY_SIZE = 32;
  enum SAVED_FRIEND_REQUEST_SIZE = 1024;
  enum MAX_NAME_LENGTH = 128;
  enum MAX_STATUSMESSAGE_LENGTH = 1007;

  enum Status : ubyte {
    NoFriend,
    Added,
    Requested,
    Confirmed,
    Online,
  }

  enum UserStatus : ubyte {
    None,
    Away,
    Busy,
    Invalid,
  }

  Status status;
  ubyte[CRYPTO_PUBLIC_KEY_SIZE] real_pk;
  char[SAVED_FRIEND_REQUEST_SIZE] info; // the data that is sent during the friend requests we do
  //ubyte pad0;
  ushort info_size; // length of the info
  char[MAX_NAME_LENGTH] name;
  ushort name_length;
  char[MAX_STATUSMESSAGE_LENGTH] statusmessage;
  //ubyte pad1;
  ushort statusmessage_length;
  UserStatus userstatus;
  //ubyte[3] pad2;
  uint friendrequest_nospam;
  ulong last_seen_time;

  void load (VFile fl) {
    // we can use CTFE here, but meh...
    status = cast(Status)fl.readNum!ubyte;
    if (status > Status.max) throw new ProtocolException("invalid friend status");
    fl.rawReadExact(real_pk[]);
    fl.rawReadExact(info[]);
    /*pad0 =*/ fl.readNum!ubyte;
    info_size = fl.readNum!(ushort, "BE");
    if (info_size > info.length) throw new ProtocolException("invalid friend data");
    fl.rawReadExact(name[]);
    name_length = fl.readNum!(ushort, "BE");
    if (name_length > name.length) throw new ProtocolException("invalid friend data");
    fl.rawReadExact(statusmessage[]);
    /*pad1 =*/ fl.readNum!ubyte;
    statusmessage_length = fl.readNum!(ushort, "BE");
    if (statusmessage_length > statusmessage.length) throw new ProtocolException("invalid friend data");
    userstatus = cast(UserStatus)fl.readNum!ubyte;
    if (userstatus > UserStatus.max) throw new ProtocolException("invalid friend userstatus");
    /*pad30 =*/ fl.readNum!ubyte;
    /*pad31 =*/ fl.readNum!ubyte;
    /*pad32 =*/ fl.readNum!ubyte;
    friendrequest_nospam = fl.readNum!uint;
    last_seen_time = fl.readNum!(ulong, "BE");
  }
}
