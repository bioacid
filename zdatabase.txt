tox id: 76-char

each account has its own separate directory.
there, we have one directory per contact, named by contact public key.
also, we have "conferences" directory, where we have... something, i
haven't decided yet.


i.e.:
accname/
  toxdata.tox
  proto.rc (see below)
  config.rc (see below)
  contacts/
    groups.rc (see below)
    UPCASED-64-HEX-CHARS/ (contact public key)
      config.rc (see below)
      avatars/
        UPCASE-HEX-AVATAR.png
        current.png: symlink to the actual avatar
      files/
        here we will store fully transferred files
      fileparts/
        here we will store partially accepted files
        (and will clear this dir on restart/reconnect)
      logs/
        here we will store textual logs, one file per month,
        named "YYYY-MM.log". see below for log format
        "unread.log" for unread messages (not put into normal log yet)
        "resend.log" for messages requires resending (copies of logged messages)


log format (it looks like text, but it isn't text):
T[YYYY/MM/DD HH:NN:SS]: text-in-utf8
date in local time.
text: chars in range of [0..31, 92, 127] are \xHH-encoded
T is message type:
  !: server/app notification
  <: outgoing
  >: incoming
if char before text is '!' instead of space, this is "/me" message


enum TriOption { Default = -1, No = 0, Yes = 1 }

struct CommonOptions {
  TriOption showOffline; // show this contact even if it is offline
  TriOption showPopup; // show popups for messages from this contact
  TriOption blinkActivity; // blink tray icon if we have some activity for this contact
  TriOption skipUnread; // skip this contacts in `next_unread` command
  TriOption ftranAllowed; // file transfers allowed for this contact
  int resendRotDays; // how many days message should be in "resend queue" if contact is offline (-1: use default value)
  int hmcOnOpen; // how many history messages we should show when we opening a chat with a contact (-1: use default value)
  //TriOption confAutoJoin; // automatically join the conference when we're going online
}


contact's config.rc: serialized with iv.txtser:
struct ContactConfig {
  uint gid; // group id (see groups.rc)
  string nick; // empty: unauthorized
  string visnick; // empty: use `nick`
  string statusmsg;
  uint lastonlinetime; // local unixtime; changed when contact status changed between offline and online (any kind of online)
  @SRZIgnore ubyte[32] pubkey; // used as unique contact id, same as directory name
  CommonOptions opts;
  bool showAlways; // always show this contact
  bool authReqPending; // pending authorisation (i.e. this is auth request, and `statusmsg` is request message)
  bool ephemeral; // "ephemeral" contact for conference (not yet)
}


proto.rc:
struct ProtoOptions {
  bool ipv6;
  bool udp;
  bool localDiscovery;
  bool holePunching;
  ushort startPort;
  ushort endPort;
  ushort tcpPort;
  ubyte proxyType;
  ushort proxyPort;
  string proxyAddr;
}


account's config.rc:
struct AccountConfig {
  string nick; // my nick
  string statusmsg; // my status message
  bool showOffline; // show offline persons?
  bool showPopup; // show popups for messages?
  bool blinkActivity; // blink tray icon if we have some activity (unread msg, transfer request, etc.)?
  bool skipUnread; // skip contacts in `next_unread` command?
  bool hideEmptyGroups; // hide empty groups? (can be overriden by `hideNoVisible` group option)
  bool ftranAllowed; // file transfers allowed for this group (-1: use default value)
  int resendRotDays; // how many days message should be in "resend queue" if contact is offline
  int hmcOnOpen; // how many history messages we should show when we opening a chat with a contact
}


groups.rc (sorted by group order):
struct GroupOptions {
  uint gid; // group id; there is always group with gid 0, it is "common" default group
  string name; // group name
  bool opened; // is this group opened?
  TriOption showOffline; // show offline persons in this group
  TriOption showPopup; // show popups for messages from this group
  TriOption blinkActivity; // blink tray icon if we have some activity (unread msg, transfer request, etc.) for this group
  TriOption skipUnread; // skip contacts from this group in `next_unread` command
  TriOption hideIfNoVisible; // hide this group if there are no visible items in it
  TriOption ftranAllowed; // file transfers allowed for this group
  int resendRotDays; // how many days message should be in "resend queue" if contact is offline (-1: use default value)
  int hmcOnOpen; // how many history messages we should show when we opening a chat with a contact (-1: use default value)
}
